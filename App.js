import React, { Component } from 'react';
import Main  from './src/Main';
import 'react-native-gesture-handler';
//import { Provider } from 'react-redux';
//import { createStore, applyMiddleware } from 'redux';
//import modules from 'src/modules';
//import thunk from 'redux-thunk';
import {Text, TextInput, Linking} from 'react-native';
//import { useScreens } from 'react-native-screens';
import TrackPlayer from 'react-native-track-player';
import NavigationService from 'src/NavigationService';
import AsyncStorage from "@react-native-community/async-storage";

//const store = createStore(modules, applyMiddleware(thunk));


export default class App extends Component {
  trackPlayerInit = async () => {
    console.log('trackPlayerInit')

    await TrackPlayer.setupPlayer();
    TrackPlayer.updateOptions({
      stopWithApp: true,
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        // TrackPlayer.CAPABILITY_STOP,
        TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
        TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
      ],
      icon: require('src/images/rect93.png')
    });
  }

  componentDidMount = () => {
    this.trackPlayerInit();

    console.log('App componentDidMount');
    Linking.removeEventListener("url");
    Linking.addEventListener("url", async (url) => {
      if(url) {
        let queue = await TrackPlayer.getQueue();
        console.log('linking', url)

        await AsyncStorage.setItem("itemTitle", queue[0].title)
        await AsyncStorage.setItem("itemId", queue[0].id);
        NavigationService.navigate('listen');
      }
    })
  }

  componentWillUnmount = () => {
    Linking.removeEventListener();
  }
  
  render() {
    if (Text.defaultProps == null) Text.defaultProps = {};
      Text.defaultProps.allowFontScaling = false;
    if (TextInput.defaultProps == null) TextInput.defaultProps = {};
      TextInput.defaultProps.allowFontScaling = false;
      
  
    /* device text 사이즈에 영향 안받게 설정 */
    if (Text.defaultProps == null) Text.defaultProps = {};
        Text.defaultProps.allowFontScaling = false;
    if (TextInput.defaultProps == null) TextInput.defaultProps = {};
        TextInput.defaultProps.allowFontScaling = false;

        
    return (
  //    <Provider store={store}>
        <Main />
  //    </Provider>
    );
  }
}

