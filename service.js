import TrackPlayer, { State } from 'react-native-track-player';
import NetInfo from "@react-native-community/netinfo";
import config from 'src/config';
import showToast from 'src/components/Toast';

let listeners = [];
let netUnsubscribe = null;

module.exports = async function() {
  listeners.map(listener => { listener.remove() });
  netUnsubscribe && netUnsubscribe();

  listeners = [
    TrackPlayer.addEventListener('remote-play', () => {
      console.log('remote-play')
      TrackPlayer.play();
    }),
    
    TrackPlayer.addEventListener('remote-pause', () => {
      console.log('remote-pause');
      TrackPlayer.pause();
    }),

    TrackPlayer.addEventListener('remote-stop', () => {
      console.log('remote-stop');
      TrackPlayer.stop();
      TrackPlayer.reset();
    }),

    TrackPlayer.addEventListener('remote-next', async () => {
      const tracks = await TrackPlayer.getQueue();
      const trackId = await TrackPlayer.getCurrentTrack();

      if(tracks[tracks.length-1].id !== trackId) {
        console.log('to next')
        TrackPlayer.skipToNext();
      }
    }),

    TrackPlayer.addEventListener('remote-previous', async () => {
      const tracks = await TrackPlayer.getQueue();
      const trackId = await TrackPlayer.getCurrentTrack();

      if(tracks[0].id !== trackId) {
        TrackPlayer.skipToPrevious();
      }
    }),

    TrackPlayer.addEventListener('playback-track-changed', async () => {
    }),
  ]

  netListener = NetInfo.addEventListener(async (state) => {
    console.log("Connection type", state.type);
    console.log("Is connected?", state.isConnected);
    console.log('useMoblieData', config.useMobileData )
    let playerState = await TrackPlayer.getState()
    console.log('player state:', playerState)
    if(!config.useMobileData && state.type !== "wifi" && playerState === 3 ) {
      showToast('Wifi 접속이 해제되어 현재 재생중인 여행이 종료됩니다.')
      TrackPlayer.removeUpcomingTracks();
    }
  });
};