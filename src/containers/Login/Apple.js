import React, {useEffect, useState, useRef, useImperativeHandle} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import AsyncStorage from "@react-native-community/async-storage";
import {StyledTextInput, StyledText} from 'src/components/styledComponents';
import jwtDecode from 'jwt-decode';

import config from 'src/config'

import appleAuth, { AppleButton } from '@invertase/react-native-apple-authentication';

import KakaoLogins, {KAKAO_AUTH_TYPES} from '@react-native-seoul/kakao-login';
// import NativeButton from 'apsl-react-native-button';

if (!KakaoLogins) {
  console.error('Module is Not Linked');
}

const logCallback = (log, callback) => {
  console.log(log);
  callback;
};

const TOKEN_EMPTY = 'token has not fetched';
const PROFILE_EMPTY = {
  id: 'profile has not fetched',
  email: 'profile has not fetched',
  profile_image_url: '',
};

const Apple = React.forwardRef((props, ref) => {
  const [loginLoading, setLoginLoading] = useState(false);
  const isMounted = useRef(null);
//   const [logoutLoading, setLogoutLoading] = useState(false);
//   const [profileLoading, setProfileLoading] = useState(false);
//   const [unlinkLoading, setUnlinkLoading] = useState(false);

//   const [token, setToken] = useState(TOKEN_EMPTY);
//   const [profile, setProfile] = useState(PROFILE_EMPTY);

  useEffect(() => {
    // executed when component mounted
    isMounted.current = true;
    return () => {
      // executed when unmount
      isMounted.current = false;
    }
  }, []);


  const kakaoLogin = () => {
    props.preOperation();

    logCallback('Login Start', setLoginLoading(true));

    return KakaoLogins.login([KAKAO_AUTH_TYPES.Talk, KAKAO_AUTH_TYPES.Account])
      .then(result => {
        logCallback(
          `Login Finished:${JSON.stringify(result)}`);
        return result
        
      })
      .catch(err => {
        if (err.code === 'E_CANCELLED_OPERATION') {
          logCallback(`Login Cancelled:${err.message}`);
        } else {
          logCallback(
            `Login Failed:${err.code} ${err.message}`);
        }
        return false
      })
      .finally(()=> {
        if (isMounted.current)
          setLoginLoading(false)
      })
      ;
  };

  const kakaoLogout = () => {

    logCallback('Logout Start', setLoginLoading(true));

    KakaoLogins.logout()
      .then(result => {
        // setToken(TOKEN_EMPTY);
        // setProfile(PROFILE_EMPTY);
        setLoginLoading(false)
        logCallback(`Logout Finished:${result}`, props.callback());
        
      })
      .catch(err => {
        setLoginLoading(false)
        logCallback(
          `Logout Failed:${err.code} ${err.message}`,
          props.callback()
        );
      });
  };

  const getProfile = () => {
    logCallback('Get Profile Start');

    return KakaoLogins.getProfile()
      .then(result => {
        // setProfile(result);
        logCallback(
          `Get Profile Finished:${JSON.stringify(result)}`,
        );

        return result
      })
      .catch(err => {
        logCallback(
          `Get Profile Failed:${err.code} ${err.message}`,
        );

        return false
      });
  };

  const unlinkKakao = () => {
    logCallback('Unlink Start');

    KakaoLogins.unlink()
      .then(result => {
        // setToken(TOKEN_EMPTY);
        // setProfile(PROFILE_EMPTY);
        logCallback(`Unlink Finished:${result}`, props.callback());
      })
      .catch(err => {
        logCallback(
          `Unlink Failed:${err.code} ${err.message}`,
          props.callback()
        );
      });
  };

  const loginProcess = async () => {
    let resp = await kakaoLogin()
    if (resp) {
        let resp2 = await getProfile()
        let result;
        if (resp2) {
            let profile = resp2;
            profile.profile_image = profile.thumb_image_url;
            result = {type: 'kakao', access_token: resp.accessToken, profile: resp2}
            props.callback(result)

        }
        else {
            console.log('kakao login - get profile error')
            props.callback(null)
        }
    }
    else {
      props.callback(null)
    }
  }
  
  const appleLogin = async() => {
    try {
        // performs login request
         const appleAuthRequestResponse = await appleAuth.performRequest({
           requestedOperation: appleAuth.Operation.LOGIN,
           requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
         });
       
         // get current authentication state for user
         const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);
       
         // use credentialState response to ensure the user is authenticated
         if (credentialState === appleAuth.State.AUTHORIZED) {
           // user is authenticated
                console.log(appleAuthRequestResponse);

                const { identityToken, email, user } = appleAuthRequestResponse;
                const decodedToken = jwtDecode(identityToken);
                console.log('email_from_decodedToken', decodedToken);
                console.log('email_from_decodedToken', decodedToken.email);
                console.log('email', email);
                console.log('user', user);

                let result = {
                  type: 'apple', 
                  access_token: user, 
                  profile: { 
                    email: decodedToken.email, 
                    nickname: decodedToken.email, 
                    name: decodedToken.email, 
                    id: user
                  }
                }

                console.log('AppleLogin: login success', result)
                props.callback(result)         
         }
       
       } catch (error) {
           if (error.code === appleAuth.Error.CANCELED) {
             console.log('APPLE LOGIN CANCELED')
                   // login canceled
           } else {
            console.log('APPLE LOGIN ERROR', error)
            // login error
           }

           props.callback(null)         

    }
}

const appleLogout = () => {

  console.log('apple logout')
  setLoginLoading(false)
  logCallback(`Logout Finished:`, props.callback());
  return 0;

}

// interface tokenType {
//   aud: string,
//   auth_time: number,
//   c_hash: string,
//   email: string,
//   email_verified: string,
//   exp: number,
//   iat: number,
//   is_private_email: string,
//   iss: string,
//   nonce: string,
//   nonce_supported: boolean,
//   sub: string
// }


  let viewText = '';
  let press = '';
  if (props.do === 'login') {
    viewText = '카카오로 로그인'
    press = appleLogin
  }
  else if (props.do === 'logout') {
    viewText = '로그아웃'
    press = appleLogout
  }
  else if (props.do === 'unlink') {
    viewText = '카카오 연결끊기'
    press = unlinkKakao
  }
  else if (props.do === 'profile') {
    viewText = '카카오 프로필가져오기'
    press = getProfile
  }

  useImperativeHandle(ref, () => ({
    logout: appleLogout}))

  let appleButton = <AppleButton
    buttonStyle={AppleButton.Style.WHITE_OUTLINE}
    buttonType={AppleButton.Type.SIGN_IN}
    cornerRadius={10}
    style={{
      width: '90%', // You must specify a width
      height: config.size._200px,

      // height: 50, // You must specify a height
      borderWidth: 1,
      borderRadius: 10,
      marginBottom: 10,
    }}
    onPress={() => appleLogin()}
  />

  if (props.do === 'logout')
    appleButton = <TouchableOpacity style={props.style} onPress={props.onPress? props.onPress: press} disabled={loginLoading}>
          <Image style={props.iconStyle} source={props.iconSource}/>
          <StyledText numberOfLines={1} style={props.textStyle}>{viewText}</StyledText>
        </TouchableOpacity>

  return appleButton;
})

export default Apple;

// {"type":"kakao",
// "token":"fdw1UlQW7He6Hx0v6THGOGiNX_PMTkLwPg5mqQo9cxgAAAF1T1_gMg",
// "profile":{
//     "age_range":"40~49",
//     "phone_number":null,
//     "display_id":null,
//     "birthday":"1122",
//     "email":"zzangyoume@gmail.com",
//     "gender":null,
//     "nickname":"정유미",
//     "thumb_image_url":"https://k.kakaocdn.net/dn/iNocl/btqLqmrGyvz/Lwhz9fhNtqfP5TKrgiNDc0/img_110x110.jpg",
//     "profile_image_url":"https://k.kakaocdn.net/dn/iNocl/btqLqmrGyvz/Lwhz9fhNtqfP5TKrgiNDc0/img_640x640.jpg",
//     "is_kakaotalk_user":null,
//     "birthyear":null,
//     "is_email_verified":true,
//     "has_signed_up":null,
//     "id":"1503203128",
//     "profile_image":"https://k.kakaocdn.net/dn/iNocl/btqLqmrGyvz/Lwhz9fhNtqfP5TKrgiNDc0/img_110x110.jpg"}}
