import React, {useEffect, useState, useRef, useImperativeHandle} from 'react';
import { Platform, 
    StyleSheet, 
    Text, 
    View,
    Image, 
    TouchableOpacity} from 'react-native';
import { LoginButton, AccessToken, LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import {StyledTextInput, StyledText} from 'src/components/styledComponents';

const logCallback = (log, callback) => {
    console.log(log);
    callback;
  };

const Facebook = React.forwardRef((props, ref) => {
    const [loginLoading, setLoginLoading] = useState(false);
    // const [logoutLoading, setLogoutLoading] = useState(false);

    const isMounted = useRef(null);

    useEffect(() => {
        // executed when component mounted
        isMounted.current = true;
        return () => {
          // executed when unmount
          isMounted.current = false;
        }
      }, []);

    const login = async () => {
        props.preOperation();
        logCallback('Login Start', setLoginLoading(true));

        try {
            // 테스트 코드
            if (Platform.OS === "android") {
                LoginManager.setLoginBehavior("web_only")
            }
            const result = await LoginManager.logInWithPermissions([
                "public_profile",
                "email",
            ]);

            if (result.isCancelled) {
                throw "User cancelled the login process"
            }

            const data = await AccessToken.getCurrentAccessToken();
            if (!data) {
                throw "Something went wrong obtainig access token";
            }

            logCallback('Login end');

            return data
        }
        catch (error) {
            logCallback('Login end with error');
            console.log(error);
            return false
        }
        finally {
            if (isMounted.current)
               setLoginLoading(false)
        }
    }



    const logout = async () => {
        
        logCallback('Logout Start', setLoginLoading(true));
 
 
        const logout = new GraphRequest(
            '/me/permissions/',
            {
                accessToken: props.accessToken,
                httpMethod: 'DELETE'
            },
            async (error, result) => {
                if (error) {
                    logCallback(`logout error: ${error}`);
                    if (isMounted.current)
                        setLoginLoading(false)
                } else {
                    logCallback('logout success');
                    LoginManager.logOut();
                    if (isMounted.current)
                        setLoginLoading(false)
                }
                props.callback();

            }
        );
        new GraphRequestManager().addRequest(logout).start();
    }

    const getProfile = (resp) => {

        return new Promise((resolve, reject) => {
            const infoRequest = new GraphRequest(
                '/me',
                {
                    accessToken: resp.accessToken,
                    parameters: {
                      fields: {
                        string: 'email,name,picture'
                      }
                    }
                  },
                (error, result) => {
                    if (error) {
                        console.log('getProfile error:', error.toString())
                        reject(error)
                    }                    
                    else {
                        console.log('getProfile result:', result)
                        resolve(result)
                    }
                }
            )

            new GraphRequestManager().addRequest(infoRequest).start()

        }) 
    }

    const loginProcess = async (param) => {
      let resp = await login();
      if (resp) {
        getProfile(resp).then(resp2 => {
            resp2.profile_image = resp2.picture.data.url;
            resp2.nickname = resp2.name;
            let result = {
              type: 'facebook',
              access_token: resp.accessToken,
              profile: resp2,
            };
            props.callback(result);
        })
        .catch(err => {
            console.log('facebook login - get profile error', err)
            props.callback(null);
        })
      }
      else {
        props.callback(null);

      }
    };

    let viewText = '';
    let press = '';
    if (props.do === 'login') {
      viewText = 'Facebook로 시작하기'
      press = loginProcess
    }
    else if (props.do === 'logout') {
      viewText = '로그아웃'
      press = logout
    }

    useImperativeHandle(ref, () => ({
        logout}))
    
    return <TouchableOpacity style={props.style} onPress={props.onPress? props.onPress: press} disabled={loginLoading}>
            <Image style={props.iconStyle} source={props.iconSource}/>
            <StyledText numberOfLines={1} style={props.textStyle}>{viewText}</StyledText>
        </TouchableOpacity>

});

export default Facebook;
// {"type":"facebook",
// "token":"EAALMbzbylZA0BAEsZC0YcgKSG9YSWgLsjyFxkSOFhCSzKSBg16VTTiUaUN44Gz9dbN1XWVm6jgFYB0RKW93HpwplDkNuiYj9VwWuda2Rdri9VUZAThsTdceY5xylTYRbhQgHwi1kxF6HjXZBHZA3zZAzhzu5qXeOZAIsPzZAr9L6WpmFAAZAHhRZCXZBFgu7RvNZAwlWam1pZCA8kjpB2MzABP59PgZBEj57dUHUXJYWpJIm4XQvP2C9SVbnPG",
// "profile":{
//     "picture":{
//         "data":{
//             "width":50,
//             "url":"https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=3657995460912366&height=50&width=50&ext=1606375736&hash=AeQ6MRAmcUkbFr_lmgY",
//             "is_silhouette":false,
//             "height":50
//         }
//     },
//     "email":"zzangyoume@gmail.com",
//     "name":"Youme Jung",
//     "id":"3657995460912366",
//     "profile_image":"https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=3657995460912366&height=50&width=50&ext=1606375736&hash=AeQ6MRAmcUkbFr_lmgY",
//     "nickname":"Youme Jung"
// }
// }
