import React, {useEffect, useState, useRef, useImperativeHandle} from 'react';
import {
  Alert,
  SafeAreaView,
  StyleSheet,
  Button,
  Platform,
  Text,
  Image, 
  View,
  TouchableOpacity,
} from 'react-native';
import {NaverLogin, getProfile} from '@react-native-seoul/naver-login';
import {StyledTextInput, StyledText} from 'src/components/styledComponents';

const ioskeys = {
  kConsumerKey: 'uSS3m754WF0EaSwl6kv3',
  kConsumerSecret: 'BjI1M6r0G5',
  kServiceAppName: '버스족',
  kServiceAppUrlScheme: 'busjokUrlScheme', // only for iOS
};

const androidkeys = {
  kConsumerKey: 'uSS3m754WF0EaSwl6kv3',
  kConsumerSecret: 'BjI1M6r0G5',
  kServiceAppName: '버스족',
};

const logCallback = (log, callback) => {
    console.log(log);
    callback;
  };

const initials = Platform.OS === 'ios' ? ioskeys : androidkeys;

const Naver = React.forwardRef((props, ref) => {
  const [loginLoading, setLoginLoading] = useState(false);
  // const [logoutLoading, setLogoutLoading] = useState(false);

  const isMounted = useRef(null);

  useEffect(() => {
    // executed when component mounted
    isMounted.current = true;
    return () => {
      // executed when unmount
      isMounted.current = false;
    }
  }, []);

  const naverLogin = (param) => {
    props.preOperation();
    logCallback('Login Start', setLoginLoading(true));

    return new Promise((resolve, reject) => {
      NaverLogin.login(param, (err, token) => {
        if (err) {
            logCallback(`Login error:${err}`);
            if (isMounted.current)
              setLoginLoading(false)

            reject(err);
        //   return false;
        }
        else {
            logCallback(`Login Finished:${JSON.stringify(token)}`);
            if (isMounted.current)
              setLoginLoading(false)
            resolve(token);
        }
      });
    });
  };

  const naverLogout = () => {
    logCallback('Logout Start', setLoginLoading(true));
    NaverLogin.logout();
    logCallback('Logout end', setLoginLoading(false));
    props.callback();
  };

  const getUserProfile = async (token) => {
    const profileResult = await getProfile(token);
    if (profileResult.resultcode === '024') {
      console.log('getUserProfile 실패', profileResult.message);
      return -1;
    }
    return profileResult.response;
  };

  const loginProcess = (param) => {
    naverLogin(param).then(async resp => {
        console.log('naver login process - login resp', resp)
        let resp2 = await getUserProfile(resp.accessToken);
        console.log('naver login process - login resp2', resp2)
        let result;
        if (resp2) {
          result = {
            type: 'naver',
            access_token: resp.accessToken,
            profile: resp2,
          }
          props.callback(result);
  
        } else {
          console.log('naver login - get profile error')
          props.callback(null);

        }
    })
    .catch(err=> {
        console.log('naver login resp error')
        props.callback(null);
    })
    .finally(()=> {
      if (isMounted.current)
        setLoginLoading(false)
    })
  };

  let viewText = '';
  let press = '';
  if (props.do === 'login') {
    viewText = '네이버로 시작하기';
    press = () => loginProcess(initials);
  } else if (props.do === 'logout') {
    viewText = '로그아웃';
    press = naverLogout;
  } else if (props.do === 'profile') {
    viewText = '네이버 프로필가져오기';
    press = getUserProfile;
  }

  
  useImperativeHandle(ref, () => ({
    logout: naverLogout}))

  return (
    <TouchableOpacity style={props.style} onPress={press} disabled={loginLoading}>
      {props.do === 'login' && <View style={props.iconStyle}><Image style={{flex: 1, width: null, height: null, resizeMode: 'contain'}} source={props.iconSource}/></View>}
      <StyledText numberOfLines={1} style={props.textStyle}>{viewText}</StyledText>
    </TouchableOpacity>
  );
})

export default Naver;

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'space-evenly',
//     alignItems: 'center',
//   },
// });


// {"type":"naver",
// "token":"AAAAOzCjEy2spvpwIanhkRoEbRhm0kP+9iivzUXFNttHsiJO6XL211H/joLtf0CoyQgaRJVdlrwpPRtUFfrn34ABFIw=",
// "profile":{
//     "id":"15236286",
//     "nickname":"dorami",
//     "profile_image":"https://phinf.pstatic.net/contact/20170329_266/1490762418879gRB6H_JPEG/nnnn-9627.jpg",
//     "email":"zzangyoume@naver.com",
//     "name":"정유미"}}
