import React, {Component} from 'react';
import {View,Image, StyleSheet, TouchableOpacity, ImageBackground, StatusBar, Text} from 'react-native';
import config from 'src/config';
import Icon from 'react-native-vector-icons/MaterialIcons'
import {StyledText} from 'src/components/styledComponents';

class StartScreen extends Component {
  state = {
    isFocused : false,
  }

  login = () => {
    this.props.navigation.navigate("start");

  }

  componentDidMount(){
    console.log("componentDidMount");
    
    this.subs = [
      this.props.navigation.addListener("didFocus", () => {
        console.log("StartScreen willFocus");
        this.setState({isFocused : true})
      }),
      this.props.navigation.addListener("willFocus", () => {
        console.log("StartScreen willFocus");
      }),
      this.props.navigation.addListener("willBlur", () => {
        console.log("StartScreen willBlur");
        this.setState({isFocused : false})
      }),
      this.props.navigation.addListener("didBlur", () => {
        console.log("StartScreen didBlur");
      }),
    ];
  }
  render() {
    const {isFocused} = this.state;
    
    return (      
      <View style={styles.container}>
        {isFocused && <StatusBar backgroundColor={config.colors.statusBarColor} barStyle='light-content'/>}
        {/*<ImageBackground source={backGroundImage} style={{width : "100%", height : "100%"}}> */}
          <View style={styles.header}>
           
          </View>
          <View style={styles.body}>
            <Image
                style={{width:config.deviceInfo.width, height:config.deviceInfo.width * 3482/4000, resizeMode:"stretch",}}
                source={config.images.mainIcon}/>  
          </View>
          <View style={styles.bottom}>
            <View style={{flexDirection:"row", justifyContent:"center"}}>
              <Icon style={[{color: "white"}]} size={12} name={'info'} />
              <StyledText style={styles.item1}>시작하기를 누르면 이용약관 동의로 간주됩니다.</StyledText>
            </View>
            <TouchableOpacity style={styles.bottomButton} onPress={this.login}>
              <StyledText style={styles.item2}>시작하기</StyledText>
            </TouchableOpacity>
          </View>
      </View>
    );
  }
}

export default StartScreen;

const styles = StyleSheet.create({
  container : {
    //height:"100%", 
    flex:1,
    backgroundColor : "#ffb468",
  },
  header : {
    flex : 2,
    justifyContent : "flex-end",
    alignItems : "center",
    bottom : 10,
  },
  body : {
    position:"absolute",
    left:0, top: config.deviceInfo.height /2 - ((config.deviceInfo.width * 3482/4000)/2) ,
    justifyContent : "flex-end",
  },
  bottom : {
    width:"100%", height:100, position:"absolute", bottom:40
  },
  bottomButton : {
    height:60,
    width : "90%",
    backgroundColor: config.colors.startButtonColor,
    padding:10,
    margin : 10,
    borderRadius: 5,
    justifyContent : "center",
    alignSelf : "center",
  },
  
  item1 : {
    alignItems : "center",
    color : "white",
    textAlign : "center",
    fontSize : 12,
  },
  item2 : {
    alignItems : "center",
    color : "white",
    textAlign : "center",
    fontSize : 24,
  },
  
})