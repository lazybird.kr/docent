import React, {Component}  from "react";
import {View, Text, TouchableOpacity, StyleSheet, Image, Alert, ImageBackground} from "react-native";
import config from 'src/config';
import {BackIcon, HeaderTitle} from 'src/components/header';
import Sound from 'react-native-sound';
import axios from 'axios';
import NaverMapView, {Marker, Path} from "react-native-nmap";
import {StyledText} from 'src/components/styledComponents';

export default class Preview extends Component {
  
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: () => <View style={{flex:1}}></View>,
      headerTitle : () => <HeaderTitle style={{fontSize: 16, color:"#d64409"}} title={navigation.state.params && navigation.state.params.item && navigation.state.params.item.subject || "듣기"} />,
      headerRight : () => 
      <TouchableOpacity style={{flex:1, marginRight:10, alignItems:"center", justifyContent:"center"}} onPress={()=>{navigation.navigate("home")}}>
        <Image source={config.images.selectedHomeIcon} style={{width:25, height:25, resizeMode:"contain"}}/>
      </TouchableOpacity>,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
      }
    }
  };

  state = {
    focused : false,
    zoom : 16,
    play : false,
    item : null,
    route_list : [],
    current : 0,
    maxItemCnt : 0,
    playList : [{title:"시작"},],
    isItemSelected : false,
  }

  componentDidMount() {
    console.log("Listen componentDidMount");
    
    this.subs = [
      this.props.navigation.addListener("didFocus", () => {
        console.log("Listen didFocus");
        if(!this.props.navigation.state.params || !this.props.navigation.state.params.item){
          /*Alert.alert(
            "Error",
            "선택된 도슨트가 없습니다. 홈화면에서 선택해 주세요.",
            [
              {text:"OK", onPress: ()=>this.props.navigation.navigate("home")}
            ],
            {cancelable : false}
            );
          return;
          */
         this.setState({isItemSelected : false})
         
        }
        else if(this.props.navigation.state.params && this.props.navigation.state.params.item.subject === (this.state.item && this.state.item.subject)){
          this.setState({focused : true});
          return;
        }
        let tmpItem = this.props.navigation.state.params && {... this.props.navigation.state.params.item} || null;
        let tmpProcessing = [...this.state.playList];
        if(tmpItem){
          const sound = new Sound(tmpItem.voice.path, Sound.MAIN_BUNDLE, (err) => {if(err) {console.log(err)}});
          tmpProcessing[0].sound = sound;
          tmpItem.locationList.forEach(
            (element,i) => {
              const sound = new Sound(element.voice_file.path, Sound.MAIN_BUNDLE, (err) => {if(err) {console.log(err)}});
              tmpProcessing.push({title:element.name, sound : sound})
              /*
                tmpItem.locationList[i].sound = sound;
                tmpProcessingTitle.push(element.name);
              */
              
              /*
              const path = `${RNFS.DocumentDirectoryPath}/a.mp4`;
              RNFS.writeFile(path, element.voice_file.path)
              .then(() => {
                console.log("path : ", path)
                const sound = new Sound(path, Sound.MAIN_BUNDLE, (err) => {if(err) {console.log(err)}});
                tmpItem.locationList[i].sound = sound;
                tmpProcessingTitle.push(element.name);
              })
              */
            })

            this.setState({item:tmpItem, maxItemCnt : tmpItem.place_num + 1});
            this.setState({focused : true, isItemSelected:true});
            this.setState({playList : tmpProcessing,
            current : 0,
            })
            
          
        }
        
      }),
      this.props.navigation.addListener("willFocus", () => {
        console.log("Listen willFocus");
      }),
      this.props.navigation.addListener("willBlur", () => {
        console.log("Listen willBlur");
        this.setState({
          focused : false,
          zoom : 16,
          play : false,
          item : null,
          current : 0,
          maxItemCnt : 0,
          route_list : [],
          playList : [{title:"시작"},],
          isItemSelected: false
        })
      }),
      this.props.navigation.addListener("didBlur", () => {
        console.log("Listen didBlur");
      }),
    ];
  }
  componentWillUnmount() {
    console.log("Listen componentDidUnmount()");
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  componentDidUpdate(prevProps, prevState) {
    console.log("Listen componentDidupdate");
    if(this.state.focused !== prevState.focused && this.state.focused){

      this.state.item && this.state.item.route_id_list.forEach(
        id => {
          axios.post(config.url.apiSvr, 
          {
            "data" : {
              "category": "route",
              "service": "GetRoute",
              "device" : {
                "device_id" : config.deviceInfo.device_id,
                "session_id" : config.sessionInfo.session_id,
              },
              "route": {
            		"id" : id
        		  }
            },
          })
          .then(resp => {
            console.log("GetRoute : ", resp);
            this.setState({route_list : [...this.state.route_list, resp.data.data.route]});
          })
        }   
      )
    }

  }

   MyMap() {
    const {item, route_list} = this.state;
    let pArr = [];
    item.locationList.forEach(loc => {
      console.log("loc : ", loc);
      pArr.push({
        latitude : parseFloat(loc.location.Latitude),
        longitude : parseFloat(loc.location.Longitude),
      })
    })

    let routeArr = [];
    if(route_list.length > 0){
      route_list.forEach(route => {
        let routeItem = [];
        routeItem.push({
          latitude : parseFloat(route.movement.start.Latitude),
          longitude : parseFloat(route.movement.start.Longitude)
        })
        route.geometry.forEach(element => {
          element.coordinate_list.forEach(pos => {
            routeItem.push({
              latitude : parseFloat(pos.Latitude),
              longitude : parseFloat(pos.Longitude)
            })  
          })
        })
        routeItem.push({
          latitude : parseFloat(route.movement.end.Latitude),
          longitude : parseFloat(route.movement.end.Longitude)
        })
        routeArr.push(routeItem);
      });  
    }
    console.log("routeArr : ", routeArr)
    
    return <NaverMapView style={{width: '100%', height: '100%'}}
                         showsMyLocationButton={true}
                         center={{...pArr[this.state.current === 0 ? 0 : this.state.current-1], zoom: this.state.zoom}}
                         onTouch={e => console.log('onTouch', JSON.stringify(e.nativeEvent))}
                         onCameraChange={e => console.log('onCameraChange', JSON.stringify(e))}
                         onMapClick={e => console.log('onMapClick', JSON.stringify(e))}>

        {routeArr.map((route, i)=>{
            return route.map((pos, j) => {
              if(j+1 === route.length){
                return;
              }
              //console.log("Path")
              return <Path key={i.toString()+j.toString()} color="#d64409" outlineColor="#d64409" passedColor="#d64409" coordinates={[route[j], route[j+1]]} onClick={() => console.warn('onClick! path')} width={3}/>
            })
          })}
        {pArr.map((pos, i) => {
          let image = config.images.markers[i];
          return (
            <Marker key={i.toString()} onLayout={(event) => {console.log("AAA : ", event)}} coordinate={pos} image={image}  width={20} height={30} onClick={() => console.warn('onClick! p0')}/>
          )
          
        })}
    </NaverMapView>
}
  play = () => {
    this.setState({play : !this.state.play});
    if(!this.state.play) {
      this.state.playList[this.state.current].sound.play(
        (success) => {
          console.log("play : ", this.state.current)
          if (success) {
            this.setState({play : false,
              current : this.state.maxItemCnt > this.state.current + 1 ? ++this.state.current : this.state.current,
            });
          } else {
            console.log('playback failed due to audio decoding errors');
          }
        }
      );
    }
    else {
      this.state.playList[this.state.current].sound && this.state.playList[this.state.current].sound.stop();
    } 
  }

  prevPlace = () => {
    this.state.playList[this.state.current].sound && this.state.playList[this.state.current].sound.stop();
    this.setState({
      play : false,
      current : this.state.current - 1 <= 0 ? 0 : this.state.current - 1,
    });
  }

  nextPlace = () => {
    this.state.playList[this.state.current].sound && this.state.playList[this.state.current].sound.stop();
      this.setState({
        play : false,
        current : this.state.current + 1 >= this.state.maxItemCnt ?  this.state.current : this.state.current+1,
      });
  }

  clickList = () => {
    this.props.navigation.navigate("listenlist", {
      locationList : this.state.item.locationList,
      subject : this.state.item.subject,
      current : this.state.current === 0 ? 0 : this.state.current-1, // current는 PlayList기준이므로 1 빼준다.
    });
  }

  clickTxt = () => {
    let text;
    if(this.state.current === 0){
      text = this.state.item.description;
    } else {
      text = this.state.item.locationList[this.state.current === 0 ? 0 : this.state.current-1].description
    }
     
    this.props.navigation.navigate("listentxt", {
      text : text,
      subject : this.state.item.subject,
    });
  }

  render() {
    const {play, playList, current,isItemSelected } = this.state;
    console.log("playList : ", current);
    return (
      <View style={{flex:1}}>
        {isItemSelected ?
      
      <View style={styles.container}>
        <View style={{flex:8, borderBottomWidth:2, borderBottomColor:"#d64409"}}>
          {this.state.focused && this.state.item && this.MyMap()}
        </View>
        <View style={{height:40, flexDirection:"row", marginTop : this.state.startButton_y ? this.state.startButton_y*0.5*-1: 0}}
          onLayout={({nativeEvent:{layout:{width, height}}}) => {
          if(!this.state.startButton_y) {
            this.setState({startButton_y : height});
          }
        }}>
          <View style={{flex:0.5}}>

          </View>
          <View style={{flex:9, borderRadius:50, borderWidth:2, borderColor:"#d64409", flexDirection:"row", backgroundColor:"white"}}>
            <TouchableOpacity style={{flex:3, alignItems : "center", justifyContent:"center"}} onPress={this.prevPlace}>
              <StyledText style={{textAlign:"center", fontSize : 14}}>{current === 0 ? `-` : playList[current-1].title}</StyledText>
            </TouchableOpacity>
            <View style={{flex:4, backgroundColor:"#d64409", alignItems : "center", justifyContent:"center"}} >
              <StyledText style={{textAlign:"center", fontSize : 16, color : "white"}}>{playList[current].title}</StyledText>
            </View>
            <TouchableOpacity style={{flex:3, alignItems : "center", justifyContent:"center"}} onPress={this.nextPlace}>
              <StyledText style={{textAlign:"center", fontSize : 14}}>{playList[current+1] ? playList[current+1].title : "-"}</StyledText>
            </TouchableOpacity>
          </View>
          <View style={{flex:0.5}}>

          </View>
        </View>
        <View style={{flex:1.5}}>
          {/* 새우깡 */}
        </View>
        <View style={{flex:1}}>
          <View style={{flex:1, padding : 10, flexDirection:"row"}}>
            <View style={{flex:1}}>
              <TouchableOpacity style={{margin:5, flex:1, backgroundColor:"lightgrey", alignItems:"center", justifyContent:"center",  borderRadius:5}} 
                onPress={this.clickList}>
                <StyledText style={{fontSize:14,color:"white"}}>LIST</StyledText>
              </TouchableOpacity>
            </View>
            <View style={{flex:3, flexDirection:"row"}}>
              <TouchableOpacity style={{flex:1, alignItems:"center", justifyContent:"center",}}>
                <Image source={config.images.before10s} style={{width : 40, height:40, resizeMode:"contain"}}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.play} style={{flex:1, alignItems:"center", justifyContent:"center",}}>
                <Image source={play? config.images.pause : config.images.play} style={{width : 30, height:30, resizeMode:"contain"}}/>
              </TouchableOpacity>
              <TouchableOpacity style={{flex:1, alignItems:"center", justifyContent:"center",}}>
              <Image source={config.images.after10s} style={{width : 40, height:40, resizeMode:"contain"}}/>
              </TouchableOpacity>
            </View>
            <View style={{flex:1}}>
            <TouchableOpacity style={{margin:5, flex:1, backgroundColor:"lightgrey", alignItems:"center", justifyContent:"center",  borderRadius:5}} 
              onPress={this.clickTxt}>
                <StyledText style={{fontSize:14,color:"white"}}>TXT</StyledText>
              </TouchableOpacity>
            </View>
        </View>
        </View>
      </View>
      :
      <View style={styles.container}>
        <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
          <ImageBackground source={config.images.speechBubble} style={{height:45, width:170}} >
            <StyledText style={{color:"#d64409", padding:5, fontSize:14}}>구매한 Docent가 없습니다.</StyledText>
          </ImageBackground>
          <Image source={config.images.errorIcon} style={{height:144, width:144}} resizeMode={"contain"}/>
        </View>
        
      </View>
      }
      </View>
    );
  }
}

const styles =StyleSheet.create({
  container : {
    flex:1,
    //alignItems : "center",
    //justifyContent : "center",
    backgroundColor : "white",
    paddingBottom : config.marginBottom,
  },
});
