import React, {Component}  from "react";
import {View, Text, Image, StyleSheet, TouchableOpacity, ScrollView, SafeAreaView } from "react-native";
import config from 'src/config';
import {BackIcon, HeaderTitle} from 'src/components/header';
import {StyledText} from 'src/components/styledComponents';

export default class ListenTxt extends Component {
  
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: () => <BackIcon navigation={navigation}/>,
      headerTitle : () => <HeaderTitle title={navigation.state.params && navigation.state.params.subject} />,
      headerRight : () => 
      <TouchableOpacity style={{flex:1, marginRight:10, alignItems:"center", justifyContent:"center"}} onPress={()=>{navigation.navigate("home")}}>
        <Image source={config.images.selectedHomeIcon} style={{width:25, height:25, resizeMode:"contain"}}/>
      </TouchableOpacity>,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
      }
    }
  };

  state = {
    text : "",
  }

  componentDidMount(){
    this.subs = [
      this.props.navigation.addListener("didFocus", () => {
        if(this.props.navigation.state.params && this.props.navigation.state.params.text){
          this.setState({text : this.props.navigation.state.params.text,
          })
        }
        console.log("ListenList didFocus");
      }),
      this.props.navigation.addListener("willFocus", () => {
        console.log("ListenList willFocus");
      }),
      this.props.navigation.addListener("willBlur", () => {
        console.log("ListenList willBlur");
        this.setState({focused : false})
      }),
      this.props.navigation.addListener("didBlur", () => {
        console.log("ListenList didBlur");
      }),
    ];
  }
  componentWillUnmount() {
    console.log("ListenList componentDidUnmount()");
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  render() {
    console.log("text : ", this.state.text);
    return (
      <SafeAreaView  style={styles.container}>
        <View style={{margin:10, marginLeft:30, flex:0.5}}>
          <StyledText style={{fontSize:16}}>여행소개</StyledText>
        </View>
        <View style={{ marginLeft:20, marginRight:20,  flex: 8,borderWidth:1, borderRadius:5, borderColor : "#d64409", }}>
          <ScrollView 
            contentContainerStyle={{
              flexGrow: 1,
              justifyContent: 'space-between',
            }}
          >
            <StyledText style={{color:"black", fontSize:14, padding:15}}>
              {this.state.text}
            </StyledText>
          </ScrollView>
        </View>
        <View style={{flex:1.5}}>
        </View>
      </SafeAreaView >
    );
  }
}

const styles =StyleSheet.create({
  container : {
    flex:1,
    margin:10,
  },
});
