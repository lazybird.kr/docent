import React, {Component}  from "react";
import {View, Text, Image, StyleSheet, TouchableOpacity, FlatList} from "react-native";
import config from 'src/config';
import {BackIcon, HeaderTitle} from 'src/components/header';
import {StyledText} from 'src/components/styledComponents';

export default class ListenList extends Component {
  
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: () => <BackIcon navigation={navigation}/>,
      headerTitle : () => <HeaderTitle title={navigation.state.params && navigation.state.params.subject} />,
      headerRight : () => 
      <TouchableOpacity style={{flex:1, marginRight:10, alignItems:"center", justifyContent:"center"}} onPress={()=>{navigation.navigate("home")}}>
        <Image source={config.images.selectedHomeIcon} style={{width:25, height:25, resizeMode:"contain"}}/>
      </TouchableOpacity>,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
      }
    }
  };

  state = {
    locationList : [],
    current : 0,
  }

  componentDidMount(){
    this.subs = [
      this.props.navigation.addListener("didFocus", () => {
        if(this.props.navigation.state.params && this.props.navigation.state.params.locationList){
          this.setState({locationList : this.props.navigation.state.params.locationList,
                          current : this.props.navigation.state.params.current,
          })
        }
        console.log("ListenList didFocus");
      }),
      this.props.navigation.addListener("willFocus", () => {
        console.log("ListenList willFocus");
      }),
      this.props.navigation.addListener("willBlur", () => {
        console.log("ListenList willBlur");
        this.setState({focused : false})
      }),
      this.props.navigation.addListener("didBlur", () => {
        console.log("ListenList didBlur");
      }),
    ];
  }
  componentWillUnmount() {
    console.log("ListenList componentDidUnmount()");
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  _keyExtractor(item, index) {
    return index.toString();
  }

  _renderLocation({item, index}) {
    console.log("_location : ", item);
    return (
      <View style={{flex:1}}>
        <View style={{flex:1, height : 50, flexDirection:"row", backgroundColor: index === this.state.current ? "rgba(214,68,9,0.5)" : "white",
          borderColor : "#d64409", borderWidth:1, borderRadius:5}} key={index}>
            <View style={{flex:1, alignItems:"center",justifyContent:"center"}}>
              <Image source={config.images.marker} style={{marginTop:8, width:30, height:30, resizeMode:"contain"}}/>
              <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'}}>
                <StyledText style={{color:"white", fontSize:10}}>{index+1}</StyledText>
              </View>

            </View>
            <View style={{flex:8, justifyContent:"center"}}>
            <StyledText style={{fontSize: 14, color:"black"}}>{item.name||""}</StyledText>
            </View>
        </View>
        <View style={{flex:1,height : 20, flexDirection:"row"}}>

        </View>
      </View>
            
    )
  }

  render() {
    console.log("locationList : ", this.state.locationList);
    return (
      <View style={styles.container}>
        <View style={{margin:10, marginLeft:30, flex:0.5}}>
          <StyledText style={{fontSize:16}}>이동경로</StyledText>
        </View>
        <View style={{margin:10, marginLeft:20, marginRight:20, flex:8}}>
        <FlatList
            data={this.state.locationList} 
            keyExtractor={(item, index) => this._keyExtractor(item, index)}
            renderItem={(item, index) => this._renderLocation(item, index,)}
            windowSize={3}
          />  
        </View>
        <View style={{flex:1.5}}>
        </View>
      </View>
    );
  }
}

const styles =StyleSheet.create({
  container : {
    flex:1,
    margin:10,
  },
});
