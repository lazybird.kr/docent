import Preview from './Preview';
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';

const ListenStack=createStackNavigator({
    listen : { 
      screen : Preview,
    },
  });

  ListenStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    /*
    if (navigation.state.routes.length > 0) {
      navigation.state.routes.map(route => {
        if (route.routeName === "listen" ||
            route.routeName === "listentxt" ||
            route.routeName === "listenlist"
        ) {
          tabBarVisible = false;
        } else {
          tabBarVisible = true;
        }
      });
    }
  */
    return {
     tabBarVisible,
    };
  };

  export {ListenStack}