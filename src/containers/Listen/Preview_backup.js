import React, {Component, useRef}  from "react";
import {View, TouchableOpacity, StyleSheet, Image, ScrollView, StatusBar, Alert, Linking, Share} from "react-native";
import config from 'src/config';
import {BackIcon, HeaderTitle} from 'src/components/header';
import Sound from 'react-native-sound';
import axios from 'axios';
import NaverMapView, {Circle, Marker, Path, Polyline, Polygon} from "react-native-nmap";
import Hyperlink from 'react-native-hyperlink'
import NetInfo from "@react-native-community/netinfo";
import Slider from "react-native-slider";

import {StyledText} from 'src/components/styledComponents';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
// import Slider from '@react-native-community/slider';
import {ImageSlider} from 'src/components/ImageSlider';
import AsyncStorage from "@react-native-community/async-storage";
import ReportPopup from 'src/components/ReportPopup';
import Modal from 'react-native-modal';
import TrackPlayer, { useTrackPlayerEvents, TrackPlayerEvents, getTrack } from 'react-native-track-player';
import Confirm from "src/components/Popup/Confirm";
import PlaySetting from "src/components/Popup/PlaySetting";
import Report from "src/components/Popup/Report";

import Loading from 'src/components/Loading';
import HyperLink from "react-native-hyperlink";
import dynamicLinks from '@react-native-firebase/dynamic-links';
import { Platform } from "react-native";
let eventListeners = [];
let unsubscribe = null;

export default class Preview extends Component {

  constructor(){
    super();
    this.sliderEditing = false;
    this.myScroll = null;
    this.beep = null;
  }
  
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    let title = navigation.getParam("title") ||  "";
    const current = navigation.getParam("current") || 0;
    if (current === 0) title = "여행떠나기"
    console.log("title: ", title, current)
    return {
      headerLeft: () => <BackIcon navigation={navigation}/>,
      headerTitle : () => <HeaderTitle style={{fontSize: 16, color:"#000000"}} title={title} />,
      headerRight: () => 
        <TouchableOpacity
          style={{marginRight: 15}}
          // onPress={navigation.getParam('showReference')}>
          onPress={navigation.getParam('pressReportButton')}>
          <StyledText style={{fontSize: config.fontSize._60px, color:"#292929"}}>
            신고
          </StyledText>
        </TouchableOpacity>,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
        elevation: 0, // remove shadow on Android
        shadowOpacity: 0, // remove shadow on iOS 

      }
    }
  };

  state = {
    focused : false,
    zoom : 16,
    play : false,
    item : null,
    route_list : [],
    playSeconds:0,
    playList : [{title:"시작"},],
    isItemSelected : false,
    selectedTabs : "이미지",
    docentDesc : {id:"리즈", desc: "나의 여행 속 이야기를 들려드리고 싶어요. 더 많은 이야기를 들려드리고"},
    commentDesc : {point:"8.5", desc: "매일 보던 곳이라고 생각했는데, 모르는 부분들을 알게 되어 신기하기도 하고 재밌었습니다."},
    isPreview: true,
    current : 0,
    maxItemCnt : 0,
    imageList : [],
    reportPopup : false,
    loading: false,
    initPlayList: false,
    openReference: false,
    openWifiNotic: false,
    myLike: false,
    isSettingPopupVisible : false,
  }

  onSliderEditStart = () => {
    this.sliderEditing = true;
}
  onSliderEditEnd = () => {
      this.sliderEditing = false;
  }
  onSliderEditing = value => {
    if(this.state.playList[0].sound){
      this.state.playList[0].sound.setCurrentTime(value);
        this.setState({playSeconds:value});
    }
  }

  setRemoteEventListener = () => {

    eventListeners.map(listener => { listener.remove() });
    eventListeners = [
      TrackPlayer.addEventListener('remote-play', this.remotePlay),
      TrackPlayer.addEventListener('remote-pause', this.remotePause),
      TrackPlayer.addEventListener('remote-stop', this.remoteStop),
      // TrackPlayer.addEventListener('remote-next', this.remoteNext),
      // TrackPlayer.addEventListener('remote-previous', this.remotePrev),
      TrackPlayer.addEventListener('playback-track-changed', (data) => { this.trackChange(data) }),
      TrackPlayer.addEventListener('playback-queue-ended', (data) => {
        console.log('queue ended', data)
        this.setState({
          play: false
         })
        this.pauseProgressBar();
      })
    ]
    
  }

  resetEventListner = () => {
    eventListeners.map( listener => { listener.remove() })
    eventListeners = [];
  }

  getTrackIndex = (trackList, trackId) => {
    let index = -1;
    for(let i = 0; i < trackList.length; i++) {
      if(trackList[i].id === trackId) {
        index = i;
        break;
      }
    }

    return index;
  }

  trackChange = async (data) => {
    const track = await TrackPlayer.getQueue()

    let prevIndex = this.getTrackIndex(track, data.track);
    let nextIndex = this.getTrackIndex(track, data.nextTrack)
    console.log('trackChanged', data, prevIndex, nextIndex)
    const state = await TrackPlayer.getState();
    if(prevIndex > -1 && prevIndex < nextIndex) {
      this.toNext();
    } else if(prevIndex > nextIndex) {
      this.toPrev();
    }
    // useAutoPlay가 셋팅되어 있지 않고 첫번째 포인트가 아니면 자동 재생 하지 않도록
    if (!config.useAutoPlay && prevIndex !== -1){
      TrackPlayer.pause();
      this.setState({play: false})
    }
    if(config.useAutoPlay && config.useBeep && prevIndex !==-1 && data.nextTrack !== null){
      TrackPlayer.pause();
      this.beep && this.beep.play((success) => {
        TrackPlayer.play();
      })
    }
  }

  remotePlay = async () => {
    const state = await TrackPlayer.getState();
    if(state === TrackPlayer.STATE_PLAYING) {
      this.startProgressBar();
      this.setState({
        play: true,
      })
    }
  }

  remotePause = async () => {
    const state = await TrackPlayer.getState();
    console.log("remotePause:", state)

    if( state === TrackPlayer.STATE_READY || state === TrackPlayer.STATE_STOPPED || state === TrackPlayer.STATE_PAUSED ){
      this.pauseProgressBar()
      this.setState({
        play: false
      })
    }
  }

  remoteStop = async () => {
    this.pauseProgressBar()

    this.setState({
      play: false,
      playSeconds: 0,
    })
  }

  toNext = () => {
    console.log('remote-next', this.state.current)
    if(!this.state.isPreview) {
      this.setState({
        current : this.state.current + 1 >= this.state.maxItemCnt ?  this.state.current : this.state.current+1,
      });
    }
  }

  toPrev = () => {
    console.log('remote-prev')
    if(!this.state.isPreview) {
      this.setState({
        current : this.state.current - 1 <= 0 ? 0 : this.state.current - 1,
      });
    }
  }

  wifiConnectionCheck = () => {
    NetInfo.fetch('wifi').then(state => {
      console.log("Is connected?", state.isConnected);
      if(!state.isConnected && !this.state.play) {
        this.setState({ 
          openWifiNotic: true,
        })
      } else {
        this.setState({focused:true});
      }
    })
  }

  handleLinkingEvt = async (url) => {
    const {playList} = this.state;
    let queue = await TrackPlayer.getQueue();
    if(queue.length > 0 && playList.length > 0) {
      let linkPlayId = queue[0].id;

      if(playList[0].playInfo.id !== linkPlayId) {
        await AsyncStorage.setItem("itemId", queue[0].id);
        await AsyncStorage.setItem("itemTitle", queue[0].title)

        this.getLocationList2();
      }
    }

  }

  componentDidMount() {
    console.log("Preview componentDidMount");
    config.goListen = false;

    this.subs = [
      this.props.navigation.addListener("didFocus", () => {
        console.log("Preview didFocus");
        unsubscribe = NetInfo.addEventListener(state => {
          console.log("Connection type", state.type);
          console.log("Is connected?", state.isConnected);
          console.log('useMoblieData', config.useMobileData )
          if(!config.useMobileData && state.type !== 'wifi') {
            this.setState({ 
              openWifiNotic: true,
            })
          } else {
            this.setState({
              focused: true,
            })
          }
        });

        if(config.useMobileData) {
          this.setState({focused:true});
        }
        
        AsyncStorage.getItem("itemTitle").then(
          title=>this.props.navigation.setParams({title : title})
        )

        Linking.addEventListener('url', this.handleLinkingEvt)        
      }),
      this.props.navigation.addListener("willFocus", () => {
        console.log("Preview willFocus");
        // NetInfo.fetch("wifi").then(state => {
        //   console.log("SSID", state.details.ssid);
        //   console.log("BSSID", state.details.bssid);
        //   console.log("Is connected?", state.isConnected);
        //   console.log("state", state)
        // });
      }),
      this.props.navigation.addListener("willBlur", () => {
        console.log("Preview willBlur");
        unsubscribe && unsubscribe();
        this.resetEventListner()
        Linking.removeEventListener('url', this.handleLinkingEvt);

        this.pauseProgressBar();

        this.setState({
          focused : false,
          zoom : 16,
          play : false,
          item : null,
          current : 0,
          maxItemCnt : 0,
          route_list : [],
          playList : [{title:"시작"},],
          isItemSelected: false,
          initPlayList: false,
          isPreview: true,
        })
      }),
      this.props.navigation.addListener("didBlur", () => {
        console.log("Preview didBlur");
      }),
    ];
    // this.props.navigation.setParams({showReference: this.showReference})
    this.props.navigation.setParams({pressReportButton: this.pressReportButton})
    this.props.navigation.setParams({current: this.state.current})

    this.beep = new Sound(require("src/assets/beep2.mp3"), null, err=>{
      console.log(err)
    })
  }

  shareItem = () => {
    console.log("shareItem : ", this.state.item)
    const {item} = this.state
    async function onShare(link) {
        try {
            const result = await Share.share({
                message: link,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    }
    async function buildLink() {
        let imageUrl = undefined;
        imageUrl = item.image_list[0].path;
        let link2 = await dynamicLinks().buildLink({
            link: config.url.webPage + '/post/' + item.id ,
            // domainUriPrefix is created in your Firebase console
            domainUriPrefix: 'https://docentogo.page.link',
            // optional set up which updates Firebase analytics campaign
            // "banner". This also needs setting up before hand
            social:{
                title:item.title || "",
                descriptionText:"",
                imageUrl: imageUrl
            },

            android: {
                packageName:"com.lazybird.docentogo",
                //fallbackUrl: 'https://bbc.com'
            },
            ios: {
                bundleId: "com.lazybird.docentogo",
                //fallbackUrl: 'https://bbc.com'
            },

        });
        console.log("buildShortLink : ", link2 + "&ofl=https%3A%2F%2Fplay%2Egoogle%2Ecom%2Fstore%2Fapps%2Fdetails?id=com%2Elazybird%2Edocentogo");
        link2 = link2 + "&ofl=https%3A%2F%2Fplay%2Egoogle%2Ecom%2Fstore%2Fapps%2Fdetails?id=com%2Elazybird%2Edocentogo"
        console.log("link : ", link2)
        //Clipboard.setString(link2);
        onShare(link2);
        return link2;
    }
    buildLink();

  }
  showReference = ()=>{
    console.log("showReference");
    this.setState({ openReference: true })
    // Alert.alert(
    //   "",
    //   this.state.item.reference || "",
    //   [
    //     {text:"확인", onPress: ()=>{
    //     }}
    //   ],
    //   {cancelable : false}
    //   );
  }
  componentWillUnmount() {
    console.log("Preview componentDidUnmount()");
    unsubscribe && unsubscribe();
    this.resetEventListner();
    this.subs.forEach((sub) => {
      sub.remove();
    });

  }

  getPlayIndex = async (playList) => {
    console.log('getPlayIndex')
    let currentTrack  = await TrackPlayer.getCurrentTrack();
    let index = 0;

    for(let i = 0; i < playList.length; i++) {
      console.log('[' + playList[i].playInfo.id, currentTrack);
      if(playList[i].playInfo.id === currentTrack) {
        index = i;
        break;
      }
    }

    return index;
  }

  setPlayerState = async (playList) => {
    let queue = await TrackPlayer.getQueue();
    let playerState = await TrackPlayer.getState()

    if(queue.length > 0 && playList.length > 0 && playList[0].playInfo) {
      this.setRemoteEventListener();
      
      if(playList[0].playInfo.id === queue[0].id) {
        if(queue.length > 1) {
          let index = await this.getPlayIndex(playList);

          this.setState({
            isPreview: false,
            current: index,
          })
        }

        if(playerState === 3) { //TrackPlayer.PLAY
          this.startProgressBar();
          this.setState({
            initPlayList: true,
            play: true
          })
        }
      }
    } else {
      // this.pauseProgressBar();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    console.log("Preview componentDidupdate");
    if(this.state.focused !== prevState.focused && this.state.focused){
      // this.getLocationList();
      this.getLocationList2();
      this.state.item && this.state.item.route_id_list && this.state.item.route_id_list.forEach(
        id => {
          axios.post(config.url.apiSvr, 
          {
            "data" : {
              "category": "route",
              "service": "GetRoute",
              "device" : {
                "device_id" : config.deviceInfo.device_id,
                "session_id" : config.sessionInfo.session_id,
              },
              "route": {
            		"id" : id
        		  }
            },
          })
          .then(resp => {
            console.log("GetRoute : ", resp);
            this.setState({route_list : [...this.state.route_list, resp.data.data.route]});
          })
        }   
      );


      if(!this.props.navigation.state.params || !this.props.navigation.state.params.item){
        this.setState({isItemSelected : false})
      } 
    }

    if(this.state.current !== prevState.current && this.state.selectedTabs === "경로"){
      this.scrollToCenterOfScrollView(this.state.current)
    }

  }

  setSoundDuration = (playList) => {
    for(let i=0;i<playList.length;i++){
      if(playList[i].playInfo) {
        const voice_file = playList[i].playInfo.url;
        if(voice_file) {
          const mainBundle = Platform.OS === 'ios'
          ? encodeURIComponent(Sound.MAIN_BUNDLE)
          : Sound.MAIN_BUNDLE;

          const sound = new Sound(voice_file, null, (err) => {
            console.log('sound index:', i)
            let duration = 0;
            if(err) {
              console.log("error2: ",err)
            } else {
              duration = sound.getDuration();
            }
            playList[i].playInfo.duration = duration;
          });

          this.setState({
            playList : playList,
          });
        }
      }
    }
  }

  checkLikeGuide = async (guide) => {
    let check = false;
    
    let recommendList = guide.recommendation_list;
    if(!recommendList) return check;

    console.log('recommend:', recommendList)
    for(let i = 0; i < recommendList.length; i++) {
      if(recommendList[i].member_id === config.member.member_id) {
        check = true;
        break;
      }
    }

    return check;
  }

  getLocationList2 = async () => {
    console.log('getLocationList2')
    this.setState({loading: true});

    const itemId = await AsyncStorage.getItem("itemId");
    console.log("itemId : ", itemId);
    if(!itemId){
      console.log('check:', this.props.navigation.getParam('testId'))
      return;
    }

    try {
      const resp = await axios.post(config.url.apiSvr, 
        {
          "data" : {
            "category": "guide",
            "service": "GetGuideDetail",
            "device" : {
              "device_id" : config.deviceInfo.device_id,
              "session_id" : config.sessionInfo.session_id,
            },
            "guide_id": itemId
          },
        });

      const {guide} = resp.data.data;
      // console.log('GetGuideDetail:', guide)
      let tmpItem = {...guide} || {};
      tmpItem.place_list = [];
      tmpItem.locationList = guide.place_list
      
      let imageList = [];
      let imageArray = []
      tmpItem.image_list && tmpItem.image_list.forEach(pic =>{
        imageArray.push({image : pic.path})
      })
      imageList.push(imageArray);

      // let playList = [...this.state.playList];
      let playList = [{title:"시작"},];

      let playerInfoList = [];
      let playInfo = {
        id: guide.id,
        url: guide.voice.path,
        title: guide.title,
        artist: guide.written_by.name || '',
        date: guide.created_at,
        artwork: imageArray.length > 0 ? imageArray[0].image : '',
      }
      if(guide.voice.duration) {
        playInfo.duration = guide.voice.duration;
      }

      playList[0].playInfo = playInfo;
      playList[0].desc = guide.description;
      playList[0].title = guide.title;
      playerInfoList.push(playInfo);
      

      tmpItem.locationList && tmpItem.locationList.forEach((place) => {
        const voice_file = place.voice_file && place.voice_file.path; 

        if(voice_file) {
          let placePlayInfo = {
            id: place.id,
            url: voice_file,
            title: place.name,
            artist: guide.written_by.name || '',
            date: guide.created_at,
          }
          if(place.voice_file.duration) {
            placePlayInfo.duration = place.voice_file.duration;
          }

          playList.push({
            playInfo: placePlayInfo,
            desc: place.description,
            title: place.name
          })

          // playerInfoList.push(placePlayInfo);
        }

        let imageArray = []
        if(place.image_files) {
          place.image_files.forEach(pic =>{
            imageArray.push({image : pic.path})
          });
        }
        imageList.push(imageArray);
      })

      // await TrackPlayer.add(playerInfoList);

      // console.log('tempItem:', imageList)
      let myLike = await this.checkLikeGuide(tmpItem);
      console.log('myLike:', myLike)
      this.setState({item:tmpItem,
        focused : true, isItemSelected:true,
        playList : playList, 
        maxItemCnt : tmpItem.place_num + 1,
        loading: false,
        imageList :imageList,
        myLike: myLike,
      });
      this.setPlayerState(playList);
      // this.setSoundDuration(playList);
      
    } catch(e) {
      this.setState({
        loading: false,
      })
    }
  }

  getLocationList = async () => {
    this.setState({loading : true})

    const itemId = await AsyncStorage.getItem("itemId");
    console.log("itemId : ", itemId);
    if(!itemId){
      return;
    }

    console.log("getLocationList : ", itemId)
    try{
      const resp = await axios.post(config.url.apiSvr, 
        {
          "data" : {
            "category": "guide",
            "service": "ReadGuide",
            "device" : {
              "device_id" : config.deviceInfo.device_id,
              "session_id" : config.sessionInfo.session_id,
            },
            "guide_id": itemId
          },
        });
        // console.log("resp.data.data : ", resp.data.data);
      const {guide} = resp.data.data;
      let tmpItem = {...guide} || {};
      
      const resp2 = await axios.post(config.url.apiSvr, 
        {
          "data" : {
            "category": "place",
            "service": "ReadPlacesOfGuide",
            "device" : {
              "device_id" : config.deviceInfo.device_id,
              "session_id" : config.sessionInfo.session_id,
            },
            "guide_id": itemId
          },
        });
      // console.log("places : ", resp2.data.data.place_list);
      
      tmpItem.locationList = resp2.data.data.place_list;
       let tmpProcessing = [...this.state.playList];
       let imageList = [];
       if(tmpItem){
        const mainBundle = Platform.OS === 'ios'
        ? encodeURIComponent(Sound.MAIN_BUNDLE)
        : Sound.MAIN_BUNDLE;
         const sound = new Sound(tmpItem.voice.path, null, (err) => {
           if(err) {console.log("error1: ",err)}
           tmpProcessing[0].sound = sound;
           tmpProcessing[0].desc = tmpItem.description;
           tmpProcessing[0].title = tmpItem.title;
          //  let imageArray = []
          //  console.log('-----------', tmpItem.image_list)
          //  tmpItem.image_list.forEach(pic =>{
          //   imageArray.push({image : pic.path})
          //  })
          //  imageList.push(imageArray);
           this.setState({item:tmpItem,
             focused : true, isItemSelected:true,playList : tmpProcessing, maxItemCnt : tmpItem.place_num + 1,
             loading: false,
            //  imageList :imageList
           });
         });
         let imageArray = []
         tmpItem.image_list.forEach(pic =>{
          imageArray.push({image : pic.path})
         })
         imageList.push(imageArray);

         for(let i=0;i<tmpItem.locationList.length;i++){
           const voice_file = tmpItem.locationList[i] && tmpItem.locationList[i].voice_file.path;

           let imageArray = []
          //  console.log('image_files:', tmpItem.locationList[i].image_files, i)
           if(tmpItem.locationList[i].image_files) {
              tmpItem.locationList[i].image_files.forEach(pic =>{
                imageArray.push({image : pic.path})
              })
            }
            console.log('imageList---1', imageList)
           if (imageArray.length > 0 ) imageList.push(imageArray);

           if(voice_file){
            const mainBundle = Platform.OS === 'ios'
            ? encodeURIComponent(Sound.MAIN_BUNDLE)
            : Sound.MAIN_BUNDLE;
             const sound = new Sound(voice_file, null, (err) => {
               if(err) {console.log("error2: ",err)}
               tmpProcessing[i+1] = {title : tmpItem.locationList[i].name, sound : sound,
                 desc:tmpItem.locationList[i].description, imageList :imageList
               };
               this.setState({item:tmpItem, playList : tmpProcessing, current : 0,
               });
             });
           }
         }
         console.log("imageList : ", imageList)
         this.setState({
           imageList: imageList,
         })
       }
    } catch(e){
      this.setState({
        loading: false,
      })
    }
    
  }
  renderCategory = () => {
    const {item } = this.state;
    // console.log("item22 : ",  item && item.category)
    // console.log("item44:", this.state.playList)
    const category = item ? item.category || [] : [];
    // console.log("item22 : ",  category)
    return (
      // <View style={{height:60, flexDirection:"row", margin:10, flexWrap:"wrap",alignItems:"center"}}>
      // <View style={{height:60, flexDirection:"row", flexWrap:"wrap",alignItems:"center"}}>
      <View style={{flex: 1, flexDirection: "row", flexWrap: "wrap"}}>
        {category.map((element,i) => {
          return (
            //<View key={i.toString()} style={{marginRight: 10, marginBottom:5, justifyContent:"center", alignItems:"center", width : element.length * 20 + 10, height:24,borderColor:"#26675e", borderRadius:12,borderWidth:1}}>
            <View key={i.toString()} style={{marginRight: 10, marginBottom:5, height:24, justifyContent: "center", alignSelf:"center", paddingLeft:12, paddingRight:12, borderColor:"#292929", borderRadius:12,borderWidth:1}}>
              <StyledText style={{color:"#292929"}} >{element}</StyledText>
            </View>
          ) 
        })}
      </View>
    )
  }

   MyMap() {
    const {item, route_list} = this.state;
    let pArr = [];
    console.log("route_list : ", route_list)
    item.locationList.forEach(loc => {
      console.log("loc : ", loc);
      pArr.push({
        latitude : parseFloat(loc.location.Latitude),
        longitude : parseFloat(loc.location.Longitude),
      })
    })

    let routeArr = [];
    if(route_list.length > 0){
      route_list.forEach(route => {
        let routeItem = [];
        routeItem.push({
          latitude : parseFloat(route.movement.start.Latitude),
          longitude : parseFloat(route.movement.start.Longitude)
        })
        route.geometry.forEach(element => {
          element.coordinate_list.forEach(pos => {
            routeItem.push({
              latitude : parseFloat(pos.Latitude),
              longitude : parseFloat(pos.Longitude)
            })  
          })
        })
        routeItem.push({
          latitude : parseFloat(route.movement.end.Latitude),
          longitude : parseFloat(route.movement.end.Longitude)
        })
        routeArr.push(routeItem);
      });  
    }
    console.log("routeArr : ", routeArr)
    
    return <NaverMapView style={{width: '100%', height: '100%'}}
                         showsMyLocationButton={true}
                         center={{...pArr[this.state.current === 0 ? 0 : this.state.current-1], zoom: this.state.zoom}}
                         onTouch={e => console.log('onTouch', JSON.stringify(e.nativeEvent))}
                         onCameraChange={e => console.log('onCameraChange', JSON.stringify(e))}
                         onMapClick={e => console.log('onMapClick', JSON.stringify(e))}>
        {routeArr.map((route, i)=>{
            return route.map((pos, j) => {
              if(j+1 === route.length){
                return;
              }
              //console.log("Path")
              return <Path key={i.toString()+j.toString()} color="#d64409" outlineColor="#d64409" passedColor="#d64409" coordinates={[route[j], route[j+1]]} onClick={() => console.warn('onClick! path')} width={3}/>
            })
          })}
        {pArr.map((pos, i) => {
          let image = config.images.markers[i];
          return (
            <Marker key={i.toString()} onLayout={(event) => {console.log("AAA : ", event)}} coordinate={pos} image={image}  width={20} height={30} onClick={() => console.warn('onClick! p0')}/>
          )
        })}
    </NaverMapView>
}

MyMap2() {
  const {item, route_list,current} = this.state;
  let pArr = [];
  console.log("route_list : ", route_list)
  item.locationList.forEach((loc,i) => {
    console.log("loc : ", loc);
    if(i===current || i+1 === current){
      pArr.push({
        index : i,
        latitude : parseFloat(loc.location.Latitude),
        longitude : parseFloat(loc.location.Longitude),
      })
    }
  })

  let routeArr = [];
  if(route_list.length > 0){
    route_list.forEach((route,i) => {
      if(i === current-1){
        let routeItem = [];
        routeItem.push({
          latitude : parseFloat(route.movement.start.Latitude),
          longitude : parseFloat(route.movement.start.Longitude)
        })
        route.geometry.forEach(element => {
          element.coordinate_list.forEach(pos => {
            routeItem.push({
              latitude : parseFloat(pos.Latitude),
              longitude : parseFloat(pos.Longitude)
            })  
          })
        })
        routeItem.push({
          latitude : parseFloat(route.movement.end.Latitude),
          longitude : parseFloat(route.movement.end.Longitude)
        })
        routeArr.push(routeItem);
      }
    });  
  }
  console.log("routeArr : ", routeArr)
  console.log("pArr : ", pArr)
  
  return <NaverMapView style={{width: '100%', height: '100%'}}
                       showsMyLocationButton={true}
                       center={{...pArr[0], zoom: this.state.zoom}}
                       onTouch={e => console.log('onTouch', JSON.stringify(e.nativeEvent))}
                       onCameraChange={e => console.log('onCameraChange', JSON.stringify(e))}
                       onMapClick={e => console.log('onMapClick', JSON.stringify(e))}>
      {routeArr.map((route, i)=>{
          return route.map((pos, j) => {
            if(j+1 === route.length){
              return;
            }
            //console.log("Path")
            return <Path key={i.toString()+j.toString()} color="#d64409" outlineColor="#d64409" passedColor="#d64409" coordinates={[route[j], route[j+1]]} onClick={() => console.warn('onClick! path')} width={3}/>
          })
        })}
      {pArr.map((pos, i) => {
        let image = config.images.markers[pos.index];
        console.log("dd")
        return (
          <Marker key={i.toString()} onLayout={(event) => {console.log("AAA : ", event)}} coordinate={pos} image={image}  width={20} height={30} onClick={() => console.warn('onClick! p0')}/>
        )
      })}
  </NaverMapView>
}

  preListen = async () => {
    const {playList, playSeconds} = this.state;

    if (!this.state.initPlayList) {
      console.log('reset & add')
      TrackPlayer.reset();
      this.setRemoteEventListener();

      await TrackPlayer.add(playList[0].playInfo);
      this.setState({
        initPlayList: true,
      })
      this.audioPlay();
    } else {
      this.audioPlay();
    }
  }

  startProgressBar = () => {
    this.timeout = setInterval(async () => {
      let position = await TrackPlayer.getPosition();
      //console.log('position:', position)
      this.setState({playSeconds: position})
    }, 100)
  }

  pauseProgressBar = () => {
    this.timeout && clearInterval(this.timeout);
  }

  audioPlay = async () => {

    // let duration = await TrackPlayer.getDuration();
    // console.log('duration:', duration)
    const {isPreview, playList} = this.state;

    const state = await TrackPlayer.getState();
    console.log('audioPlay state:', state)

    this.pauseProgressBar();
    if( state === TrackPlayer.STATE_PLAYING) {
      TrackPlayer.pause();
      this.setState({play: false})
    } else if( state === TrackPlayer.STATE_STOPPED) {
      if(isPreview) {
        TrackPlayer.reset();
        await TrackPlayer.add(playList[0].playInfo);
      }

      TrackPlayer.play();
      this.setState({
        playSeconds: 0,
        play: true
      });
      this.startProgressBar();

    } else if( state !== TrackPlayer.STATE_NONE){
      console.log('setInterval')
      this.startProgressBar();

      TrackPlayer.play();
      this.setState({play: true});
    }

  }

  play = () => {
    console.log("play");

    this.timeout = setInterval(() => {
      if(this.state.playList[0].sound && this.state.play === true){
          this.state.playList[0].sound.getCurrentTime((seconds, isPlaying) => {
              this.setState({playSeconds:seconds});
          })
      }
    }, 100);

    this.setState({play:!this.state.play});
    if(!this.state.play) {
      if (this.state.playList && this.state.playList.length > 0) {
        this.state.playList[0].sound && this.state.playList[0].sound.play(
          (success) => {
            if (success) {
              this.setState({play : false, playSeconds:0});
              this.state.playList[0].sound.setCurrentTime(0);
              this.timeout && clearInterval(this.timeout)
            } else {
              console.log('playback failed due to audio decoding errors');
            }
          }
        );
      }
    } else {
      if (this.state.playList && this.state.playList.length > 0) {
        this.state.playList[0].sound && this.state.playList[0].sound.stop();
      }
    } 
  }

  play2 = () => {
    const {playList, maxItemCnt} = this.state;
    console.log("play : ", this.state.current)
    this.timeout && clearInterval(this.timeout);
    this.timeout2 = setInterval(() => {
      if(playList[this.state.current].sound && this.state.play === true){
          playList[this.state.current].sound.getCurrentTime((seconds, isPlaying) => {
            
              this.setState({playSeconds:seconds});
          })
      }
    }, 100);
    this.setState({play : !this.state.play});
    if(!this.state.play) {
      playList[this.state.current].sound.play(
        (success) => {
          console.log("play : ", this.state.current)
          if (success) {
            playList[this.state.current].sound.setCurrentTime(0);
            this.timeout2 && clearInterval(this.timeout2);
            this.setState({play : false,
              current : maxItemCnt > this.state.current + 1 ? ++this.state.current : this.state.current,
            });
          } else {
            console.log('playback failed due to audio decoding errors');
          }
        }
      );
    }
    else {
      playList[this.state.current].sound && playList[this.state.current].sound.stop();
    } 
  }

  toPrevPlace = async () => {
    await TrackPlayer.skipToPrevious();
    // this.setState({
    //   current : this.state.current - 1 <= 0 ? 0 : this.state.current - 1,
    // });
  }

  prevPlace = () => {
    this.timeout && clearInterval(this.timeout);
    this.timeout2 && clearInterval(this.timeout2);
    this.state.playList[this.state.current].sound && this.state.playList[this.state.current].sound.stop();
    this.setState({
      play : false, playSeconds:0,
      current : this.state.current - 1 <= 0 ? 0 : this.state.current - 1,
    });
  }

  toNextPlace = async () => {
    await TrackPlayer.skipToNext();
    // this.setState({
    //   current : this.state.current + 1 >= this.state.maxItemCnt ?  this.state.current : this.state.current+1,
    // });
  }

  nextPlace = () => {
    this.timeout && clearInterval(this.timeout);
    this.timeout2 && clearInterval(this.timeout2);
    this.state.playList[this.state.current].sound && this.state.playList[this.state.current].sound.stop();
      this.setState({
        play : false, playSeconds:0,
        current : this.state.current + 1 >= this.state.maxItemCnt ?  this.state.current : this.state.current+1,
      });
  }

  selectTabs = (value) => {
    console.log("this.state.item : ", this.state.item);
    if(value === "지도" && !this.state.item.locationList[0].location.Latitude){
      Alert.alert(
        "",
        "위치기반 도슨트가 아닌 경우 지도 정보를 제공하지 않습니다.",
        [
          {text:"확인", onPress: ()=>{
          }}
        ],
        {cancelable : false}
        );
    } else {
      this.setState({selectedTabs:value})
    }
    
  }

  renderTabs = () => {
    const {selectedTabs} = this.state;
    let tabs = ["이미지", "설명", "경로", "지도"];
    if(!this.state.item || !this.state.item.locationList[0].location.Latitude){
      tabs = ["이미지", "설명", "경로"];
    }
    return (
      <View style={{flexDirection:"row", flex:1, justifyContent:"center",alignItems:"center"}}>
        {
          tabs.map((item, i) => {
            return (
              <TouchableOpacity key={i.toString()} style={{ height:40,
                borderBottomColor:"#d64409", borderBottomWidth : selectedTabs === item ? 2 : 0,
                width:config.deviceInfo.width*0.9/(tabs.length === 4 ? 4 : 3), alignItems:"center", justifyContent:"center"}}
                onPress={()=>this.selectTabs(item)}
              >
                <StyledText style={{color: selectedTabs === item ? '#d64409': '#707070',
                  fontFamily: selectedTabs === item ? config.defaultBoldFontFamily: config.defaultFontFamily}}>{item}</StyledText>
              </TouchableOpacity>
            )
          })
        }
      </View>
    )   
  }

  MyDescription = () => {
    const {playList, current} = this.state;

    return (
      <ScrollView style={{paddingHorizontal:30, marginTop: 30, marginBottom:30}}> 
        <StyledText>{playList[current].desc}</StyledText>
      </ScrollView>
    )
  }

  scrollToCenterOfScrollView = (indexOf) =>{
    console.log("indexOf : ", indexOf);
    indexOf > 0 && setTimeout(()=>this.myScroll  && this.myScroll.scrollTo({ x: 0, y: Math.max(indexOf*40), animated: true }),1);
  }

  MyPath = () => {
    const {playList, current} = this.state;

    const RoadLine = ({location}) => {
      return(
          // <View style={{...styles.roadLine, top: location === "first" ? 36: 0, height : location ==="last" || location === "first" ? 36 : 72}}></View>
          <Image source={config.images.dotline} style={{...styles.roadLine, top: location === "first" ? 36: 0, height : location === "first" ? 36 : 110}} resizeMode={"contain"} />  
      )
    }

    const renderPath = () => {
      return playList.map((item,i) => {
        const title = item.title;
        // const duration = item.sound && item.sound.getDuration();
        const duration = item.playInfo ? item.playInfo.duration : 0;
        console.log("duration : ", duration)
        
        let playTime = '';
        if(duration) {
          const playMinTime = Math.floor(duration / 60);
          const playSecTime = Math.floor(duration % 60);
          playTime = playMinTime + ":" + (playSecTime < 10? "0" + playSecTime : playSecTime);
        }
        return (
          <View key={i.toString()} style={{width:"65%", height:60, paddingLeft : 30, paddingTop:10}} flexDirection={"row"}>
            <View style={{position:"absolute", left:42}}>
              { i+1 !== playList.length &&
                <Image source={config.images.dotline} style={{...styles.roadLine, top: i === 0 ? 36: 0, height : i === 0 ? 36 : 110}} resizeMode={"contain"} />  
                // <RoadLine location={i === 0 ? "first" : i+1 === playList.length ? "last" : ""}/>  
              }
            </View>
            { i === 0 ?
              <View style={{width:30, height:40}}>
                <Image source={i===current ?config.images.dotRedMarker : config.images.dotGreyMarker} style={{width:"100%", height: i===current ? "100%": "85%"}} resizeMode={"contain"}/>
              </View>
              :<View style={{width:30, height:40}}>
                <Image source={i===current ?config.images.pathmarker1 : config.images.pathmarker2} style={{width:"100%", height: i===current ? "100%": "85%"}} resizeMode={"contain"}/>
                <View style={{position:"absolute",left:0,top:2,width: "100%", height: "100%", alignItems: "center"}}>
                  <StyledText style={{fontFamily:config.defaultBoldFontFamily, color:"white"}}>{i}</StyledText>
                </View>
              </View>
            }
            <View style={{flexDirection:"row",alignItems:"flex-start"}} >
              <StyledText style={[i === current? styles.selectedPathTxt: styles.defaultPathTxt, 
                {fontSize:14, marginLeft: 10}]}>{title}</StyledText>
              <StyledText style={{fontSize:12, color: "#707070"}}>{duration && "  (" + playTime + ")"}</StyledText>
            </View>
            
            
          </View>
        )
      })
      
    }
    return (
      <View style={{width:"100%", height:"100%", padding:10}}> 
        <ScrollView
          onContentSizeChange={() => {
            console.log("AAAAAAA")
            this.scrollToCenterOfScrollView(this.state.current); //ios에서 안되서 여기 코드 넣어 놓음;; 확인 필요
          }}
          ref={ref =>this.myScroll= ref}
        >
          {renderPath()}
        </ScrollView>
      </View>
    )
  }

  MyImage = () => {
    const {imageList, current} = this.state;
    // console.log('MyImage imgageList:', current, imageList)
    if(!imageList[current] || imageList[current].length === 0){
      // return <></>
      return (
        <View style={{width:"100%", height:"100%", alignItems: "center", justifyContent: "center"}}> 
          <Image source={config.images.greyDocentogo} style={{width:"30%", height:"30%", resizeMode:"contain"}}/>
        </View>
      )
    }
/*
    return (
      <View style={{width:"100%", height:"100%", paddingTop:20, paddingBottom:20}}> 
        <Image source={{uri:imageList[current][0].path}} style={{flex:1,width:null, height:config.deviceInfo.width, resizeMode:"contain"}}/>
      </View>
    )
    */
   
   console.log("current : ", current)
  //  console.log("images : ", imageList[current])
  //  console.log('width, height:', config.deviceInfo.width, config.deviceInfo.height)
   return (
    <View style={{width:"100%", height:"100%"}}> 
      <ImageSlider
        data={imageList[current]}
        //images={imageList[current]}
        height={"100%"}
        width={config.deviceInfo.width}
        /*
        customSlide={({ index, item, style, width, height }) => 
         { 
          console.log("index:", index)
         return(<View key={index} style={[style],{justifyContent:"center",backgroundColor:"blue"}}>
            <Image source={{uri:item}} style={{flex:1,width:width, height:height, resizeMode:"contain"}} />
          </View>)}
        }
        */
        indicator={true}
        animation={false}
        onPress={()=>{}}
        indicatorContainerStyle={{position:"absolute", bottom:10}}
        indicatorInActiveColor={"#FFFFFF"}
        indicatorActiveColor={"#9e9e9e"}
        style={{justifyContent:"center",alignItems:"center"}}
    />
    </View>
   )
  }

  renderComponents = () => {
    const {item, focused, selectedTabs, current} = this.state;
    // console.log('renderComponents:', item)

    if(!focused || !item){
      return null;
    }

    return (
      <>
        {selectedTabs === "이미지" &&  this.MyImage()}
        {selectedTabs === "설명" &&  this.MyDescription()}
        {selectedTabs === "경로" &&  this.MyPath()}
        {selectedTabs === "지도" &&  (current === 0 ? this.MyMap() : this.MyMap2())}
      </>
    )
  }
  renderDocent = () => {
    const {docentDesc, item} = this.state;
    if(!docentDesc|| !item){
      return <></>
    }
    // console.log("item : ", item)
    return (
      <View style={{flex:1, flexDirection:"row"}}>
        <View style={{justifyContent:"center", alignItems:"center", marginRight: 10}}>
          <Image style={{width:15, height:20,resizeMode:"contain"}} source={config.images.docentImage}/>
        </View>
        <View style={{width:"80%",justifyContent:"center"}}>
          <View style={{height:20, flexDirection: "row"}}>
            <StyledText style={{color:"#292929", fontSize:16}} numberOfLines={1}>{"도슨트  "}</StyledText>
            <StyledText style={{color:config.colors.defaultText, fontSize:16, fontFamily: config.defaultBoldFontFamily}}>{item.written_by.name}</StyledText>
          </View>
          {/* <View style={{height:20, width:"80%"}}>
          <StyledText style={{color:"#707070", fontSize:12}} numberOfLines={1}>{docentDesc.desc}</StyledText>
          </View> */}
        </View>
      </View>
    )
  }

  renderDocent2 = () => {
    const {docentDesc} = this.state;
    if(!docentDesc){
      return <></>
    }
    return (
      <View style={{flex:1, flexDirection:"row"}}>
        <View style={{width:"20%",justifyContent:"center"}}>
          <StyledText style={{color:"#292929"}} numberOfLines={1}>{docentDesc.id}</StyledText>
        </View>
        <View style={{width:"80%",justifyContent:"center"}}>
          
        </View>
      </View>
    )
  }

  renderComment = () =>{
    const {commentDesc} = this.state;
    if(!commentDesc){
      return <></>
    }
    return (
      <View style={{flex:1, flexDirection:"row"}}>
        <View style={{width:"20%",justifyContent:"center"}}>
          <StyledText style={{color:"#292929"}} numberOfLines={1}>{commentDesc.point}</StyledText>
        </View>
        <View style={{width:"80%",justifyContent:"center"}}>
          <StyledText style={{color:"#707070"}} numberOfLines={1}>{commentDesc.desc}</StyledText>
        </View>
      </View>
    )
  }

  renderPoint = () => {
    const {current, playList, route_list} = this.state;
    const point = "포인트 " + (current) +" : " + playList[current].title
    let path = ""
    console.log("route_list : ", route_list)
    if(current === 0 || route_list.length === 0){
      path = "";
    } else if(route_list[current-1] && route_list[current-1].movement.transportation === "walking"){
      path = "도보 " + route_list[current-1].total_time + "분"+ "(" + route_list[current-1].total_distance + "m)" 
    }
    
    return (
      <View style={{flex:1,paddingLeft:20}}>
        <View style={{height:30,justifyContent:"center"}}> 
          <StyledText style={{color:"#292929", fontSize:17 }} numberOfLines={1}>{point}</StyledText>
        </View>
        {/*<View style={{height:30, flexDirection:"row", alignItems:"center"}}>
          <StyledText style={{color:"#26675e", fontSize:16 }} numberOfLines={1}>{"경로   "}</StyledText>
          <StyledText style={{color:"#292929", fontSize:14 }} numberOfLines={1}>{path}</StyledText>
          </View>*/}
      </View>
    )
  }

  renderTip = () => {
    const {item, current} = this.state;
    console.log('renderTip:', current)
    const tip = current >0 ? item.locationList[current-1].tip || "" : ""
    
    
    return (
      <View style={{flex:1}}>
        {
          tip.length > 0 && 
          <View style={{flex:1, alignItems:"flex-start"}}>
            <StyledText style={{color:"#d64409", fontSize:15, fontFamily: config.defaultBoldFontFamily }} numberOfLines={1}>{"추가정보"}</StyledText>
            <ScrollView style={{width: "100%"}}>
              <HyperLink linkDefault={true} linkStyle={{color: "#2980b9"}}>
                <StyledText style={styles.tipTextBox}>{tip}</StyledText>
              </HyperLink>
            </ScrollView>
          </View>
        }
      </View>
    )
  }

  renderIconList = () => {
    return(
      <View style={{flexDirection: "row", alignItems:"center", justifyContent: "flex-end", width: "100%"}}>
        <TouchableOpacity 
          style={{justifyContent:"center", alignItems:"center", zIndex:1,}}
            onPress={this.shareItem}
          // onPress={this.pressReportButton}
        >
          <Image source={config.images.share} style={{width : 20, height:20, resizeMode:"contain", marginRight: 15}}/>
        </TouchableOpacity>
        <TouchableOpacity 
          style={{justifyContent:"center", alignItems:"center", zIndex:1,}}
            onPress={this.showSetting}
          // onPress={this.pressReportButton}
        >
          <Image source={config.images.play_setting} style={{width : 20, height:20, resizeMode:"contain", marginRight: 15}}/>
        </TouchableOpacity>
        <TouchableOpacity 
          style={{justifyContent:"center", zIndex:1,
            borderRadius:10, height:30, width:30}}
            onPress={this.showReference}
          // onPress={this.pressReportButton}
        >
          <StyledText style={{color:config.colors.extraText, fontSize:13}}>{"출처"}</StyledText>
        </TouchableOpacity>
      </View>
    )
  }
  renderTitle = () => {
    const {item, isPreview, current} = this.state;

    let title = item ? item.title : '';
    if(!isPreview && current !== 0) {
      const {current, playList} = this.state;
      title = "포인트 " + (current) +" : " + playList[current].title
    }

    // let view = 0
    // if (item && item.subscribe_count > 0) view = item.subscribe_count; 

    // let like = 0;
    // if(item && item.recommendation_count > 0) like = item.recommendation_count;

    return (
      <View style={{flex:1, flexDirection: "row", alignItems: "flex-start"}}>
        <StyledText numberOfLines={2} style={styles.guideTitle}>{title}</StyledText>
        {/* <TouchableOpacity 
          style={{justifyContent:"center", alignItems:"center", zIndex:1,}}
            onPress={this.shareItem}
          // onPress={this.pressReportButton}
          >
          <Image source={config.images.share} style={{width : 20, height:20, resizeMode:"contain", marginRight: 10}}/>
        </TouchableOpacity>
        <TouchableOpacity 
          style={{justifyContent:"center", alignItems:"center", zIndex:1,
            borderRadius:10, height:30, width:60}}
            onPress={this.showReference}
          // onPress={this.pressReportButton}
          >
          <StyledText style={{color:config.colors.extraText, fontSize:13}}>{"출처"}</StyledText>
        </TouchableOpacity> */}
      </View>
    )
  }

  renderViewInfo = () => {
    const {item} = this.state;

    let view = 0
    if (item && item.subscribe_count > 0) view = item.subscribe_count; 

    let like = 0;
    if(item && item.recommendation_count > 0) like = item.recommendation_count;
    
    let date = '';
    let guideDate;
    if(item) {
      if(item.updated_at) {
        guideDate = new Date(item.updated_at);
      } else if(item.create_at) {
        guideDate = new Date(item.created_at);
      }
    }
    
    if(guideDate) {
      date = guideDate.getUTCFullYear() + '-' + guideDate.getUTCMonth() + '-' + guideDate.getUTCDate();
      date += ' ' + guideDate.getUTCHours() + ':' + guideDate.getUTCMinutes();
    }
    return (
      <View style={{flexDirection: "row", marginTop: 3}}>
        <StyledText style={{fontSize: 12, color: config.colors.extraText, marginRight: 10}}>{date}</StyledText>
        {/* <StyledText style={{color:"#bfbfbf"}}>{"  |  "}</StyledText>
        <StyledText style={{fontSize: 12, color: config.colors.extraText}}>{"조회 " + view}</StyledText> */}
        <View style={styles.viewIconSet}>
          <Image source={config.images.boldListen} style={{width : 12, height:10, resizeMode:"contain", marginRight: 5}}/>
          <StyledText style={{fontSize: 12, color: config.colors.extraText}}>{view}</StyledText>
        </View>
        { config.access_info ?
          <TouchableOpacity style={styles.viewIconSet} onPress={this.pressLike}>
            <Image source={this.state.myLike? config.images.boldHeartOn :config.images.boldHeartOff} style={{width : 12, height:10, resizeMode:"contain", marginRight: 5}}/>
            <StyledText style={{fontSize: 12, color: config.colors.primary}}>{like}</StyledText>
          </TouchableOpacity>
          :<View style={styles.viewIconSet}>
            <Image source={config.images.guideHeartGrey} style={{width : 12, height:10, resizeMode:"contain", marginRight: 5}}/>
            <StyledText style={{fontSize: 12, color: config.colors.primary}}>{like}</StyledText>
          </View>
        }
        {/* <StyledText style={{color:"#bfbfbf"}}>{"  |  "}</StyledText>
        <StyledText style={{fontSize: 12, color: config.colors.extraText}}>{"좋아요 " + like}</StyledText> */}
      </View>
    )
  }

  configPlayList = () => {
    const {playList} = this.state

    let playInfoList = [];
    playInfoList = playList && playList.map((playItem) => {
      return playItem.playInfo;
    })

    TrackPlayer.add(playInfoList);
  }

  clickTravel = async () =>{
    this.setRemoteEventListener();
    this.timeout && clearInterval(this.timeout);
    TrackPlayer.reset();
    this.configPlayList();

    TrackPlayer.play();
    this.startProgressBar();
    this.setState({
      playSeconds: 0,
      isPreview:false,
      play: true,
      initPlayList: true,
    });
    // 조회 수 증가시키는 서비스 호출
    try{
      const resp = await axios.post(config.url.apiSvr, 
        {
          "data" : {
            "category": "guide",
            "service": "SubscribeGuide",
            "device" : {
              "device_id" : config.deviceInfo.device_id,
              "session_id" : config.sessionInfo.session_id,
            },
            "guide": {
              "id" : this.state.item.id,
            },
            "member" : {
              "member_id" : config.member.member_id
            }
          },
        });
      console.log("SubscribeGuide Success : ", resp.data.data);
    } catch(e){
      console.log("SubscribeGuide Error : ",e);
    }
    
  }

  pressReportButton = () =>{
    console.log("AAA")
    if(!this.state.openWifiNotic) {
      this.setState({reportPopup : !this.state.reportPopup})
    }
  }

  referencePopup = () => {
    return(
      <Modal isVisible={this.state.openReference}
      >
        <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
          <View style={styles.refPopupBody}>
            {
              this.state.item && this.state.item.reference?
              <View style={{flex:4, justifyContent:"center"}}>
                <ScrollView>
                  <HyperLink linkDefault={true} linkStyle={{color: "#2980b9"}}>
                    <StyledText style={{fontSize: 16, color: config.colors.defaultText, margin: 20, textAlign: 'left'}}>
                      {this.state.item.reference}
                    </StyledText>
                  </HyperLink>
                </ScrollView>
              </View>
              :
              <View style={{flex:4, justifyContent:"center", alignItems:"center"}}>
                <Image source={config.images.greyDocentogo} style={{width: 80, height: 80}}></Image>
                <StyledText style={{fontSize: 16, color: config.colors.defaultText, margin: 30, textAlign: 'center'}}>
                  {"출처가 없습니다."}
                </StyledText>
              </View> 
            }
            
            <View style={{flex:1, borderTopColor:"#bfbfbf", borderTopWidth:1}}>
                <View style={{flex: 1}}>
                <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                  onPress={() => {this.setState({openReference: false})}}>
                  <StyledText style={{fontSize: 16, fontFamily:config.defaultBoldFontFamily, color: '#d64409'}}>
                    {"확인하기"}
                  </StyledText>
                </TouchableOpacity>
                </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  showSetting = () =>{
    this.setState({isSettingPopupVisible : true})
  }

  playSettingPopup = () => {
    let that = this

    async function confirm({autoPlay, beep, onlyWifi}){
      that.setState({isSettingPopupVisible : false})
      config.useMobileData = onlyWifi;
      config.useBeep = beep;
      config.useAutoPlay = autoPlay;
      await AsyncStorage.setItem('localSetting', JSON.stringify({
        "useMoblieData": onlyWifi,
        "useBeep": beep,
        "useAutoPlay": autoPlay
      }))
    }
    function close(){
      that.setState({isSettingPopupVisible : false})
    }
    if(!this.state.isSettingPopupVisible){
      return(<></>)
    }
    return (
      <PlaySetting 
            visible={this.state.isSettingPopupVisible}
            onBackdropPress={close}
            leftButtonLabel={'닫기'}
            rightButtonLabel={'완료'}
            onLeftButtonPress={close}
            onRightButtonPress={confirm} />
    )
  }

  reportPopup = () => {
    let that = this
    function close(){
      that.setState({reportPopup : false})
    }
    function confirm(index){
      console.log("confirm : ", index)
      that.setState({reportPopup : false})
    }
    return (
      <Report
          visible={this.state.reportPopup}
          onBackdropPress={close}
            leftButtonLabel={'닫기'}
            rightButtonLabel={'신고'}
            onLeftButtonPress={close}
            onRightButtonPress={confirm} />
    )
    
  }

  goHome = () => {
    this.setState({
      openWifiNotic: false,
    })
    this.props.navigation.navigate('home');
  }

  goSetting = () => {
    this.setState({
      openWifiNotic: false,
    })
    this.props.navigation.navigate('accountScreen', {
      access_info: config.access_info,
    });
  }

  pressLike = async() =>{
    const {item} = this.state;
    this.setState({myLike : !this.state.myLike});
    try {
      const send_data = {
        "category": "guide",
        "service": "LikeGuide",
        "device" : {
          "device_id" : config.deviceInfo.device_id,
          "session_id" : config.sessionInfo.session_id,
        },
        "guide": {
          "id": item.id,
        },
        "member": {
            "member_id": config.member.member_id
        }
      }
      const resp = await axios.post(config.url.apiSvr,
        {"data" : send_data});
      console.log("LikeGuide : ", resp.data);
      let newCount = resp.data.data.guide.recommendation_count
      let newItem = item;
      newItem.recommendation_count = newCount ? newCount : 0;
      this.setState({item : newItem});
    } catch(e){
      console.log("LikeGuide : ", e);
    }
  }

  onTrackSliderChange = (value) => {
    console.log('slider change:', value)
    TrackPlayer.seekTo(value);
  }

  render() {
    const { playSeconds, startButton_y, isPreview, playList, current, loading, item } = this.state;
    // console.log("playList : ", playList);
    /*
    if(!isItemSelected){
      return (
        <View style={styles.container}>
          <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
            <ImageBackground source={config.images.speechBubble} style={{height:45, width:170}} >
              <StyledText style={{color:"#d64409", padding:5, fontSize:14}}>구매한 Docent가 없습니다.</StyledText>
            </ImageBackground>
            <Image source={config.images.errorIcon} style={{height:144, width:144}} resizeMode={"contain"}/>
          </View>
        </View>
      )
    }
    */
    let playTime = "00:00";
    let duration = playList && playList[current].playInfo ? parseFloat(playList[current].playInfo.duration) : 0;
    if(playList) console.log('render:', playList[current].playInfo)
    console.log('duration:', duration)
    if(duration){
      const playMinTime = Math.floor((duration - playSeconds) / 60);
      const playSecTime = Math.floor((duration- playSeconds) % 60);
      playTime = playMinTime + ":" + (playSecTime < 10? "0" + playSecTime : playSecTime);
    } else duration = 0;
    return (
      <View style={styles.container}>
        {(loading) && <Loading color={config.colors.primary}/>}
        {this.state.focused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
        
        <Confirm visible={this.state.openWifiNotic}
            onBackdropPress={this.pressReportButton}
            message={'Wi-Fi가 연결해제되어 이후 재생이 종료됩니다. 설정을 확인하세요. '}
            leftButtonLabel={'홈으로'}
            rightButtonLabel={'설정으로'}
            onLeftButtonPress={this.goHome}
            onRightButtonPress={this.goSetting} />
        {this.referencePopup()}
        {this.reportPopup()}
        {this.playSettingPopup()}
        {/* <Confirm visible={this.state.openReference}
          noImage={this.state.item && this.state.item.reference? true: false}
          image={config.images.greyDocentogo}
          message={this.state.item && this.state.item.reference ? this.state.item.reference: '출처가 없습니다.'}
          onCenterButtonPress={() => {this.setState({openReference: false})}}
          oneButton={true} /> */}
        <View style={styles.tabs}>
          {this.renderTabs()}
        </View>
        <View style={{flex:5, borderBottomWidth:1, borderBottomColor:"#d64409", backgroundColor: "#fffefb"}}>
          {this.renderComponents()}
        </View>
        <View style={{height:40, flexDirection:"row", marginTop : startButton_y ? startButton_y*0.5*-1: 0}}
          onLayout={({nativeEvent:{layout:{width, height}}}) => {
          if(!startButton_y) {
            this.setState({startButton_y : height});
          }
        }}>
          <View style={{flex:0.5}}>
          </View>

          
          {isPreview && 
          <View style={{flex:9, borderRadius:50, borderWidth:1, borderColor:"#d64409", flexDirection:"row", backgroundColor:"white"}}>
            <TouchableOpacity style={{flex:1, alignItems : "center", flexDirection:"row", borderRadius:50, borderTopEndRadius:0, borderBottomEndRadius:0, justifyContent:"center", backgroundColor:"#d64409"}} 
              onPress={this.preListen}>
              {/* onPress={this.play}> */}
              {/* <Icons name={'play'} color={"#ffffff"} size={24}/> */}
              <Image source={this.state.play? config.images.pause : config.images.play} style={{width : 13, height:15, resizeMode:"contain"}}/>
              <StyledText style={{textAlign:"center", fontSize : 15, color:"#ffffff", marginLeft: 7}}>{"미리듣기"}</StyledText>
            </TouchableOpacity>
            <TouchableOpacity style={{flex:1, alignItems : "center", justifyContent:"center"}} onPress={this.clickTravel}>
              <StyledText style={{textAlign:"center", fontSize : 15, color:"#d64409"}}>{"여행떠나기"}</StyledText>
            </TouchableOpacity>
          </View>
          }
          {!isPreview && 
          <View style={{flex:9, borderRadius:50, borderWidth:1, borderColor:"#d64409", flexDirection:"row", backgroundColor:"white"}}>
            {/* <TouchableOpacity style={{flex:3, alignItems : "center", justifyContent:"center"}} onPress={this.prevPlace}> */}
            <TouchableOpacity style={{flex:3, alignItems : "center", justifyContent:"center"}} onPress={this.toPrevPlace}>
              <StyledText numberOfLines={1} style={{ width: "90%", textAlign:"center", fontSize : 14}}>{current === 0 ? `-` : playList[current-1].title}</StyledText>
            </TouchableOpacity>
            <TouchableOpacity style={{flex:4, backgroundColor:"#d64409", alignItems : "center", justifyContent:"center"}} onPress={this.audioPlay}>
            {/* <TouchableOpacity style={{flex:4, backgroundColor:"#d64409", alignItems : "center", justifyContent:"center"}} onPress={this.play2}> */}
              <Image source={this.state.play? config.images.pause : config.images.play} style={{width : 13, height:15, resizeMode:"contain"}}/>
              {/* <Icons name={'play'} color={"#ffffff"} size={36}/> */}
            </TouchableOpacity>
            {/* <TouchableOpacity style={{flex:3, alignItems : "center", justifyContent:"center"}} onPress={this.nextPlace}> */}
            <TouchableOpacity style={{flex:3, alignItems : "center", justifyContent:"center"}} onPress={this.toNextPlace}>
              <StyledText numberOfLines={1} style={{ width: "90%", textAlign:"center", fontSize : 14}}>{playList[current+1] ? playList[current+1].title : "-"}</StyledText>
            </TouchableOpacity>
          </View>
          }
          <View style={{flex:0.5}}>

          </View>
        </View>
        <View style={{flex:4}}>
          <ScrollView style={{paddingHorizontal: 20, paddingTop: 10}}>
            {/* <View style={{height:30, zIndex:1, backgroundColor: "grey"}} >
              <TouchableOpacity 
                style={{justifyContent:"center", alignItems:"center", zIndex:1,
                  position:"absolute", top:35, right:10, borderRadius:10, height:30, width:60, backgroundColor: config.colors.secondary}}
                  onPress={this.showReference}
                // onPress={this.pressReportButton}
                >
                <StyledText style={{color:"#ffffff", fontSize:13}}>{"출처"}</StyledText>
              </TouchableOpacity>
            </View> */}
            <View style={{height: 35}}>
              {this.renderIconList()}
            </View>
            <View >
              {this.renderTitle()}
            </View>
            {/* <View style={{height:50, flexDirection:"row", justifyContent:"center", alignItems:"center"}}> */}
            <View style={{justifyContent:"center", alignItems:"flex-end", paddingTop: 15}}>
              <Slider style={styles.voiceSlider}
                trackStyle={styles.track}
                minimumValue={0}
                maximumValue={duration}
                thumbTintColor='transparent'
                maximumTrackTintColor='#eebaa5' 
                minimumTrackTintColor='#d64409'
                value={playSeconds}
                onValueChange={this.onTrackSliderChange}
               />
              <StyledText style={{color:"#292929"}}>{playTime}</StyledText>
            </View>
            {/* {!isPreview && current !== 0 && 
              <View style={{height:60}}>
                {this.renderPoint()}
              </View>
            } */}
            {(isPreview || current === 0 )&&
              <View style={{height:60, marginBottom: 15}}>
              {this.renderDocent()}
              {this.renderViewInfo()}
              </View>
            }
              {(isPreview || current === 0 )&& 
                // <View style={{height:60}}>               
                <View style={{flex: 1, marginBottom: 20}}>
                  {this.renderCategory()}
                </View>
              }
            {/* isPreview && 
            <View style={{height:80, paddingLeft:30, paddingRight:30}}>
              <View style={{flex:0.5, flexDirection:"row", justifyContent:"center", borderBottomColor:"#707070", borderBottomWidth:1}}>
                {this.renderDocent()}
              </View>
              <View style={{flex:0.5, flexDirection:"row", justifyContent:"center"}}>
                {this.renderComment()}
              </View>
            </View>
            */}
            { !isPreview && current !== 0 && 
            <View style={{flex: 1, marginRight:20, marginVertical: 20}}>
              {this.renderTip()}
            </View>
            }
            {/* <View style={{height:20}}></View> */}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles =StyleSheet.create({
  container : {
    flex:1,
    //alignItems : "center",
    //justifyContent : "center",
    backgroundColor : "white",
    paddingBottom : config.marginBottom,
  },
  tabs:{
    height:40,width:"90%",
    alignSelf:"center",
    borderBottomColor:"#d7e5e4", borderBottomWidth:1
  },
  roadLine: {
    width: 6,
    height: 72,
    // backgroundColor: '#9e9e9e',
  },
  modal:{
    flex:1,
    justifyContent: "flex-end",
    margin: 0
  },
  voiceSlider: {
    // width:config.deviceInfo.width*0.8,
    width: '100%',
    height: 10,
    alignSelf:'center',
    justifyContent: "center",
    marginHorizontal:Platform.select({ios:5})
  },
  guideTitle: {
    // width: '81%',
    flex: 1,
    // height: 50,
    alignSelf: "center",
    marginRight: 10,
    color: config.colors.defaultText,
    fontFamily: config.defaultBoldFontFamily,
    fontSize: config.size._70px,
  },
  tipTextBox: {
    marginTop: 10,
    color:"#292929",
    fontSize:13,
  },
  selectedPathTxt: {
    color: config.colors.primary,
    fontFamily: config.defaultBoldFontFamily
  },
  defaultPathTxt: {
    color: config.colors.defaultText
  },
  refPopupBody: {
    height:config.deviceInfo.width - 60,
    width:config.deviceInfo.width - 60, 
    borderRadius:20, 
    backgroundColor:"white"
  },
  viewIconSet: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 10,
  },
  track: {
    //borderStyle: 'solid',
    //borderWidth: StyleSheet.hairlineWidth,
    //borderColor: '#707070',
  }
});
