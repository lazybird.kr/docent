import React, {Component}  from "react";
import {View, Text, TextInput, Image, StyleSheet, TouchableOpacity, FlatList} from "react-native";
import config from 'src/config';
import {BackIcon, HeaderTitle} from 'src/components/header';
import {Map} from 'src/components/dialog';
import Modal from 'react-native-modal';
import {StyledTextInput} from 'src/components/styledComponents';

export default class HomeSearch extends Component {

  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    return {
      headerLeft: () => <BackIcon navigation={navigation}/>,
      headerTitle : () => 
        <StyledTextInput
          style={{paddingVertical : 0, fontSize: 20, color:"grey"}}
          autoCorrect={false} placeholder="검색" 
          placeholderTextColor="grey"
          onChangeText={params? params.onChangeSerachText : ()=>{}}
        >{params.searchValue}</StyledTextInput>,
      headerRight: () => 
        <View
          style={{flex: 1, marginRight : 10, alignItems:"center", justifyContent:"center"}}
          /*onPress={params? params.clickSearch : () =>{}}*/>
          <Image source={config.images.searchIcon} style={{height:30, width:30, resizeMode:"contain"}}/>
        </View>,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
      }
    }
  };

  constructor() {
    super();
    this.state = {
        searchValue : '',
        searchData : [],
        isMapVisible : false,
        selectedValue : {},
    }
    this._isMounted = false;
  }

  componentDidMount(){
    this._isMounted = true;
    this.props.navigation.setParams({
      onChangeSerachText: this.onChangeSerachText,
      searchValue : this.state.searchValue,
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onChangeSerachText = text => {
    console.log("text : ", text)
      if(this._isMounted)
        this.setState({ searchValue : text });
        const APP_KEY =config.kakaoRestAPI.appKey;
        const url =  config.kakaoRestAPI.searchURL +`?query=${text}`;

        fetch(url, {
          headers : {"Authorization" : "KakaoAK " +APP_KEY},
        })
        .then(res => res.json())
        .then(resp => {
          //console.log(("resp : ", resp));
          let newData = [];
          resp.documents.forEach(item => {
            newData.push(item)
          })
          if(this._isMounted) {
            this.setState({searchData : newData});
          }
          console.log(resp);
        }).catch(err => {console.log("resp : ", err)})
    }


  handleSelectPlace = (item) => {
    console.log("item : ", item);
    this.setState({/*isMapVisible : true,*/
      selectedValue : item});
      this.props.navigation.navigate("map", {
        place : item
      })
  }

  toggleMap = () => {
    //this.setState({isMapVisible : !this.state.isMapVisible});
    
  }
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '86%',
          backgroundColor: '#CED0CE',
          //marginLeft: '14%',
        }}
      />
    );
  }
  
  render() {
    //console.log("searchData : ", this.state.searchData);
    return (
      <View style={styles.container}>
        {this.state.isMapVisible?
        <Modal isVisible={this.state.isMapVisible}
        onBackdropPress={this.toggleMap}
        avoidKeyboard={true}
        style={styles.modalStyle} >
          <Map
            toggleMap={this.toggleMap}
            place={this.state.selectedValue}
            />
        </Modal>
        :
        <FlatList 
          style={{backgroundColor:"#FFFFFF", flex:1}}
          data={this.state.searchData}
          renderItem={({ item, index }) => (
            <View style={{height :40, margin:5, marginLeft:10}} key={index}>
              <TouchableOpacity onPress={()=>{this.handleSelectPlace(item)}}>
                <StyledTextInput style={{fontSize:14, color:"black"}}>{item.place_name}</StyledTextInput>
                <StyledTextInput style={{fontSize:12, color:"grey"}}>{item.address_name}</StyledTextInput>
              </TouchableOpacity>
              
            </View>
            )
          }
          keyExtractor={(item, index) => "key" + index}
          ItemSeparatorComponent={this.renderSeparator}
          keyboardShouldPersistTaps="always"
          //contentContainerStyle={{ paddingBottom: 50}}
        />
        }
      </View>
    );
  }
}

const styles =StyleSheet.create({
  container : {
    flex:1,
  },
  modalStyle : {
    flex: 1,
    justifyContent: "flex-end",
    margin: 0
  }
  
});
