import Home from './Home';
import HomeSearch from './HomeSearch';
import Map from './Map';
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';

const HomeStack=createStackNavigator({
  home : { 
    screen : Home,
  },
  homesearch : {
    screen : HomeSearch,
  },
  map : {
    screen : Map,
  }
},{
  navigationOptions: {
    ...TransitionPresets.SlideFromRightIOS,
  },
});

  export {HomeStack}