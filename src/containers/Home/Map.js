import React, {Component}  from "react";
import {View, FlatList,Keyboard, StatusBar, Platform, Text, TouchableOpacity} from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import config from 'src/config'
import {BackIcon, HeaderTitle} from 'src/components/header';
import NaverMapView, {Circle, Marker, Path, Polyline, Polygon} from "react-native-nmap";

export default class Map extends Component {

  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    return {
      headerLeft: () => <BackIcon navigation={navigation}/>,
      headerTitle : () => 
      <HeaderTitle title="지도" />,
      headerRight: () => <View style={{flex:1, alignItems:"center", justifyContent:"center"}}>
        </View>,        
      headerStyle : {
        backgroundColor: config.colors.headerColor,
      }
    }
  };

    constructor() {
        super();
        this._isMounted = false;
        this.state ={
          place : null,
          isFocused : false,
        }
    }
     
      
      componentDidMount() {
        console.log("Maps :", this.props.navigation.state.params)
        this._isMounted = true;
        this.subs = [
          this.props.navigation.addListener("didFocus", () => {
            this.setState({isFocused : true})
          }),
          this.props.navigation.addListener("willFocus", () => {
            
          }),
          this.props.navigation.addListener("willBlur", () => {
            this.setState({isFocused : false, place : null})
          }),
          this.props.navigation.addListener("didBlur", () => {
            
          }),
        ];
      }

      componentDidUpdate(prevProps, prevState){
        if(prevState.isFocused !== this.state.isFocused && this.state.isFocused){
          this.setState({place : this.props.navigation.state.params.place})
        }
      }

      componentWillUnmount() {
        this._isMounted = false;
        this.subs.forEach((sub) => {
          sub.remove();
        });
      }
    
      render() {
        const {place} = this.state;
        const tmpHtml = place ? `
          <!DOCTYPE html>
          <html>
          <body>
            <div id="map" style="width:1000px;height:1000px;"></div>
            <script type="text/javascript" src="https://dapi.kakao.com/v2/maps/sdk.js?appkey=65c756d7bf6cae5b922a846d2d7d5503"></script>
            <script>
              var container = document.getElementById('map');
              var options = {
                center: new kakao.maps.LatLng(${place.y}, ${place.x}),
                level: 3
              };

              var map = new kakao.maps.Map(container, options);
            </script>
          </body>
          </html>
          ` : `<h1>test </h1>`;
          
          return(
          
              <View style={{flex:1}}>
                {place &&<NaverMapView style={{width: '100%', height: '100%'}}
                         //showsMyLocationButton={true}
                         center={{latitude: parseFloat(place.y),
                          longitude: parseFloat(place.x), zoom: this.state.zoom}}
                         onTouch={e => console.log('onTouch', JSON.stringify(e.nativeEvent))}
                         onCameraChange={e => console.log('onCameraChange', JSON.stringify(e))}
                         onMapClick={e => console.log('onMapClick', JSON.stringify(e))}>
                          
                   
                         <Marker coordinate={{latitude: parseFloat(place.y),
                          longitude: parseFloat(place.x)}} onClick={() => console.warn('onClick! p0')}/>
                        
        
                        </NaverMapView>}    
                </View>
          )
      }

     
}