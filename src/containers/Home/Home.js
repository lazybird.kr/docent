import React, {Component}  from "react";
import {
  View, 
  StatusBar, 
  Image, 
  StyleSheet, 
  Keyboard,
  BackHandler,
  Linking,
  ActivityIndicator, 
  TouchableOpacity, 
  FlatList} from "react-native";
import config from 'src/config';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from 'src/containers/styles/SliderEntry.style';
import SliderEntry from 'src/containers/components/SliderEntry';
import AsyncStorage from "@react-native-community/async-storage";
import axios from 'axios';
import {formatInt} from 'src/modules/utils';
import {StyledText, StyledTextInput} from 'src/components/styledComponents';
import { ScrollView } from "react-native-gesture-handler";
import Modal from 'react-native-modal';
import {SearchFilter} from 'src/components/dialog';
import Loading from 'src/components/Loading';
import Confirm from "../../components/Popup/Confirm";

import VersionCheck from 'react-native-version-check';
import compareVersions from 'compare-versions';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

const SLIDER_1_FIRST_ITEM=1;

export default class Home extends Component {

  state = {
    advertisement : [
      // {
      //     title: '구석구석 동네탐방',
      //     subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
      //     illustration: 'https://drive.google.com/uc?id=12Gbf3vnNRSzsybCL3s6jZ6OK8G8rGENy'
      // },
      // {
      //     title: '오늘은 경복궁으로',
      //     subtitle: 'Lorem ipsum dolor sit amet',
      //     illustration: 'https://drive.google.com/uc?id=18e85ajYYfKxaK1yG8xvBsbrtzQ7DLuo5'
      // },
      // {
      //     title: '서촌 동네한바퀴',
      //     subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
      //     illustration: 'https://drive.google.com/uc?id=1qMn4J4_ixtBIK_CjOGnvWYxPiLZmueyC'
      // },
    ],
    slider1ActiveSlide: SLIDER_1_FIRST_ITEM,

    isFocused : false,
    loading : false,
    isModalVisible : false,
    filters : [],
    searchValue : "",
    version : {},
    showUpdatePopup:false,
    showUpdatePopup2:false,
    updateUrl:"",
    displayNotic: false,

  }

  
  
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    return {
      headerLeft: () => <Image source={config.images.logoText} style={{marginLeft:10, width:100, height:40, resizeMode:"contain"}}/>,
      headerTitle : () => null,
      
      /*
        <StyledText
          style={{fontSize: 20, color:"#d64409"}}
        >Docentogo</StyledText>,
      */
      headerRight: () => 
        <TouchableOpacity 
          style={{flex: 1, marginRight : 10, alignItems:"center", justifyContent:"center"}}
          onPress={params? params.clickFilter : () =>{}}>
          <Image source={config.images.filter} style={{height:16, width:16, resizeMode:"contain"}}/>
        </TouchableOpacity>,
      headerStyle : {
        //backgroundColor: "#f2fcfb",
        backgroundColor: "#fffefb",
        elevation: 0, // remove shadow on Android
        shadowOpacity: 0, // remove shadow on iOS 
      }
    }
  };

  isNeededUpdate = (latestVersion, currentVersion) => {
    if(compareVersions(latestVersion, currentVersion, '>') === 1){
      console.log("isNeededUpdate : true")
      return true
    }
    console.log("isNeededUpdate : false")
    return false
  }

  checkVersion = async ()=> {
    const currentVersion = VersionCheck.getCurrentVersion();
    
    console.log('current version', currentVersion)
    if(Platform.OS === "android"){
      const latestVersion = config.version.android;
      const updateUrl = config.googlePlayStoreUrl;
      if(this.isNeededUpdate(latestVersion, currentVersion)){
        const currentVersion1depth = currentVersion ? currentVersion.split(".")[0] :"";
        const latestVersion1depth = latestVersion ? latestVersion.split(".")[0] : "";
        const currentVersion2depth = currentVersion ? currentVersion.split(".")[1]: "";
        const latestVersion2depth = latestVersion ? latestVersion.split(".")[1] : "";
        if(currentVersion1depth !== latestVersion1depth || currentVersion2depth !== latestVersion2depth){
          //this.setState({showUpdatePopup2 : true, updateUrl})
        } else {
          this.setState({showUpdatePopup : true, updateUrl})
        }
      }
    } else {
      try {
        let updateNeeded = await VersionCheck.needUpdate();
        console.log("ios updateNeeded : ", updateNeeded)
        if(updateNeeded && updateNeeded.isNeeded){
          const currentVersion1depth = updateNeeded.currentVersion ? updateNeeded.currentVersion.split(".")[0] :"";
          const latestVersion1depth = updateNeeded.latestVersion ? updateNeeded.latestVersion.split(".")[0] : "";
          const currentVersion2depth = updateNeeded.currentVersion ? updateNeeded.currentVersion.split(".")[1]: "";
          const latestVersion2depth = updateNeeded.latestVersion ? updateNeeded.latestVersion.split(".")[1] : "";
          if(currentVersion1depth !== latestVersion1depth || currentVersion2depth !== latestVersion2depth){
            // this.setState({showUpdatePopup2 : true, updateUrl:updateNeeded.storeUrl})
            this.setState({showUpdatePopup : true, updateUrl:updateNeeded.storeUrl})
          } else {
            this.setState({showUpdatePopup : true, updateUrl:updateNeeded.storeUrl})
          }
          return;
        }
      } catch(e){
        console.log("VersionCheck : ", e)
      }
    }
  }

  chekcNotiPlayer = async () => {
    const initialUrl = await Linking.getInitialURL();
    console.log('initialUrl:', initialUrl)
  }

  componentDidMount(){
    console.log("Make2Step componentDidMount");
    this.props.navigation.setParams({
      //clickSearch: this.search,
      clickFilter:this.filter
    });

    AsyncStorage.getItem('localSetting', (err, value) => {
      let setting = JSON.parse(value)
      console.log('Setting', setting, typeof(setting))
      if(value === null) {
        this.setState({ useMoblieData: true })
      } else {
        config.useMobileData = setting["useMoblieData"]
        config.useAutoPlay = setting["useAutoPlay"]
        config.useBeep = setting["useBeep"]
        this.setState({
          displayNotic: false
        })
      }
    });

    AsyncStorage.removeItem("item");
    this.subs = [
      this.props.navigation.addListener("didFocus", () => {
        this.setState({isFocused : true, searchValue:""});
      }),
      this.props.navigation.addListener("willFocus", () => {
        console.log("Make2Step willFocus");
      }),
      this.props.navigation.addListener("willBlur", () => {
        console.log("Make2Step willBlur");
        this.setState({isFocused : false, searchValue:""});
      }),
      this.props.navigation.addListener("didBlur", () => {
        console.log("Make2Step didBlur");
      }),
    ];
    AsyncStorage.getItem("localCategory").then((data)=>{
      console.log("data : ", JSON.parse(data))
      const localCategory = JSON.parse(data);
      this.setState({filters : localCategory || []})
    });
    this.GetAdvertisementList();
    
  }

  setInitData = async () => {
    const access_token = config.access_info ? config.access_info.access_token : "";
    axios.post(config.url.apiSvr + "/private/GetAppVersion", {
      "data": {
        "device": {
          "device_id": config.deviceInfo.device_id,
          "session_id": config.sessionInfo.session_id,
        }
      }
    }).then(resp => {
      console.log("GetAppVersion : ", resp.data.data);
      const {version} = resp.data.data;
      if(version) {
        config.version = version
      }
      
      this.setState({
        version
      })
    }).catch(err => {
      console.log("err1 : ", err)
    })
  }

  componentDidUpdate(prevProps, prevState){
    if(prevState.isFocused !== this.state.isFocused && this.state.isFocused){
      this.setInitData();

      if(this.state.filters && this.state.filters.length === 0){
        this.readAllGuide();
      } else {
        this.searchGuide();
      }
      
    }

    if(this.state.isFocused){
      if(JSON.stringify(prevState.filters) !== JSON.stringify(this.state.filters)){
        if(this.state.filters && this.state.filters.length === 0){
          this.readAllGuide();
        } else {
          this.searchGuide();
        }
        
      }
      
    }

    if(JSON.stringify(prevState.version) !== JSON.stringify(this.state.version)){
      this.checkVersion();
    }
  }

  GetAdvertisementList = async () => {
    this.setState({loading : true})
    try {
      const send_data = {
        "category": "private",
        "service": "GetAdvertisementList",
        "device" : {
          "device_id" : config.deviceInfo.device_id,
          "session_id" : config.sessionInfo.session_id,
        }
      }
      const resp = await axios.post(config.url.apiSvr, 
        {"data" : send_data})
        console.log("GetAdvertisementList : ", resp.data.data.advertisement_list);
      if(resp.data.data && resp.data.data.advertisement_list){
        const {advertisement_list} = resp.data.data;
        let advertisement = advertisement_list ? advertisement_list.map((ad) => {
          return {
            title: ad.name,
            subtitle: ad.name,
            illustration: ad.uri + '?' + new Date(),
            linkUri : ad.link_uri,
          }
        }): [];

        this.setState({advertisement : advertisement, loading:false});
        
      } else {
        this.setState({loading : false, advertisement :[]})
      }
    } catch(e){
      console.log(e); 
      this.setState({loading : false});
    }
  }

  readAllGuide = async () => {
    
    this.setState({loading : true})
    try {
      const send_data = {
        "category": "guide",
        // "service": "ReadAllGuide",
        "service": "GetTodayGuides",
        "device" : {
          "device_id" : config.deviceInfo.device_id,
          "session_id" : config.sessionInfo.session_id,
        }
      }
      const resp = await axios.post(config.url.apiSvr, 
        {"data" : send_data})
      console.log("GetTodayGuides : ", resp.data.data.guide_list);
      let tmpItems = [];
      if(resp.data.data && resp.data.data.guide_list){
        const {guide_list} = resp.data.data;
        tmpItems = [...(guide_list || [])];

        if(config.member && config.member.member_id){
          const send_data2 = {
            "category": "guide",
            "service": "GetMyRecommendGuide",
            "device" : {
              "device_id" : config.deviceInfo.device_id,
              "session_id" : config.sessionInfo.session_id,
            },
            "member": {
              "member_id" : config.member.member_id
            }          
          }

          const resp2 = await axios.post(config.url.apiSvr, 
            {"data" : send_data2});
          // console.log("GetMyRecommendGuide : ", resp2.data.data);
          const {guide_list} = resp2.data.data;
          const guideCount = guide_list ? guide_list.length : 0;
          for(let i=0;i<guideCount;i++){
            const index = tmpItems.findIndex(function findId(item){
              if(item.id === guide_list[i].id) 
                return true})
            if(index !== -1){
              tmpItems[index].like = true;
            }
          }
        }
        this.setState({guide_list : guide_list, loading:false});
        AsyncStorage.getItem("itemId").then(id =>{
          if(!id){
            AsyncStorage.setItem("itemTitle",guide_list[0].title);
            AsyncStorage.setItem("itemId", guide_list[0].id);
          }
        })
        
      } else {
        this.setState({loading : false, guide_list :[]})
      }
    } catch(e){
      console.log(e); 
      this.setState({loading : false});
    } 
  }

  searchGuide = async () =>{
    try {
      this.setState({loading : true})
      const send_data = {
        "category": "guide",
        "service": "SearchGuide",
        "device" : {
          "device_id" : config.deviceInfo.device_id,
          "session_id" : config.sessionInfo.session_id,
        },
        "search": {
          "category": this.state.filters,
            "search_key": this.state.searchValue
        }
      }
      const resp = await axios.post(config.url.apiSvr,
        {"data" : send_data});
        let tmpItems = [];
        // console.log("SearchGuide : ", resp.data.data);
        if(resp.data.data && resp.data.data.guide_list){
          const {guide_list} = resp.data.data;

          tmpItems = [...(guide_list || [])];
          
          if(config.member && config.member.member_id){
            const send_data2 = {
              "category": "guide",
              "service": "GetMyRecommendGuide",
              "device" : {
                "device_id" : config.deviceInfo.device_id,
                "session_id" : config.sessionInfo.session_id,
              },
              "member": {
                "member_id" : config.member.member_id
              }          
            }
            const resp2 = await axios.post(config.url.apiSvr, 
              {"data" : send_data2});
            const {guide_list} = resp2.data.data;
            const guideCount = guide_list ? guide_list.length : 0;
            for(let i=0;i<guideCount;i++){
              const index = tmpItems.findIndex(function findId(item){
                if(item.id === guide_list[i].id) 
                  return true})
              if(index !== -1){
                tmpItems[index].like = true;
              }
            }
          }
          this.setState({ guide_list : tmpItems, loading:false});
        } else {
          this.setState({loading : false, guide_list : []})
        }
    } catch(e){
      console.log("searchGuide : ", e);
      this.setState({loading: false, guide_list: []})
    }
  }

  search = () =>{
    this.props.navigation.navigate("homesearch");
  }

  filter = () =>{
    this.setState({isModalVisible:true})
  }
  toggleFilter = () => {
    this.setState({isModalVisible:!this.state.isModalVisible})
  }

  mainExample (number, title) {
    const { slider1ActiveSlide } = this.state;

    return (
        <View style={styles2.exampleContainer}>
            <Carousel
              ref={c => this._slider1Ref = c}
              data={this.state.advertisement}
              renderItem={this._renderItemWithParallax}
              sliderWidth={sliderWidth}
              itemWidth={itemWidth}
              hasParallaxImages={true}
              firstItem={SLIDER_1_FIRST_ITEM}
              loop={true}
              loopClonesPerSide={3}
              autoplay={true}
              autoplayDelay={1000}
              autoplayInterval={5000}
              onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
            />
           
        </View>
    );
  }
  _renderItemWithParallax ({item, index}, parallaxProps) {
    return (
        <SliderEntry
          data={item}
          even={(index + 1) % 2 === 0}
          parallax={true}
          parallaxProps={parallaxProps}
          index={index}
        />
    );
  }

  _keyExtractor(item, index) {
    return index.toString();
  }

  pressLike = async(id) =>{
    console.log("id", id);
    let newItems = [...this.state.guide_list];
    newItems[id].like = !newItems[id].like;
    this.setState({guide_list : newItems});
    try {
      const send_data = {
        "category": "guide",
        "service": "LikeGuide",
        "device" : {
          "device_id" : config.deviceInfo.device_id,
          "session_id" : config.sessionInfo.session_id,
        },
        "guide": {
          "id": newItems[id].id,
        },
        "member": {
            "member_id": config.member.member_id
        }
      }
      const resp = await axios.post(config.url.apiSvr,
        {"data" : send_data});
      console.log("LikeGuide : ", resp.data);
      let newCount = resp.data.data.guide.recommendation_count
      newItems[id].recommendation_count = newCount ? newCount : 0;
      this.setState({guide_list : newItems});
    } catch(e){
      console.log("LikeGuide : ", e);
    }
  }

  concatStr (strList, style) {
    let newString = '';
    strList && strList.forEach((str, index) => {
      if (newString.length > 0 && index < strList.length) newString += ', ';
      if(style === 'tag') newString += '#';
      newString += str;
    })

    return newString;
  }

  _renderGuide({item, index}) {
    return (
      <TouchableOpacity onPress={()=>{
        console.log('renderGuide')
        AsyncStorage.setItem("itemTitle",item.title);
        AsyncStorage.setItem("itemId",item.id).then(
          this.props.navigation.navigate("listen")
        )
        }} style={{width : "100%", height:100, flexDirection:"row", marginBottom: 20, paddingLeft: 10, paddingRight: 10}}>
          <View style={{flex:3}}>
              {/* {item.image_list &&  */}
                <View style={{width: 100, height: 100, justifyContent: 'center', alignItems: 'center', backgroundColor: item.image_list? '#ffffff': '#fffefb'}}>
                  {item.image_list ?
                    <Image source={{uri : item.image_list[0].path}} style={{width : 100, height:100, borderRadius: 10}}/>
                    : <Image source={config.images.greyDocentogo} style={{width : '50%', height:'50%', resizeMode:"contain", borderRadius: 3}}/>
                  }
                  {config.access_info &&
                    <TouchableOpacity hitSlop={{ top: 16, bottom: 16, left: 16, right: 16 }} style={{position:"absolute", top:5,right:5}} onPress={()=>this.pressLike(index)}>
                      {/*<MaterialCommunityIcons name={item.like ? "heart" : "heart-outline" } size={30} color={item.like ? "#d64409" : "white"} />*/}
                      <Image source={item.like ? config.images.heartOn : config.images.heartOff} style={{width:24, height:20,resizeMode:"contain"}}/>
                    </TouchableOpacity>
                  }
                </View>
              {/* } */}
          </View>
          <View style={{flex:7, marginLeft : 15, flexDirection:"column", paddingTop: 10, paddingBottom: 5}}>
            <View style={{flex:1, flexDirection:"row", alignItems:"center", marginBottom: 3}}>
              <View style={{flexDirection:"row", alignItems:"center"}}>
                <Image source={config.images.docent_home} style={{width : 18, height:18, resizeMode:"contain"}}/>
                <StyledText style={{marginLeft : 5, fontSize:12, color:"#292929"}}>{item.written_by.name}</StyledText>
              </View>
              <View style={{marginLeft: 10, flexDirection:"row", alignItems:"center"}}>
                <Image source={config.images.view_home} style={{width : 18, height:18, resizeMode:"contain"}}/>
                <StyledText style={{marginLeft : 5, fontSize:12, color:"#d64409"}}>{item.subscribe_count ? item.subscribe_count : "0"}</StyledText>
              </View>
              <View style={{marginLeft: 10, flexDirection:"row", alignItems:"center"}}>
                <Image source={config.images.heart_home} style={{width : 18, height:18, resizeMode:"contain"}}/>
                <StyledText style={{marginLeft : 5, fontSize:12, color:"#d64409"}}>{item.recommendation_count ? item.recommendation_count : "0"}</StyledText>
              </View>
              
            </View>
            <View style={{flex:3,  marginTop: 5, marginBottom:5}}>
              <StyledText numberOfLines={2} style={{fontSize:15, color:"#292929"}}>{item.title}</StyledText>
            </View>
            <View style={{flex:1}}>
              <View style={{flexDirection:"row",flexWrap:"wrap"}}>
                <StyledText numberOfLines={1} style={{color:"#707070", fontSize:11}}>
                  {this.concatStr(item.category)}
                </StyledText>
                {/* {item.category && item.category.map((data,i) => {
                  return (
                    <StyledText key={i+"category"} style={{color:"#707070", fontSize:11}}>
                      {item.category.length === i+1 ? data : data + ", "}
                    </StyledText>
                  )
                })} */}
              </View>
            </View>
            {/* <View style={{flex:1}}>
              <View style={{flexDirection:"row",flexWrap:"wrap"}}>
                <StyledText numberOfLines={1} style={{color:"#707070", fontSize:11}}>
                  {this.concatStr(item.tag, 'tag')}
                </StyledText> */}
                {/* {item.tag && item.tag.map((data,i) => {
                  return (
                    <StyledText numberOfLines={1} key={i+"tag"} style={{color:"#707070", fontSize:11}}>
                      {item.tag.length === i+1 ? "#" + data : "#" + data + ", "}
                    </StyledText>
                  )
                })} */}
              {/* </View>
            </View> */}
          </View>
       </TouchableOpacity>)
     
    }

    _renderGuide2({item, index}) {
      console.log("item22 : ",item);
      return (
        <TouchableOpacity onPress={()=>{
          AsyncStorage.setItem("item", item.subject).then(
            this.props.navigation.navigate("listen",
            {item : item})
          )
          }} style={{width : "100%", height:100, flexDirection:"row", marginBottom: 10}}>
            <View style={{flex:3}}>
                {item.iamge_list && <Image source={{uri : item.image_list[0].path}} style={{width : 100, height:100, borderRadius : 5}}/>}
            </View>
            <View style={{flex:7, marginLeft : 10, flexDirection:"column"}}>
              <View style={{flex:1, justifyContent:"center"}}>
                <StyledText style={{fontSize:14, color:"darkgrey"}}>{item.subject}</StyledText>
              </View>
              <View style={{flex:1, flexDirection:"row"}}>
                <View style={{flex:1, alignItems:"center", justifyContent:"center", flexDirection:"row"}}>
                  <Image source={config.images.like} style={{width:20, height:20, resizeMode:"contain"}}/>
                  <StyledText style={{fontSize:12, color:"#d64409"}}>{"  " + (index+10).toString()}</StyledText>
                  <StyledText style={{fontSize:14, color:"grey"}}>{` | `}</StyledText>
                </View>
                <View style={{flex:3, justifyContent:"center"}}>
                  <StyledText style={{fontSize:12, color:"grey"}}>{"￦" + formatInt(parseInt(item.price))}</StyledText>
                </View>
              </View>
              <View style={{flex:1}}>
                <StyledText style={{fontSize:12, color:"grey"}}>{item.locationList[0].address}</StyledText>
              </View>
             
            </View>
         </TouchableOpacity>)
       
      }

    _renderSeparator = () => {
      return (
        <View
          style={{
            height: 1,
            width: '86%',
            backgroundColor: '#CED0CE',
            //marginLeft: '14%',
            alignSelf : "center"
          }}
        />
      );
    }

  onChangeText = (text) =>{
    this.setState({searchValue : text})
  }

  initSearchValue = () => {
    this.setState({searchValue:""})
  }

  searchHeader = () => {
    return (
      <View style={{flex:1, justifyContent:"center", alignItems:"center", backgroundColor:"#fffefb",}}>
        <View style={{flexDirection:"row", backgroundColor:"white", zIndex:1, width:config.deviceInfo.width-10-10, height:50,  borderColor:"#d64409", borderWidth:1, borderRadius:25,justifyContent:"center"}}>
          <View style={{marginLeft:30, width:"100%", height:50,alignItems:"center", flexDirection:"row"}}>
            <View style={{flex:9}}>
              <StyledTextInput  style={{fontSize:14}}
                onChangeText={this.onChangeText}
                placeholder={"검색"} placeholderTextColor="#9e9e9e"
                maxLength={30} value={this.state.searchValue}/>
            </View>
            {
              this.state.searchValue.length > 0 && 
              <TouchableOpacity style={{flex:1,marginRight:60}} onPress={this.initSearchValue}>
                <MaterialIcons name={"close"} size={20} color={"#9e9e9e"}/>
              </TouchableOpacity>
            }
          </View>
          <View style={{height:40, position:"absolute", right:1,top:1}}>
            <TouchableOpacity style={{alignSelf:"flex-end", width:46,height:46,borderRadius:23,backgroundColor:"#d64409", marginRight:1, justifyContent:"center", alignItems:"center"}}
              onPress={()=>{console.log("AAAA"); Keyboard.dismiss(); this.searchGuide()}}
            >
              <StyledText style={{color:"white", fontSize:17, fontFamily: config.defaultBoldFontFamily}}>GO</StyledText>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{position:"absolute", backgroundColor:"white", top:25,width:"100%", height:25, borderTopColor:"#d64409", borderTopWidth:1}}>

        </View>
        
      </View>
      

    );
  }

  confirmFilter = (filterData) =>{
    console.log("filterData : ", filterData);
    this.toggleFilter();
    this.setState({filters:filterData})
    AsyncStorage.setItem("localCategory", JSON.stringify(filterData))
  }
  cancelFilter = () => {
    this.toggleFilter();
    //this.setState({filters:[]});
  }
  showFilters = () => {
    console.log("filters : ", this.state.filters);
    return (
      <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10}}>
            {this.state.filters && this.state.filters.map((item, index) =>{
              return (
                <View key={index} style={{marginLeft:8}}>
                  <View style={{height : 24, width:item.length*20, borderRadius:12, backgroundColor:"#ffe6cc", justifyContent:"center", alignItems:"center"
                    }} 
                    >
                      <StyledText style={{color:"#d64409", fontSize:10}}>{item}</StyledText>
                  </View>
                </View>
              )
            })}
            </View>
    )
  }

  appUpdate = () => {
    if(Platform.OS === "android"){
      BackHandler.exitApp();
    }
    Linking.openURL(this.state.updateUrl);
  }

  notUseMobileData = async () => {
    await AsyncStorage.setItem('localSetting', JSON.stringify({
      'useMoblieData': false
    }))

    config.useMobileData = false;
    this.setState({
      displayNotic: false,
    });
  }

  useMobileData = async () => {
    await AsyncStorage.setItem('localSetting', JSON.stringify({
        'useMoblieData': true
    }))

    config.useMobileData = true;
    this.setState({
      displayNotic: false,
    });
  }

  render() {
    const example1 = this.mainExample(1, '');
    const {isModalVisible, isFocused, loading} = this.state;
    if(!isFocused){
      return <></>
    }
    
    return (
      <View style={styles.container}>
        {(loading) && <Loading color={config.colors.primary}/>}
        <StatusBar backgroundColor={"#fffefb"} barStyle='dark-content'/>
        <Modal isVisible={isModalVisible}
              onBackdropPress={this.toggleFilter}
              avoidKeyboard={true}
              style={styles.modalStyle} >
          <View style={{flex:1, margin:10, marginTop: 20, marginBottom: 20, backgroundColor:"white", borderRadius:10}}>
            <SearchFilter confirm={this.confirmFilter} cancel={this.cancelFilter}/>
          </View>
        </Modal>
        <Confirm visible={this.state.displayNotic}
            onBackdropPress={this.pressReportButton}
            message={'데이터 네트워크 상태에서 서비스를 이용하실 경우 데이터 이용료가\n발생할 수 있습니다.\nWi-Fi로 서비스를 이용하시겠습니까? '}
            leftButtonLabel={'항상 이용'}
            rightButtonLabel={'Wi-Fi로 이용'}
            onLeftButtonPress={this.useMobileData}
            onRightButtonPress={this.notUseMobileData} />
        <Confirm visible={this.state.showUpdatePopup}
            onBackdropPress={this.pressReportButton}
            message={'새로운 버전이 있습니다.\n업데이트를 진행하시겠습니까?'}
            leftButtonLabel={'나중에'}
            rightButtonLabel={'업데이트하기'}
            onLeftButtonPress={() => {
              this.setState({
                showUpdatePopup: false,
              });
            }}
            onRightButtonPress={this.appUpdate} />
        <Confirm visible={this.state.showUpdatePopup2}
            oneButton={true}
            onBackdropPress={this.pressReportButton}
            message={'필수 업데이트가 있습니다.'}
            centerButtonLabel={'업데이트하기'}
            onCenterButtonPress={this.appUpdate} />
        <ScrollView
          stickyHeaderIndices={[1]} keyboardShouldPersistTaps={"handled"}
        >
          <View style={styles.section2}>
          { example1 }
          </View>
          {/*<View style={styles.section3}>
            <ImageBackground source={config.images.toGoBackground} style={{width : "100%", height : "100%"}}>
              <View style={{flex:1, flexDirection : "row"}}>
              <View style={{flex:7, alignItems:"center", justifyContent:"center"}}>
                <StyledText style={{fontSize: 16, color:"white"}}>오늘의 픽!   #관광 #문화재</StyledText>
              </View>
              <TouchableOpacity style={{flex:3, justifyContent:"center"}} onPress={()=>{alert("나중에")}}>
                <Image source={config.images.toGoIcon} style={{width:90, height:50, resizeMode:"contain"}} />
              </TouchableOpacity>
              </View>
            </ImageBackground>
          </View>*/}
          <View>
          <View style={styles.section3}>
            {this.searchHeader()}
              
          </View>
          <View style={{height:30, paddingTop:10,backgroundColor:"white"}}>
              {this.showFilters()}
            </View>
          </View>
          
          <View style={styles.section4}>
          <FlatList
              data={this.state.guide_list} 
              keyExtractor={(item, index) => this._keyExtractor(item, index)}
              renderItem={(item, index) => this._renderGuide(item, index,)}
              //ItemSeparatorComponent={this._renderSeparator}
              windowSize={3}
              //keyboardShouldPersistTaps="always"
            />
            {/*this.state.items.length > 0 &&
              <TouchableOpacity onPress={()=>{this.props.navigation.navigate("listen",
              {item : this.state.items[0]})}} style={{width : "100%", height:100, flexDirection:"row"}}>
                <View style={{flex:3}}>
                    {this.state.items[0].picture && <Image source={{uri : 'data:image/png;base64,' + this.state.items[0].picture.base64_image}} style={{width : 100, height:100, borderRadius : 5}}/>}
                </View>
                <View style={{flex:7, flexDirection:"column"}}>
                  <StyledText style={{fontSize:20}}>{this.state.items[0].subject}</StyledText>
                  <StyledText style={{fontSize:14, color:"grey"}}>{this.state.items[0].locationList[0].location.address_name}</StyledText>
                </View>
              </TouchableOpacity>
            */}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles =StyleSheet.create({
  container : {
    flex:1,
    backgroundColor:"#ffffff"
  },
  section1 : {
    flex:1.5,
    flexDirection:"row",
    marginBottom : 0,
  },
  section2 : {
    height:240, 
    backgroundColor:"#fffefb",
    
  },
  section3 : {
    //backgroundColor:"#ffb468",
    //flex:1,
    height:50,
    
    backgroundColor:"blue",
    
    //flexDirection:"row",
  },
  section4 : {
    margin : 10,
    flex:3.5,
  },
});


const colors = {
  black: '#1a1917',
  gray: '#888888',
  background1: '#B721FF',
  background2: '#21D4FD'
};

const styles2 = StyleSheet.create({
  safeArea: {
      flex: 1,
      backgroundColor: colors.black
  },
  container: {
      flex: 1,
      backgroundColor: colors.background1
  },
  gradient: {
      ...StyleSheet.absoluteFillObject
  },
  scrollview: {
      flex: 1
  },
  exampleContainer: {
      // paddingVertical: 0,
      paddingTop: 10,
  },
  exampleContainerDark: {
      backgroundColor: colors.black
  },
  exampleContainerLight: {
      backgroundColor: 'white'
  },
  title: {
      paddingHorizontal: 0,
      backgroundColor: 'transparent',
      color: 'rgba(255, 255, 255, 0.9)',
      fontSize: 20,
      fontWeight: 'bold',
      textAlign: 'center'
  },
  titleDark: {
      color: colors.black
  },
  subtitle: {
      marginTop: 5,
      paddingHorizontal: 0,
      backgroundColor: 'transparent',
      color: 'rgba(255, 255, 255, 0.75)',
      fontSize: 13,
      fontStyle: 'italic',
      textAlign: 'center'
  },
  slider: {
      marginTop: 15,
      overflow: 'visible' // for custom animations
  },
  sliderContentContainer: {
      paddingVertical: 0 // for custom animation
  },
  paginationContainer: {
      paddingVertical: -8,
      //backgroundColor: 'rgba(0,0,0,0.75)'
  },
  paginationDot: {
      width: 8,
      height: 8,
      borderRadius: 4,
      marginHorizontal: 8
  },
});


