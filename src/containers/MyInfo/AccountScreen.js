import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Alert,
    View,
    Image,
    TouchableOpacity,
    SafeAreaView,
    StatusBar,
    Switch,
    Appearance, Keyboard,
} from 'react-native';
import AsyncStorage from "@react-native-community/async-storage";
import VersionCheck from 'react-native-version-check';

import config from 'src/config';
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import {StyledText, StyledTextInput} from 'src/components/styledComponents';
import Kakao from "../Login/Kakao"
import Apple from "../Login/Apple"
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Confirm from "src/components/Popup/Confirm";
import showToast from 'src/components/Toast';
//import Naver from "../Login/Naver"
//import Facebook from "../Login/Facebook"

import {getStatusBarHeight} from "react-native-status-bar-height";



export default class AccountScreen extends Component {
    //닉네임변경시 저장버튼 활성화

    static navigationOptions =  ({ navigation }) => {
    return {
      headerLeft: () => <BackIcon navigation={navigation} color={"black"}/>,
      headerTitle :() => <HeaderTitle style={[styles.headerTitle]} title={"나의 도슨투고"} />,
      headerRight: () => <View style={{flex:1}}></View>,/*
        let button = <></>;
        let buttonDisabled = navigation.getParam('disabled');
        // if (navigation.getParam('submitText'))
          button = (
            <TouchableOpacity
                disabled={buttonDisabled}
                style={{marginRight: 15}}
                onPress={navigation.getParam('onPressSubmit')}>
                <StyledText style={{fontSize: config.fontSize._60px, color: buttonDisabled? config.colors.greyColor:config.colors.baseColor2}}>
                    저장
                </StyledText>
            </TouchableOpacity>
          );

            return button;*/
        
        headerStyle : {
            backgroundColor: "#ffffff",
            //borderBottomWidth: 0.5,
            elevation: 0,       //remove shadow on Android
            shadowOpacity: 0,   //remove shadow on iOS
        }
        }
    }



    state = {
        nickname: '',
        orgNickname: '',
        introduce: '',
        openConfirm: false,
        isFocused: false,
        showErrorDialog: false,
        onlyWifi: true,
        beep:true,
        autoPlay:true,
    }

    logoutRef =  React.createRef();

    onPressSubmit = () => {
        console.log("닉네임 : ", this.state.nickname.length)
        if(!this.state.nickname || this.state.nickname.length>10){
            this.setState({showErrorDialog:true});
            return;
        }
        
        let send_data = {
            "device": {
                "device_id": config.deviceInfo.device_id,
                "session_id": config.sessionInfo.session_id
            },
            "access_token": config.access_info.access_token,
            "member": {
                "member_id": config.member.member_id,
                "nickname": this.state.nickname,
                "introduction": this.state.introduce,
            }
        }
        console.log('XXXXXXXXXXXXX send_Data', send_data, JSON.stringify(send_data))

        fetch(config.url.apiSvr + "/private/UpdateProfile", {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
        .then(res => {
            if (res) {
            return res.json()
            }
        })
        .then(json => {
            if (json.err) {
                console.log('/private/UpdateProfile error', json);
            }
            else {
                console.log("/private/UpdateProfile success : ", json.data);
                config.access_info.profile.nickname = this.state.nickname;
                config.member.nickname = this.state.nickname;
                config.member.introduce = this.state.introduce;
                this.props.navigation.setParams({disabled: true})
                this.textInput && this.textInput.getInnerRef().blur();
            }
            this.props.navigation.goBack();
        }).catch(err => {
            console.log("/private/UpdateProfile err : ", err)
        })

    }


    componentDidMount() {
        const {access_info} = this.props.navigation.state.params;
        const profile = access_info ? access_info.profile: '';

        const {navigation} = this.props;
        this.subs = [
            navigation.addListener("didFocus", () => {
                this.setState({ isFocused: true });
            }),
            navigation.addListener("willBlur", () => {
                this.setState({ isFocused: false,});
            }),
        ];

        console.log('conifg useMobileData:', config.useMobileData)
        this.setState({
            nickname: config.member.nickname || profile.nickname,
            orgNickname: config.member.nickname || profile.nickname,
            onlyWifi: !config.useMobileData,
            autoPlay : config.useAutoPlay,
            beep : config.useBeep
        })

    }
    componentWillUnmount() {
        this.subs && this.subs.forEach((sub) => {
            sub.remove();
        });
    }

    changeRegisterButton = () => {
        let disabled = true;
        if (this.state.nickname !== this.state.orgNickname)
            disabled = false;
        this.props.navigation.setParams({disabled, onPressSubmit: this.onPressSubmit})
      };


    componentDidUpdate(prevProps, prevState) {
        if (this.state.nickname !== prevState.nickname) {
            this.changeRegisterButton()
        }
    }

    logout = async () => {
        const {access_info} = config;
        let send_data = {
            "device": {
                "device_id": config.deviceInfo.device_id,
                "session_id": config.sessionInfo.session_id
            },
            "access_token": access_info.access_token,
            "category": "private",
            "service": "Logout"
        }
        await AsyncStorage.removeItem("access_info")
        config.member = {
          member_id: '',
          nickname: '',
        };
        config.access_info = null;
        fetch(config.url.apiSvr, {
            method: 'post',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                'data': send_data,
            }),
        })
            .then(res => {
                if (res) {
                    return res.json()
                }
            })
            .then(json => {
                if (json.err) {
                    console.log('/private/login error', json)
                }
                else {
                    console.log("/private/Logout success : ", json.data)
                }
                this.props.navigation.goBack();
            }).catch(err => {
            console.log("/private/Login err : ", err)
        })
    }

    openConfirm = () => {
        this.setState({
            openConfirm: true,
        })
        
    }

    onDialog = (result) => {
        if (result) { //로그아웃 - 예
            this.setState({
                openConfirm: false,
            });
            this.logoutRef.current.logout()
        }
        else { //로그아웃 - 아니요
            this.setState({
                openConfirm: false,
            });
        }

    }

    renderOpenConfirm = () => {
        Alert.alert(
            "",
            "정말 로그아웃 하시겠습니까?",
            [
              {text:"아니오", onPress: ()=>{
                this.onDialog(false)        
              }},
              {text:"예", onPress: ()=>{
                this.onDialog(true)        
              }}
            ],
            
            );
        
        

    }

    onChangeText = (text) => {
        this.setState({
            nickname: text
        })
    }
    onChange = (text, name) => {
        this.setState({
            [name]: text,
        })
    }
    /*
    render() {
        const {access_info} = config;

        const loginComponent = {
          kakao: Kakao,
          apple: Apple
          //naver: Naver,
          //facebook: Facebook
        }

        const LogoutButton = loginComponent[access_info.type];
        return (
            <SafeAreaView style={styles.container}>
                {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
                <View style={styles.profile}>
                    <Image style={styles.profileImage} source={config.images.avatar}></Image>
                    <View style={styles.inputField}>
                        <StyledTextInput style={styles.profileText}
                                        ref={(input) => this.textInput = input}
                                        onChangeText={this.onChangeText}
                        >
                            {this.state.nickname}
                        </StyledTextInput>
                        <TouchableOpacity style={styles.deleteButtonWrapper}
                            onPress={()=>this.setState({nickname:''})}>
                            <Image style={styles.deleteButton} source={require('src/images/delete.png')}/>
                        </TouchableOpacity>
                    </View>
                    <StyledText style={styles.notice}>최소 2자 이상 최대 10자 이내로 작성해 주세요.</StyledText>
                </View>
                <View style={styles.bottom}>
                    <LogoutButton do='logout'
                        ref={this.logoutRef}
                        onPress={this.openConfirm}
                        textStyle={{fontSize: config.fontSize._52px, color: config.colors.greyColor3}}
                        token={access_info.access_token}
                        callback={this.logout}
                    />
                </View>
                {this.state.openConfirm? this.renderOpenConfirm(): null}
            </SafeAreaView>

        )
    }
    */
    componentDidUpdate(prevProps, prevState){
        if(prevState.autoPlay !== this.state.autoPlay){
          if(!this.state.autoPlay){
            AsyncStorage.setItem('localSetting', JSON.stringify({
                "useBeep": false
            })).then(()=>{
                this.setState({beep: false})
                config.useBeep = false;
            })
          }
        }
      }

    toggleNetSwitch = async () => {
      console.log('toggleSwitch:', this.state.onlyWifi)
      config.useMobileData = this.state.onlyWifi;
      await AsyncStorage.setItem('localSetting', JSON.stringify({
        "useMoblieData": this.state.onlyWifi
    }))
      this.setState({
        onlyWifi: !this.state.onlyWifi
      })
    }
    toggleAutoPlay = async (value) => {
        config.useAutoPlay = value;
        await AsyncStorage.setItem('localSetting', JSON.stringify({
          "useAutoPlay": value
        }))
        this.setState({
            autoPlay: value
        })
    }
    toggleBeep = async (value) => {

        if(!this.state.autoPlay){
            showToast('자동 재생 설정이 켜 있을 경우에만 유효한 설정입니다.')
            return;
        }

        config.useBeep = value;
        await AsyncStorage.setItem('localSetting', JSON.stringify({
            "useBeep": value
        }))
        this.setState({
            beep: value
        })
    }

    closeErrDialog = () => {
        this.setState({showErrorDialog:false})
    }

   render() {
    const {access_info, version} = config;

    const loginComponent = {
      kakao: Kakao,
      apple: Apple
      //naver: Naver,
      //facebook: Facebook
    }
    const currentVersion = VersionCheck.getCurrentVersion();

    // const LogoutButton = loginComponent[access_info.type];
    return (
        <SafeAreaView style={styles.container}>
            {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
            <Confirm visible={this.state.showErrorDialog}
                oneButton={true}
                onBackdropPress={this.closeErrDialog}
                message={'닉네임은 1글자 이상 10글자 이하로 입력하셔야 합니다.'}
                centerButtonLabel={'확인'}
                onCenterButtonPress={this.closeErrDialog} />
            { access_info &&
              <View style={{height: 200}}>
                {/* <View style={{height:40, width:"100%", backgroundColor:"#f2fcfb", alignItems:"center", paddingLeft:20, flexDirection:"row"}}> */}
                <View style={{height:40, width:"100%", alignItems:"center", paddingLeft:20, flexDirection:"row"}}>
                  <StyledText style={styles.menuName}>
                      {"내 정보"}
                  </StyledText>
                  <TouchableOpacity style={{position:"absolute", right:20}} /*onPress={this.props.navigation.getParam('onPressSubmit')}*/ onPress={this.onPressSubmit}>
                      <StyledText style={{color:"#d64409"}}>{"수정"}</StyledText>
                  </TouchableOpacity>
                </View>
                <View style={styles.profile}>
                    <View style={styles.inputField}>
                        <StyledText style={styles.inputTitle} >
                            {"이메일"}
                        </StyledText>
                        <StyledTextInput editable={false} 
                            style={styles.textInput} 
                            defaultValue={access_info.profile.email} />
                    </View>
                    <View style={styles.inputField}>
                        <StyledText style={styles.inputTitle} >
                            {"닉네임"}
                        </StyledText>
                        <StyledTextInput
                            style={styles.textInput} 
                            defaultValue={this.state.nickname}
                            onChangeText={(text) => this.onChange(text, 'nickname')} />
                        <TouchableOpacity style={styles.deleteButtonWrapper}
                            onPress={()=>this.setState({nickname:''})}>
                            <Image style={styles.deleteButton} source={require('src/images/delete.png')}/>
                        </TouchableOpacity>
                    </View>
                    {/* <View style={styles.inputField}>
                        <StyledText style={styles.inputTitle} >
                            {"소개"}
                        </StyledText>
                        <StyledTextInput style={styles.textInput}
                            onChangeText={(text) => this.onChange(text, 'introduce')} >
                            {this.state.introduce}
                        </StyledTextInput>
                        <TouchableOpacity style={styles.deleteButtonWrapper}
                            onPress={()=>this.setState({introduce:''})}>
                            <Image style={styles.deleteButton} source={require('src/images/delete.png')}/>
                        </TouchableOpacity>
                    </View> */}
                    {/* <Image style={styles.profileImage} source={config.images.avatar}></Image>
                    <View style={styles.inputField}>
                        <StyledTextInput style={styles.profileText}
                                        ref={(input) => this.textInput = input}
                                        onChangeText={this.onChangeText}
                        >
                            {this.state.nickname}
                        </StyledTextInput>
                        <TouchableOpacity style={styles.deleteButtonWrapper}
                            onPress={()=>this.setState({nickname:''})}>
                            <Image style={styles.deleteButton} source={require('src/images/delete.png')}/>
                        </TouchableOpacity>
                    </View>
                    <StyledText style={styles.notice}>최소 2자 이상 최대 10자 이내로 작성해 주세요.</StyledText> */}
                </View>
                <View style={[styles.sectionLine, {marginTop: 30}]}></View>
              </View>
            }
            <View style={styles.settingMenu}>
              <StyledText style={styles.menuName} >{"버전 정보"}</StyledText>
              <View>
                <StyledText style={styles.menuSubText} >{"현재 " + currentVersion}</StyledText>
                <StyledText style={styles.menuSubText} >{"최신 " + version.android}</StyledText>
              </View>
            </View>
            <View style={styles.menuCategory}>
              <StyledText style={styles.category} >{"재생 설정"}</StyledText>
              <View style={styles.submenu}>
                <StyledText style={styles.menuName} >{"자동 재생"}</StyledText>
                <View>
                    <Switch
                    style={styles.switch}
                    trackColor={{false : "#bfbfbf", true : "#26675e"}}
                    thumbColor={"#ffffff"}
                    thumbSize={5}
                    ios_backgroundColor="#ffffff"
                    onValueChange={this.toggleAutoPlay}
                    value={this.state.autoPlay}
                    />
                </View>
              </View>
              {/* <View style={styles.submenu}>
                <StyledText style={styles.menuName} >{"자동 재생 중 비프음 재생"}</StyledText>
                <View>
                    <Switch
                    style={styles.switch}
                    trackColor={{false : "#bfbfbf", true : "#26675e"}}
                    thumbColor={"#ffffff"}
                    thumbSize={5}
                    ios_backgroundColor="#ffffff"
                    onValueChange={this.toggleBeep}
                    value={this.state.beep}
                    />
                </View>
              </View> */}
              <View style={styles.submenu}>
                <StyledText style={styles.menuName} >{"WiFi 환경에서만 재생"}</StyledText>
                <View>
                    <Switch
                    style={styles.switch}
                    trackColor={{false : "#bfbfbf", true : "#26675e"}}
                    thumbColor={"#ffffff"}
                    thumbSize={5}
                    ios_backgroundColor="#ffffff"
                    onValueChange={this.toggleNetSwitch}
                    value={this.state.onlyWifi}
                    />
                </View>
              </View>
            </View>
            {/*<TouchableOpacity style={styles.settingMenu} onPress={()=>this.props.navigation.navigate("sendMailScreen")}>
                <StyledText style={styles.menuName} >{"문의글 보내기"}</StyledText>
                
                    <Icon
                        style={{marginLeft : 5,}}
                        name="chevron-right"
                        color="#bfbfbf"
                        size={36}
                        underlayColor="#4BA6F8"
                        
                    />
                
            </TouchableOpacity>*/}
            {/* <View style={styles.bottom}>
                <LogoutButton do='logout'
                    ref={this.logoutRef}
                    onPress={this.openConfirm}
                    textStyle={{fontSize: config.fontSize._52px, color: config.colors.greyColor3}}
                    token={access_info.access_token}
                    callback={this.logout}
                />
            </View> */}
            {this.state.openConfirm? this.renderOpenConfirm(): null}
        </SafeAreaView>

    )
}
}



const styles=StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: config.colors.greyColor2
        backgroundColor: '#ffffff',
    },
    profile: {
        flex: 1,
        height: config.size._1080px,
        // flex: 1,
        // paddingTop: 30,
        alignItems: 'center',
        alignContent: 'center',
        // justifyContent: 'space-around',
        // backgroundColor: 'white',
        // borderBottomWidth: config.size._2px,
        // borderTopWidth: 1,
        borderColor: config.colors.lineColor,
    },
    profileImage: {
        // flex: 1,
        width: config.size._240px,
        height: config.size._240px,
        borderRadius: config.size._240px
    },
    inputField: {
        flexDirection: 'row',
        height: 50,
        // height: config.size._220px,
        // width: '100%',
        // alignItems: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderBottomWidth: 1,
        // borderColor: config.colors.lineColor,
        // borderWidth: 1,
        // borderColor: 'red'

    },

    profileText: {
        // flex: 1,
        // width: '100%',
        height: '70%',
        // minWidth: config.size._840px,
        // height: config.size._64px,
        paddingBottom: 8,
        paddingLeft: config.size._84px,
        paddingRight: config.size._8px,
        color: 'black',
        fontSize: config.size._64px,

        textAlignVertical: 'center',
        textAlign: 'center',
        // borderWidth: 1,
        // borderColor: 'red'
    },
    deleteButtonWrapper: {
        position: 'absolute',
        left: 100,
        height: '100%',
        // height: '65%',
        alignItems: 'center',
        // alignContent: 'center',
        justifyContent: 'center',
    },
    deleteButton: {
        width: config.size._40px,
        height: config.size._40px,
    },
    notice: {
        paddingTop: config.size._28px,
        fontSize: config.fontSize._44px,
        color: '#757575'
    },
    bottom: {
        paddingTop: config.size._64px,
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        alignSelf: 'center',
        fontSize: 18,
        //fontWeight: 'bold',
        fontFamily:config.defaultBoldFontFamily
    },
    textInput: {
        // minWidth: config.size._840px,
        width: '60%',
        height: '90%',
        borderBottomWidth: 1,
        textAlign: 'right',
        borderColor: '#bfbfbf',
        color: 'black'
    },
    inputTitle: {
        width: 90,
        color: config.colors.secondary,
        fontFamily: config.defaultBoldFontFamily
    },
    sectionLine: {
      height: 15,
      backgroundColor:"#f2fcfb",
      marginBottom: 5
    },
    menuCategory: {
        paddingVertical: 20,
        paddingHorizontal: 20,
        borderBottomColor: "#bfbfbf",
        borderBottomWidth: 0.5,
    },
    category: {
        paddingBottom: 20,
        fontFamily: config.defaultBoldFontFamily,
    },
    submenu: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingLeft: 20,
        marginBottom:5
    },
    settingMenu: {
      height: 70,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: 20,
      borderBottomColor: "#bfbfbf",
      borderBottomWidth: 0.5,
    },
    menuName: {
      fontFamily: config.defaultBoldFontFamily,
    },
    menuSubText: {
      fontSize: 12,
      color: config.colors.defaultText
    },
    switch: {
        transform: Platform.OS === "android" ? [{scaleX : 1.3}, {scaleY: 1.3}] : [{scaleX : 1.0}, {scaleY: 1.0}]
    }
  });
