import React, {Component}  from "react";
import {View, StatusBar, ScrollView, Image, StyleSheet, TouchableOpacity, Alert} from "react-native";
import config from 'src/config';
import {BackIcon, HeaderTitle} from 'src/components/header';
import AsyncStorage from "@react-native-community/async-storage";

import {StyledText} from 'src/components/styledComponents';
import Profile from './Profile';
import axios from 'axios';
import Confirm from "src/components/Popup/Confirm";

import Kakao from "../Login/Kakao"
import Apple from "../Login/Apple"

export default class AboutMyself extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: () => null,
      headerTitle : () => <HeaderTitle style={{fontSize: 16, color:"#000000"}} title={"나의 도슨투고"} />,
      // headerTitle : () => {
      //   return (
      //     <View style={{
      //       alignItems: "center",
      //       justifyContent:"center",
      //       flex : 1,}}>
      //       <StyledText style={{fontSize: 20, color:"#000000"}}>{"나의 도슨트"}</StyledText>
      //     </View>
      // )},
      headerRight: () => null,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0,   //remove shadow on iOS
      }
    }
  };

  state = {
    usingDocent : "",
    isLogin: false,
    isFocused: false,
    subscribedGuideList : [],
    recommenedGuideList: [],
    openConfirm: false,
  }

  logoutRef =  React.createRef();

  logout = async () => {
    const {access_info} = config;
    let send_data = {
        "device": {
            "device_id": config.deviceInfo.device_id,
            "session_id": config.sessionInfo.session_id
        },
        "access_token": access_info.access_token,
        "category": "private",
        "service": "Logout"
    }
    await AsyncStorage.removeItem("access_info")
    config.member = {
      member_id: '',
      nickname: '',
    };
    config.access_info = null;
    fetch(config.url.apiSvr, {
        method: 'post',
        headers: {'Content-type': 'application/json'},
        body: JSON.stringify({
            'data': send_data,
        }),
    })
        .then(res => {
            if (res) {
                return res.json()
            }
        })
        .then(json => {
            if (json.err) {
                console.log('/private/login error', json)
            }
            else {
                console.log("/private/Logout success : ", json.data)
                this.setState({
                  isLogin: false,
                })
            }
            // this.props.navigation.goBack();
        }).catch(err => {
        console.log("/private/Login err : ", err)
    })
  }

  openConfirm = () => {
    this.setState({
        openConfirm: true,
    })
    
  }

  onDialog = (result) => {
      if (result) { //로그아웃 - 예
          this.setState({
              openConfirm: false,
          });
          this.logoutRef.current.logout()
      }
      else { //로그아웃 - 아니요
          this.setState({
              openConfirm: false,
          });
      }

  }

  renderOpenConfirm = () => {
    Alert.alert(
        "",
        "정말 로그아웃 하시겠습니까?",
        [
          {text:"아니오", onPress: ()=>{
            this.onDialog(false)        
          }},
          {text:"예", onPress: ()=>{
            this.onDialog(true)        
          }}
        ],
        
    );
  }

  checkLoginStatus = () => {
    if (!config.access_info) {
      this.setState({
        isLogin: false
      })
    } else {
      this.setState({
        isLogin: true
      })
    }
  }

  componentDidMount() {
    console.log("AboutMyself componentDidMount");
    
    this.subs = [
      this.props.navigation.addListener("didFocus", () => {
        /*AsyncStorage.getItem("item").then(value => {
          console.log("sub :", value)
          if(value){
            this.setState({usingDocent : value})
          }
        })*/
        console.log("AboutMyself willFocus");
        this.setState({isFocused : true})
      }),
      this.props.navigation.addListener("willFocus", () => {
        console.log("AboutMyself willFocus");
      }),
      this.props.navigation.addListener("willBlur", () => {
        console.log("AboutMyself willBlur");
        this.setState({isFocused : false})
      }),
      this.props.navigation.addListener("didBlur", () => {
        console.log("AboutMyself didBlur");
      }),
    ];
  }
  componentWillUnmount() {
    console.log("AboutMyself componentDidUnmount()");
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  componentDidUpdate(prevProps, prevState){
    const { isFocused } = this.state;
    if(prevState.isFocused != isFocused && isFocused){
      this.checkLoginStatus();
      this.getMySubscribeGuides();
      this.getMyRecommendGuide();
    }
  }

  getMySubscribeGuides = async () =>{
    if(!config.member || !config.member.member_id){
      this.setState({subscribedGuideList : []})
      return;
    }
    try {
      const send_data = {
        "category": "guide",
        "service": "GetMySubscribeGuide",
        "device" : {
          "device_id" : config.deviceInfo.device_id,
          "session_id" : config.sessionInfo.session_id,
        },
        "member": {
          "member_id" : config.member.member_id
        }
      }
      const resp = await axios.post(config.url.apiSvr,
        {"data" : send_data});
      console.log("GetMySubscribeGuide : ", resp.data.data);
      const {guide_list} = resp.data.data;
      this.setState({subscribedGuideList : guide_list})
    } catch(e){
      console.log("GetMySubscribeGuide Error : ", e);
    }
  }

  getMyRecommendGuide = async () => {
    if(!config.member || !config.member.member_id){
      this.setState({recommenedGuideList : []})
      return;
    }
    try {
      const send_data = {
        "category": "guide",
        "service": "GetMyRecommendGuide",
        "device" : {
          "device_id" : config.deviceInfo.device_id,
          "session_id" : config.sessionInfo.session_id,
        },
        "member": {
          "member_id" : config.member.member_id
        }
      }
      const resp = await axios.post(config.url.apiSvr,
        {"data" : send_data});
      console.log("GetMyRecommendGuide : ", resp.data.data);
      const {guide_list} = resp.data.data;
      this.setState({recommenedGuideList : guide_list})
    } catch(e){
      console.log("GetMyRecommendGuide Error : ", e);
    }
  }

  onPressAccount = () => {
    if (this.state.isLogin) {
      /*this.props.navigation.navigate('accountScreen', {
        access_info: config.access_info
      });*/
      this.pressSetting();
    } else {
      this.props.navigation.navigate('loginScreen');

    }

  };

  onPressPrivacyPolicy = () => {
    this.props.navigation.navigate("webViewScreen", {
      uri : config.url.webPage + "/privacy-policy/",
      title: '개인정보처리방침'
    });
  }

  onPressService = () => {
    this.props.navigation.navigate("webViewScreen", {
      uri : config.url.webPage + "/service/",
      title: '이용약관'
    });
  }

  renderMySubscribeGuides = () => {
    const {subscribedGuideList} = this.state;
    if(!subscribedGuideList){
      return <></>
    }
    return subscribedGuideList.map(guide => {
      console.log("guide.title : ", guide.title)
      return (
        // <TouchableOpacity key={guide.id} style={{width:100, height: 100 * 3/2, marginRight:10}} onPress={()=>this.goToDocent(guide)}>
        <TouchableOpacity key={guide.id} style={{width:100, height: 100 * 3/2, marginRight: 15, }} onPress={()=>this.goToDocent(guide)}>
          {guide.image_list ?
            <Image source={{uri : guide.image_list[0].path}} style={{width:100,height:100}}/>
            : <View style={styles.defaultSnapshot}>
              <Image source={config.images.greyDocentogo} style={{width : '50%', height:'50%', resizeMode:"contain", borderRadius: 3}}/>
            </View>
          }
          <StyledText numberOfLines={2} style={styles.guideName} >{guide.title}</StyledText>
        </TouchableOpacity>
      )
    })
  }
  goToDocent = (item) =>
  {
    AsyncStorage.setItem("itemTitle",item.title);
    AsyncStorage.setItem("itemId",item.id).then(
      this.props.navigation.navigate("listen")
    )
  }
  renderMyRecommendGuides = () => {
    const {recommenedGuideList} = this.state;
    if(!recommenedGuideList){
      return <></>
    }
    return recommenedGuideList.map(guide => {
      console.log("guide.title : ", guide.title)
      return (
        // <TouchableOpacity key={guide.id} style={{width:100, height: 100 * 3/2, marginRight:10}} onPress={()=>this.goToDocent(guide)}>
        <TouchableOpacity key={guide.id} style={{width:100, height: '100%', marginRight: 15}} onPress={()=>this.goToDocent(guide)}>
          {guide.image_list ?
            <Image source={{uri : guide.image_list[0].path}} style={{width:100,height:100}}/>
            : <View style={styles.defaultSnapshot}>
              <Image source={config.images.greyDocentogo} style={{width : '50%', height:'50%', resizeMode:"contain", borderRadius: 3}}/>
            </View>
          }
          <StyledText numberOfLines={2} style={styles.guideName} >{guide.title}</StyledText>
        </TouchableOpacity>
      )
    })
  }

  pressSetting = ()=>{
    // if (this.state.isLogin) {
      console.log('pressSetting')
      this.props.navigation.navigate('accountScreen', {
        access_info: config.access_info
      });
    // }
  }

  render() {
    const {access_info} = config;
    const {subscribedGuideList, recommenedGuideList} = this.state;
    const profile = access_info? access_info.profile: {};
    const countOfMySubscribed = subscribedGuideList ? subscribedGuideList.length : 0;
    const countOfMyRecommended = recommenedGuideList ? recommenedGuideList.length : 0;

    const loginComponent = {
      kakao: Kakao,
      apple: Apple
      //naver: Naver,
      //facebook: Facebook
    }

    const LogoutButton = this.state.isLogin && loginComponent[access_info.type];
    return (
      <View style={styles.container}>
        <Confirm visible={this.state.openConfirm}
          onBackdropPress={this.pressReportButton}
          message={"로그아웃 하시겠습니까?"}
          rightButtonLabel={"로그아웃"}
          onLeftButtonPress={() => this.onDialog(false)}
          onRightButtonPress={() => this.onDialog(true)} />
        {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'#ffffff'}/>}
        {
          <ScrollView style={styles.body} contentContainerStyle={!profile.nickname && styles.logoutProfile}>
            <View>
              {/* <View style={styles.header}>
                <StyledText style={{color : "#707070", fontFamily: config.defaultBoldFontFamily}}>
                  {"내 계정"}
                </StyledText>
                <TouchableOpacity style={{position:"absolute", right:20}} onPress={this.pressSetting}>
                  <Image source={config.images.setting} style={{width:24, height:24}}/>
                </TouchableOpacity>
              </View> */}
              <View style={{height:130, padding : 10, paddingLeft:20 }}>
                <View style={{height:24, alignItems: "flex-end", paddingRight: 15}}>
                  <TouchableOpacity onPress={this.pressSetting}>
                    <Image source={config.images.setting} style={{width:24, height:24}}/>
                  </TouchableOpacity>
                </View>
                <TouchableOpacity style={styles.profile}
                onPress={this.onPressAccount}>
                  <Profile profile={profile} />
                </TouchableOpacity>
                {
                  this.state.isLogin && 
                  <View style={styles.logout}>
                    {/* <StyledText style={styles.logoutText}>{"로그아웃"}</StyledText> */}
                    <LogoutButton do='logout'
                      textDecorationLine='underline'
                      ref={this.logoutRef}
                      onPress={this.openConfirm}
                      textStyle={{fontSize: config.fontSize._48px, color: config.colors.newButtonColor, textDecorationLine: 'underline'}}
                      token={access_info.access_token}
                      callback={this.logout}
                      />
                  </View>
                }
                {/* <TouchableOpacity style={styles.deleteAccount}>
                  <StyledText style={styles.delAccountText}>{"탈퇴하기"}</StyledText>
                </TouchableOpacity> */}
              </View>
            </View>
            {
              profile.nickname &&
              <View>
                <View style={{height:220}}>
                  <View style={styles.sectionLine}></View>
                  <View style={styles.titleBar}>
                    <StyledText style={{color : "#707070", fontFamily: config.defaultBoldFontFamily }}>{"내가 떠난 여행"} </StyledText>
                    <StyledText style={{color: "#d64409"}}> {" (" + countOfMySubscribed + "개)"}</StyledText>
                  </View>
                  <ScrollView style={{height:180, paddingLeft:15, paddingRight:15}}  horizontal={true}>
                    {this.renderMySubscribeGuides()}
                  </ScrollView>
                </View>
                <View style={{height:220, borderTopColor: "#bfbfbf",borderTopWidth: 0.5,}}>
                  {/*<View style={styles.sectionLine}></View>*/}
                  <View style={styles.titleBar}>
                    <StyledText style={{color : "#707070", fontFamily: config.defaultBoldFontFamily}}>{"내가 좋아하는 여행"} </StyledText> 
                    <StyledText style={{color: "#d64409"}}> {" (" + countOfMyRecommended + "개)"}</StyledText>
                  </View>
                  <ScrollView style={{height:180, paddingLeft:15, paddingRight:15}}  horizontal={true}>
                    {this.renderMyRecommendGuides()}
                  </ScrollView>
                </View>
              </View>
              // : <View style={styles.body}></View>
            }
            <View style={styles.appInfo}>
              <View style={{height: 20, marginLeft:10, marginTop:20}}>
                <View style={{flexDirection:"row"}}>
                  <TouchableOpacity onPress={this.onPressService}>
                    <StyledText>
                      {"이용약관"}
                    </StyledText>
                  </TouchableOpacity>
                  <StyledText>
                      {" | "}
                  </StyledText>
                  <TouchableOpacity onPress={this.onPressPrivacyPolicy}>
                    <StyledText>
                      {"개인정보처리방침"}
                    </StyledText>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{flex:1, marginLeft:10, justifyContent:"center"}}>
                <View style={{}}>
                  <StyledText>{"(주)레이지버드"}</StyledText>
                </View>
                <StyledText style={styles.companyDetail}>
                  {"공동대표이사 : 송원석, 김희경   사업자등록번호 : 251-87-01078"}
                </StyledText>  
                <StyledText style={styles.companyDetail}>
                  {"주소 : 경기도 성남시 분당구 성남대로 331번길 8 20"}
                </StyledText>  
                <StyledText style={styles.companyDetail}>
                  {"ⓒLazybird Corp."}
                </StyledText>
              </View>
            </View>
          </ScrollView>
        }
        {/* {!profile.nickname &&
          <View style={styles.bottom}>
            <View style={{flex:1, marginLeft:10, marginTop:5, justifyContent:"space-around"}}>
              <View style={{flexDirection:"row"}}>
                <TouchableOpacity onPress={this.onPressService}>
                  <StyledText>
                    {"이용약관"}
                  </StyledText>
                </TouchableOpacity>
                <StyledText>
                    {" | "}
                </StyledText>
                <TouchableOpacity onPress={this.onPressPrivacyPolicy}>
                  <StyledText>
                    {"개인정보처리방침"}
                  </StyledText>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{flex:2, marginLeft:10, justifyContent:"center"}}>
            <View style={{}}>
                <StyledText>{"(주)레이지버드"}</StyledText>
              </View>
              <StyledText style={styles.companyDetail}>
                {"공동대표이사 : 송원석, 김희경   사업자등록번호 : 251-87-01078"}
              </StyledText>  
              <StyledText style={styles.companyDetail}>
                {"주소 : 경기도 성남시 분당구 성남대로 331번길 8 20"}
              </StyledText>  
              <StyledText style={styles.companyDetail}>
                {"ⓒLazybird Corp."}
              </StyledText>
            </View>
          </View>
        } */}

        {/* {this.state.openConfirm? this.renderOpenConfirm(): null} */}
         
      </View>
    );
  }
}

const styles =StyleSheet.create({
  container : {
    flex:1, backgroundColor:"#ffffff"
  },
  header : {
    height:40, 
    width:"100%", 
    backgroundColor:"#f2fcfb", 
    alignItems:"center", 
    paddingLeft:20, 
    flexDirection:"row"
  },
  profile : {
    // marginTop: 15,
    justifyContent:"center",
    alignItems:"center",
    flex:1,
  },
  logout: {
    position: 'absolute',
    right: 20,
    // right: 80,
    bottom: 15,
  },
  logoutText: {
    fontSize: 12,
    color: '#26675e',
    textDecorationLine: 'underline',
  },
  deleteAccount: {
    position: 'absolute',
    right: 20,
    bottom: 8,
  },
  delAccountText: {
    fontSize: 12,
    color: '#bfbfbf',
    textDecorationLine: 'underline',
  },
  body:{
    flex:3,
  },
  bottom: {
    height:140, 
    justifyContent:"flex-end",
    backgroundColor:"#f2f2f2"
  },
  companyDetail: {
    color: config.colors.extraText,
    fontSize: 12
  },
  guideName: {
    fontSize: 14,
    color: config.colors.defaultText
  },
  defaultSnapshot: {
    width: 100, 
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f2fcfb'
  },
  appInfo: {
    height:180,
    // backgroundColor:"#f2f2f2",
    backgroundColor:"#f2fcfb",
    paddingVertical: 5,
    paddingHorizontal: 10
  },
  logoutProfile: {
    flex:1, 
    justifyContent: "space-between"
  },
  titleBar: {
    // backgroundColor: '#d64409', 
    flexDirection:"row", 
    height:40, 
    alignItems: "center", 
    paddingLeft: 15, 
    paddingRight: 15,
  },
  sectionLine: {
    height: 15,
    backgroundColor:"#f2fcfb",
    marginBottom: 5
  }
});
