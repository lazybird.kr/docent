import React from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import config from 'src/config';
import {StyledText} from 'src/components/styledComponents';

export default function Profile(props) {
    const {profile}= props;
    return (
        <View style={styles.container}>
            <Image style={styles.profileImage} 
                source={profile.nickname? config.images.avatarOn: config.images.avatarOff}
                />
            <View style={styles.profileText}>
                {/* <StyledText style={[styles.profileName, {color: profile.nickname? 'black': config.colors.titleColor}]}> */}
                <StyledText style={profile.nickname? styles.profileName: styles.loginText}>
                    {config.member.nickname || profile.nickname || '로그인을 하세요 :)\n나만의 여행도 만들고,\n 마음에 드는 여행을 저장할 수 있어요.'}
                </StyledText>
                {/* {
                    profile.nickname &&
                    <StyledText numberOfLines={1} style={[styles.profileDesc]}>
                        {'나는 지구별여행자예요. 흘러가는대로 즐겁고, 재미있게'}
                    </StyledText>
                } */}
            </View>
        </View>

    )
}


const styles=StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        // borderWidth: 2,
        // borderColor: 'red'
    },
    profileImage: {
        // flex: 1,
        width: 27,
        height: config.size._100px,
        resizeMode: 'contain'
        // borderRadius: config.size._212px
    },
    profileText: {
        height: '100%',
        flex: 1,
        justifyContent: 'center',
        // height: 100,
        // color: 'black',
        // textAlignVertical: 'center',
    },
    loginText: {
        fontSize: config.size._64px,
        color: config.colors.defaultText,
        marginLeft: 20,
    },
    profileName: {
        color: 'black',
        fontFamily: "NotoSansKR-Bold",
        fontSize: config.size._70px,
        marginLeft: 20,
    },
    profileDesc: {
        height: 35,
        marginLeft: 20,
        marginRight: 20,
        fontSize: 12,
        color: '#707070'
    }
  });