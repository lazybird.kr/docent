import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Alert,
    View,
    Image,
    TouchableOpacity,
    SafeAreaView,
    StatusBar,
    Linking,
} from 'react-native';
import qs from 'qs';

import config from 'src/config';
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import {StyledText, StyledTextInput} from 'src/components/styledComponents';


export default class SendMailScreen extends Component {
    //닉네임변경시 저장버튼 활성화

    static navigationOptions =  ({ navigation }) => {
        return {
            headerLeft: () => <BackIcon navigation={navigation} color={"black"}/>,
            headerTitle :() => <HeaderTitle style={[styles.headerTitle]} title={"나의 도슨투고"} />,
            headerRight: () => {
                let button = <></>;
                let buttonDisabled = navigation.getParam('disabled');
                // if (navigation.getParam('submitText'))
                button = (
                    <TouchableOpacity
                        disabled={buttonDisabled}
                        style={{marginRight: 15}}
                        onPress={navigation.getParam('onPressSubmit')}>
                        <StyledText style={{fontSize: config.fontSize._60px, color: buttonDisabled? "grey": "#d64409"}}>
                            보내기
                        </StyledText>
                    </TouchableOpacity>
                );

                    return button;
            },        
            headerStyle : {
                backgroundColor: "#ffffff",
                //borderBottomWidth: 0.5,
                elevation: 0,       //remove shadow on Android
                shadowOpacity: 0,   //remove shadow on iOS
            }
        }
    }


    state = {
        from: '',
        to: 'weare@lazybird.kr',
        subject: '',
        contents: '',
        isFocused: false,
    }

    onPressSubmit = async () => {
        console.log("AAAAAAA")
        const {from, to, subject, contents} = this.state
        console.log("from : ", from)
        console.log("to : ", to)
        console.log("subject : ", subject)
        console.log("contents : ", contents)



        let url = `mailto:${to}`;

        // Create email link query
        const query = qs.stringify({
            subject: subject,
            body: contents,
        });

        if (query.length) {
            url += `?${query}`;
        }

        // check if we can use this link
        const canOpen = await Linking.canOpenURL(url);

        if (!canOpen) {
            throw new Error('Provided URL can not be handled');
        }

        Linking.openURL(url);
        this.props.navigation.goBack();
    }

    componentDidMount() {
        this.props.navigation.setParams(
            {disabled: true, onPressSubmit: this.onPressSubmit}
        )
        const {navigation} = this.props;
        this.subs = [
            navigation.addListener("didFocus", () => {
                this.setState({ isFocused: true });
            }),
            navigation.addListener("willBlur", () => {
                this.setState({ isFocused: false,});
            }),
        ];

        this.setState({
        })

    }
    componentWillUnmount() {
        this.subs && this.subs.forEach((sub) => {
            sub.remove();
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.state.subject !== prevState.subject){
            if(this.state.subject.length > 0){
                this.props.navigation.setParams({disabled:false})
            }  else {
                this.props.navigation.setParams({disabled:true})
            }
        }
    }

    changeSubjectText = (text) =>{
        this.setState({subject:text})
    }
    changeContentsText = (text) =>{
        this.setState({contents:text})
    }

   render() {
    const {subject, contents, to} = this.state;
    const {changeSubjectText, changeContentsText} = this;
    console.log("isFocused : ", this.state.isFocused)
    return (
        <SafeAreaView style={styles.container}>
            {this.state.isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
        <View style={styles.email}>
            <View style={{flex:1, borderWidth:1, borderColor:"#707070", borderRadius:10, flexDirection:"row" ,paddingLeft:10}}>
                <View style={{justifyContent:"center", paddingRight:20}}>
                    <StyledText style={{color:"#26675e"}}>
                        이메일
                    </StyledText>
                </View>
                <View style={{justifyContent:"center"}}>
                    <StyledText style={{color:"#bfbfbf"}}>
                        {to}
                    </StyledText>
                </View>
            </View>
        </View>
        <View style={styles.body}>
            <View style={{flex:1, borderWidth:1, borderColor:"#707070", 
                            borderRadius:10, paddingLeft:10, paddingRight:10}}>
                <View style={styles.subject}>
                    <View style={{justifyContent:"center",paddingRight:30}}>
                        <StyledText style={{color:"#26675e"}} >
                            제목
                        </StyledText>
                    </View>
                    <View style={{justifyContent:"center"}}>
                        <StyledTextInput 
                            placeholderTextColor="grey" placeholder="메일 제목을 입력해 주세요"
                            numberOfLines={1} onChangeText={(text) => changeSubjectText(text)}>
                            {subject} 
                        </StyledTextInput>
                    </View>
                </View>
                <View style={styles.contents}>
                    <View style={{justifyContent:"center"}}>
                        <StyledText style={{color:"#26675e"}}>
                             설명
                        </StyledText>
                    </View>
                    <View style={{justifyContent:"center"}}>
                        <StyledTextInput  
                            placeholderTextColor="grey" placeholder="메일 내용을 입력해 주세요"
                            multiline onChangeText={(text) => changeContentsText(text)}>
                            {contents}
                        </StyledTextInput>
                    </View>
                </View>
            </View>
        </View>
        </SafeAreaView>

    )
}
}



const styles=StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: config.colors.greyColor2
        backgroundColor: '#ffffff',
    },
    email : {
        height:70,
        padding:10
    },
    body:{
        flex:1,
        padding:10
    },
    subject:{
        height:50, borderBottomWidth:1, borderBottomColor:"#707070", flexDirection:"row",
        
    },
    contents:{
        marginTop:20,
        flex:1, 
    }
   
  });
