import AboutMyself from './AboutMyself';
import LoginScreen from '../Login';
import AccountScreen from './AccountScreen';
import SendMailScreen from './SendMailScreen';
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';
import WebViewScreen from 'src/components/WebView'

const AboutMyselfStack=createStackNavigator({
    aboutMyself : { 
      screen : AboutMyself,
    },
    loginScreen: {
        screen: LoginScreen,
        navigationOptions: {
          tabBarVisible: false,
          ...TransitionPresets.SlideFromRightIOS,
        },
    },
    accountScreen: {
      screen: AccountScreen,
      navigationOptions: {
        ...TransitionPresets.SlideFromRightIOS,
      },
    },
    webViewScreen: {
      screen : WebViewScreen,
      navigationOptions: {
        ...TransitionPresets.SlideFromRightIOS,
      },
    },
    sendMailScreen: {
      screen : SendMailScreen,
      navigationOptions: {
        ...TransitionPresets.SlideFromRightIOS,
      },
    }
})
AboutMyselfStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.routes.length > 0) {
      navigation.state.routes.map(route => {
          if (route.routeName === "loginScreen") {
              tabBarVisible = false; 
          } else {
              tabBarVisible = true;
          }
      });
  }

  return {
      tabBarVisible,
  };
};

export {AboutMyselfStack}