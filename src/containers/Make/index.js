import Make0Step from './Make0Step';
import Make1Step from './Make1Step';
import Make2Step from './Make2Step';
import AddFile from './AddFile';
import Make3Step from './Make3Step';
import SearchPointLocation from './SearchPointLocation';


import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';


const MakeStack=createStackNavigator({
    make0step : { 
      screen : Make0Step,
    },
    make1step : { 
      screen : Make1Step,
    },
    searchPointLocation : {
      screen : SearchPointLocation,
    },
    make2step : { 
      screen : Make2Step,
    },
    addfile : {
      screen : AddFile,
    },
    make3step : { 
      screen : Make3Step,
    },
    
  },{
    
  });

  MakeStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.routes.length > 0) {
      navigation.state.routes.map(route => {
        if (route.routeName === "make1step" ||
            route.routeName === "make2step" ) {
          tabBarVisible = false;
        } else {
          tabBarVisible = true;
        }
      });
    }
  
    return {
     tabBarVisible,
    };
  };
  

export {
    MakeStack,  
}