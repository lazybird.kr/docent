import React, {Component, createRef}  from "react";
import {View, FlatList, StatusBar, Image, StyleSheet, TouchableOpacity, Alert, BackHandler} from "react-native";
import config from 'src/config';
import {BackIcon, HeaderTitle, NextIcon} from 'src/components/header';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import {MakeSubject, MakeLocation} from 'src/components/item'
import {StyledText} from 'src/components/styledComponents';
import Confirm from "../../components/Popup/Confirm";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { NavigationActions } from 'react-navigation';
import axios from 'axios';

export default class Make1Step extends Component {

  state = {
    subject : "",
    description : "",
    picture : [],
    locationFlag : true,
    locationList : [{audio : null, location : {}, description : "", tip : "", picture : []}],
    isFocused : false,
    toggleMakeSubject : false,
    toggleMakeLocation : -1,
    openConfirm: false,
    confirmMsg: "",
    leftBtnText: "취소하기",
    rightBtnText: "확인하기",
    confirmAction: () => {},
    removeIndex : -1,
    alertMsg: '',
    alertOpen: false,
    orgGuide : {}
  }
  flatList = createRef();

  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    return {
      // headerLeft: () => {<BackIcon navigation={navigation}/>},
      headerLeft: () => <Icon style={{marginLeft : 5,}} name="chevron-left" color="#000000" size={36} underlayColor="#4BA6F8"
                          onPress={navigation.getParam('clickBack')}
                       />,
          // onPress={() => {
          //   navigation.dispatch(backAction);
          // }}
        // />,
      headerTitle : () => <HeaderTitle title="여행 만들기 (1/2)" />,
      headerRight : () => <NextIcon title="다음" navigation={navigation} clickNext={params? params.clickNext : ()=>{}} />,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
        borderBottomWidth: 0,
        elevation: 0,
        shadowOpacity: 0,
      }
    }
  };

  backAction = () => {
    console.log('backaction')
    this.setState({
      openConfirm: true,
      confirmMsg: "지금 작성 중인 여행이 사라집니다. \n여행을 그만 만드시겠습니까?",
      rightBtnText: "확인하기",
      confirmAction: this.goBackAction
    })

    return true;
  }

  checkInput(){
    if(!this.state.subject){
      this.setState({
        alertMsg: "제목을 입력하지 않으셨습니다.",
        alertOpen: true,
      })
      // Alert.alert(
      //   "",
      //   "제목을 입력하지 않으셨습니다.",
      //   [
      //     {text:"확인", onPress: ()=>{
      //     }}
      //   ],
      //   {cancelable : false}
      //   );
      return false;
    // } else if(this.state.picture.length === 0){
    //   Alert.alert(
    //     "",
    //     "대표 사진이 없습니다.",
    //     [
    //       {text:"확인", onPress: ()=>{
    //       }}
    //     ],
    //     {cancelable : false}
    //     );
    //   return false;
    }

    console.log('loaction length:', this.state.locationList.length)
    if(this.state.locationList.length === 0 ) {
      this.setState({
        alertMsg: "포인트를 한 개 이상 만들어주세요.",
        alertOpen: true,
      });

      return false;
    }
    for(let i=0; i<this.state.locationList.length;i++){
      console.log("this.state.locationList : ", this.state.locationList[i])
      if(this.state.locationList[i] && this.state.locationList[i].location && this.state.locationList[i].location.place_name){

      } else {
        let pointNo = i + 1;
        this.setState({
          alertMsg: pointNo + '번째 포인트를 입력해주세요',
          alertOpen: true
        })
        // Alert.alert(
        //   "",
        //   "포인트를 입력해 주세요.",
        //   [
        //     {text:"확인", onPress: ()=>{
        //     }}
        //   ],
        //   {cancelable : false}
        //   );
        return false;
      }
      if(this.state.locationList[i] && this.state.locationList[i].picture && this.state.locationList[i].picture[0]){

      } else {
        /*
        Alert.alert(
          "",
          "포인트 사진이 없습니다.",
          [
            {text:"확인", onPress: ()=>{
            }}
          ],
          {cancelable : false}
          );
        return false;
        */
      }
    }

    return true;
  }

  next = () => {
    if(this.checkInput()){ 
      console.log("picture : ", this.state.picture)
      this.props.navigation.navigate("make2step",
      { subject : this.state.subject,
        description : this.state.description,
        locationList : this.state.locationList,
        picture : this.state.picture,
        orgGuide : this.state.orgGuide,
        whereFrom : "Make1Step"
      })
    }
  }

  componentDidMount(){
    console.log("Mak1Step componentDidMount");
    this.props.navigation.setParams({
      clickNext: this.next,
    });
    this.props.navigation.setParams({
      clickBack: this.backAction,
    });
    
    BackHandler.addEventListener('hardwareBackPress', this.backAction);
    this.subs = [
      this.props.navigation.addListener("didFocus", () => {
        console.log("Make1Step didFocus");
      }),
      this.props.navigation.addListener("willFocus", () => {
        BackHandler.addEventListener('hardwareBackPress', this.backAction);
        console.log("Make1Step willFocus");
      }),
      this.props.navigation.addListener("willBlur", () => {
        console.log("Make1Step willBlur");
      }),
      this.props.navigation.addListener("didBlur", () => {
        console.log("Make1Step didBlur");
        BackHandler.removeEventListener('hardwareBackPress', this.backAction);
      }),
    ]

    this.setState({isFocused:true})
    console.log("ddd", this.props.navigation.state.params)
    this.getItem(this.props.navigation.state.params.itemId)
  }

  getItem = async (itemId) => {
    if(itemId){
      try {
        const resp = await axios.post(config.url.apiSvr, 
          {
            "data" : {
              "category": "guide",
              "service": "GetGuideDetail",
              "device" : {
                "device_id" : config.deviceInfo.device_id,
                "session_id" : config.sessionInfo.session_id,
              },
              "guide_id": itemId
            },
          });
  
        const {guide} = resp.data.data;
        const {title, description, image_list, place_list} = guide;
        console.log("guide :", guide)

        let locationList = []
        let locationFlag = false
        place_list.forEach(place => {
          console.log("place : ", place)
          let tempLoc = {
            "id" : place.id,
            "description" : place.description,
            "tip" : place.tip,
            "location" : {
              "place_name" : place.name,
              "y" : place.location.Latitude,
              "x" : place.location.Longitude,
              "address_name" : place.address
            },
            "audio" : place.voice_file,
            "picture" : [...(place.image_files||[])]
          }
          locationList.push(tempLoc)
        })
        console.log("locationList : ", locationList)
        if(locationList.length>0 && locationList[0].location && locationList[0].location.x){

          locationFlag = true
        }

        this.setState(
          {
            orgGuide:{...guide},
            subject : title,
            picture : [...(image_list || [])],
            description,
            locationList,
            locationFlag,
            
          }
        )
        
      } catch (e){
        console.log("guide :", e)
      }
    }
  }

  componentWillUnmount() {
    console.log('Make1Step componentWillUnmount')
    this.backAction();
    BackHandler.removeEventListener('hardwareBackPress', this.backAction);
  }

  makeLocation = (index) => {
    console.log('makeLocation:', index)
    this.flatList.current.scrollToIndex({
      index: index,
      animated: true,
    })

    this.setState({
      toggleMakeLocation : index,
      toggleMakeSubject : false,
    })
  }

  searchLocation = () => {


    


    
    //alert("위치 찾기는 나중에")
  }

  toggleLocation = () => {
    this.setState({toggleMakeLocation : -1});
  }

  changeLocation = (index,location) => {
    if(this.state.locationFlag){
      console.log("LOc : ", index, location)
      let locList = [...this.state.locationList];
      locList[index].location = {...location};
      this.setState({locationList : locList});  
    } else {
      console.log("LOc : ", index, location)
      let locList = [...this.state.locationList];
      locList[index].location = {place_name : location};
      this.setState({locationList : locList});
    }
    
  }
  changeLocDesc = (index,description) => {
    let locList = [...this.state.locationList];
    locList[index].description = description;
    this.setState({locationList : locList});
  }
  changeLocTip = (index,tip) => {
    let locList = [...this.state.locationList];
    locList[index].tip = tip;
    this.setState({locationList : locList});
  }
  changeLocPicture = (index,picture) => {
    console.log("A1 : ", picture)
    let locList = [...this.state.locationList];
    console.log("A2 : ", locList[index].picture);
    // locList[index].picture = [...locList[index].picture, picture];
    if(!locList[index].picture) locList[index].picture = [];  
    let newPictures = locList[index].picture.concat(picture)
    locList[index].picture = newPictures
    console.log("A3 : ", locList[index].picture);
    this.setState({locationList : locList});
    console.log("A4")
  }
  removeLocPicture = (index,removeIndex) => {
    let locList = [...this.state.locationList];
    locList[index].picture.splice(removeIndex,1)
    this.setState({locationList : locList});
  }

  _renderLocation(item, index) {
    // _renderLocation({item, index}) {
    console.log("_location   index: ", index)
    const {locationList, toggleMakeLocation, locationFlag} = this.state;
    const _location = locationList[index] && locationList[index].location && locationList[index].location.place_name  ? locationList[index].location.place_name : "포인트 입력" ;
    console.log("_location : ", _location)
    
    return (
      <View style={{flex:1}} key={index}>
        { toggleMakeLocation === index ?
        <View style={styles.makeSubject}>
          <MakeLocation index={index} 
            handleClick={this.toggleLocation} 
            // removeLocation={this.removeLocation} 
            //searchLocation={this.searchLocation} 
            changeLocation={this.changeLocation}
            changeLocDesc={this.changeLocDesc}
            changeLocTip={this.changeLocTip}
            changeLocPicture={this.changeLocPicture}
            removeLocPicture={this.removeLocPicture}
            locationInfo={locationList[index]}
            locationFlag={locationFlag}
            navigation={this.props.navigation}
            />
        </View>
        :
          <View style={styles.locationItem} >
            {/* <StyledText style={{marginLeft:10, flex:1.5, color: "#26675e", fontSize: 14}}>{index+1}</StyledText> */}
            <StyledText style={{marginLeft:10, flex:1.5, color: "#26675e", fontSize: 14}}>{index + 1}</StyledText>
            <TouchableOpacity style={{flex:8.5}} onPress={()=>this.makeLocation(index)}>
              <StyledText style={{fontSize: 14, color:_location === "포인트 입력" ? "grey" : "#26675e"}}>{_location}</StyledText>
            </TouchableOpacity>
            <TouchableOpacity style={{flex:1}} onPress={() => {
              this.openConfirm("선택한 포인트를 삭제하시겠습니까?", "취소하기", "삭제하기", index)
            }}>
              <Image source={config.images.delIcon} style={{width: 12, height: 12}}></Image>
              {/* <AntDesignIcon name={"minus"} size={15} color={"grey"}/> */}
            </TouchableOpacity>      
          </View>
        }
      </View>
            
    )
  }

  _renderMainGuide() {
    const {toggleMakeSubject, subject, description, picture, locationFlag} = this.state;
    const _subject = subject.length === 0? "제목 입력" : subject;

    return (
      <View>
      { toggleMakeSubject ? 
        <View style={styles.makeSubject}>
          <MakeSubject toggleSubject={this.toggleSubject}
            changeSubject={this.changeSubject} subject={subject}
            changeDesc={this.changeDesc} description={description}
            removePicture={this.removePicture} selectPicture={this.selectPicture} picture={picture} 
            setLocationFlag={this.setLocationFlag} locationFlag={locationFlag}
          />
        </View>
        :
        <View style={styles.subject}>
          <StyledText style={{marginLeft:10, flex:1.5, color: "#26675e", fontSize: 14}}>제목</StyledText>
          <TouchableOpacity style={{flex:8.5}} onPress={()=>{this.toggleSubject()}}>
            <StyledText style={{fontSize: 14, color:subject ? "#26675e":"grey"}}>{_subject}</StyledText>
          </TouchableOpacity>
        </View>
      }
      </View>
    )
  }

  _renderCreatePage({item, index}) {
    console.log('_renderCreatePage:', index)
    if (index === 0) {
      return (
        <View>
          {this._renderMainGuide()}
          {this._renderLocation(item, index)}
        </View>
      )
    } else {
      return this._renderLocation(item, index)
    }
  }

  addLocation = () => {
    
    let newLocation = { audio : null, location : {}, tip : "", description : "", picture : []}
    let tmpLocationList = [...this.state.locationList];
    tmpLocationList.push(newLocation);

    this.setState({
      locationList : tmpLocationList,
      toggleMakeLocation : -1,
      toggleMakeSubject : false,
    });
  }

  removeLocation = () => {
    let index = 0;
    if( this.state.removeIndex > 0 ) index = this.state.removeIndex;
    console.log("removeLocation index:", index)
    let locationList = [...this.state.locationList];
    locationList.splice(index, 1);
    this.setState({
      locationList: locationList,
      removeIndex: -1,
    });
    this.toggleConfirm();
  }

  toggleConfirm = () => {
    this.setState({openConfirm: !this.state.openConfirm})
  }

  _keyExtractor(item, index) {
    return index.toString();
  }

  
  toggleSubject = () => {
    this.setState({
      toggleMakeSubject : !this.state.toggleMakeSubject,
      toggleMakeLocation : -1
    });
  }

  changeSubject = (subject) =>{
    this.setState({subject})
  }
  changeDesc = (description) =>{
    this.setState({description})
  }

  removePicture = (index) => {
    let newPicture = [...this.state.picture];
    newPicture.splice(index, 1);
    this.setState({picture : newPicture});
  }
  selectPicture = (data) => {
    // let newPicture = [...this.state.picture, data]
    console.log('selectPicture:', data)
    let newPicture = this.state.picture.concat(data);
    this.setState({picture : newPicture}); 
  }
  setLocationFlag = (data) =>{
    console.log("data : ", data);
    if(this.state.orgGuide.id){
      this.setState({
        alertMsg: "위치기반 여부는 수정할 수 없습니다.",
        alertOpen: true,
      })
      return;
    }
    this.setState({locationFlag:data})
  }

  openConfirm(message, leftText, rightText, index) {
    console.log('openConfirm: ', index)
    this.setState({
      openConfirm: true,
      confirmMsg: message,
      leftBtnText: leftText,
      rightBtnText: rightText,
      removeIndex: index,
      confirmAction: this.removeLocation
    })
  }

  goBackAction = () => {
    console.log('goBackAction:')
    this.props.navigation.goBack();
  }

  render() {
    const {toggleMakeSubject, subject, description, picture, locationFlag, isFocused} = this.state;

    const _subject = subject.length === 0? "제목 입력" : subject;
    return (
      <View style={styles.container}>
        {isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
          <Confirm visible={this.state.alertOpen}
            oneButton={true}
            onBackdropPress={this.pressReportButton}
            message={this.state.alertMsg}
            onCenterButtonPress={() => {
              this.setState({ alertOpen: false })
            }} />
          <Confirm visible={this.state.openConfirm} 
            onBackdropPress={this.pressReportButton}
            message={this.state.confirmMsg}
            leftButtonLabel={this.state.leftBtnText}
            rightButtonLabel={this.state.rightBtnText}
            onLeftButtonPress={this.toggleConfirm}
            onRightButtonPress={this.state.confirmAction} />
        <View style={styles.body}>
          {/* { toggleMakeSubject ? 
          <View style={styles.makeSubject}>
            <MakeSubject toggleSubject={this.toggleSubject}
              changeSubject={this.changeSubject} subject={subject}
              changeDesc={this.changeDesc} description={description}
              removePicture={this.removePicture} selectPicture={this.selectPicture} picture={picture} 
              setLocationFlag={this.setLocationFlag} locationFlag={locationFlag}
            />
          </View>
          :
          <View style={styles.subject}>
            <StyledText style={{marginLeft:10, flex:1.5, color: "#26675e", fontSize: 14}}>제목</StyledText>
            <TouchableOpacity style={{flex:8.5}} onPress={()=>{this.toggleSubject()}}>
              <StyledText style={{fontSize: 14, color:subject ? "#26675e":"grey"}}>{_subject}</StyledText>
            </TouchableOpacity>
          </View>
          } */}
          {
            this.state.locationList.length == 0 && this._renderMainGuide()
          }
          <FlatList
            data={this.state.locationList} 
            keyExtractor={(item, index) => this._keyExtractor(item, index)}
            renderItem={(item, index) => this._renderCreatePage(item, index,)}
            flatListRef = {React.createRef()}
            ref={this.flatList}
            // renderItem={(item, index) => this._renderLocation(item, index,)}
            windowSize={3}
          />  
          <TouchableOpacity style={styles.addButton} onPress={this.addLocation}>
            <Image source={config.images.newIcon} style={{height:15, width:15}}/>
            <StyledText style={styles.addButtonText}>포인트 추가하기</StyledText>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles=StyleSheet.create({
  container : {
    height : "100%",
    flex : 1,
    justifyContent:"center",
    backgroundColor : config.colors.makeItemColor,
    paddingBottom : config.marginBottom,
  },
  body : {
    flex : 9,
    marginLeft : 10,
    marginRight : 10
  },
  locationItem : {
    height : 50,
    width : "100%",
    backgroundColor: "white",
    borderWidth : 1,
    marginTop : 10,
    borderRadius: 5,
    borderColor : "#707070",
    justifyContent : "center",
    alignItems : "center",
    flexDirection : "row"
  },
  subject : {
    height : 50,
    width : "100%",
    backgroundColor: "white",
    borderRadius: 5,
    marginTop : 10,
    borderColor : "#707070",
    borderWidth : 1,
    justifyContent : "center",
    alignItems : "center",
    flexDirection : "row"
  },
  makeSubject : {
    height : config.deviceInfo.height * 0.6,
    width : "100%",
    backgroundColor: "white",
    borderRadius: 5,
    marginTop : 10,
    borderColor : "#707070",
    borderWidth : 1,
    justifyContent : "center",
    alignItems : "center",
    flexDirection : "row"
  },
  
  addButton : {
    height : 50,
    width : "100%",
    backgroundColor: "#d64409",
    marginTop : 10,
    marginBottom : 10,
    borderRadius: 25,
    borderColor : "#d64409",
    borderWidth : 1,
    justifyContent : "center",
    alignItems : "center",
    flexDirection : "row"
  },
  addButtonText : {
    alignItems : "center",
    color : "#ffffff",
    textAlign : "center",
    fontSize : 14,
    marginLeft:5
  },
  textInput : {
    fontSize : 14,
  }
});