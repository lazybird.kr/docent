import React, {Component}  from "react";
import {View, Text, Image, StyleSheet, TouchableOpacity, StatusBar} from "react-native";
import config from 'src/config';
import {BackIcon, HeaderTitle} from 'src/components/header';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-community/async-storage';
import {StyledText, StyledTextInput} from 'src/components/styledComponents';

export default class Make3Step extends Component {

  state = {
    isFocused : false,
  }
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: () => null,
      headerTitle : () => <HeaderTitle title="여행 만들기 완료" />,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
        borderBottomWidth: 0,
        elevation: 0,
        shadowOpacity: 0,
      }
    }
  };

  componentDidMount() {
    this.setState({isFocused:true})
  }

  confirmGuide = () => {
    let item = {};

    this.props.navigation.popToTop();
    this.props.navigation.navigate("home")
  }
  render() {
    const {isFocused} = this.state;

    return (
      <View style={styles.container}>
        {isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
        <View style={styles.body}>
          <View style={styles.item1}>
            
          </View>
          <View style={styles.item2}>
            <View style={styles.doneMessage}>
              <StyledText style={{borderWidth : 1, borderRadius: 15,borderColor : "#26675e",
                backgroundColor:"#f2fcfb", width:config.deviceInfo.width * 0.7, textAlign:"center",
                padding : 5, paddingLeft:10, paddingRight:10, color: "#26675e", fontSize: 12,}}>당신의 여행이 등록되었습니다.</StyledText>  
            </View>
            <View style={{flex:3, alignItems:"center", justifyContent:"center",}}>
              <Image source={config.images.logo} style={{height:110, width:110, resizeMode:"contain"}} />
            </View>
            <View style={styles.bottom}>
              {/*<TouchableOpacity style={{...styles.bottomItem, backgroundColor:"#d5d5d5", borderWidth:0}} onPress={()=>{alert("보기는 나중에")}}>
                <StyledText style={{color: "#292929", fontSize: 14}}>수정하기</StyledText>
                </TouchableOpacity>*/}
              <TouchableOpacity style={{...styles.bottomItem, backgroundColor:"#d64409", borderWidth:0}} 
                onPress={this.confirmGuide}>
                <StyledText style={{color: "#ffffff", fontSize: 14}}>홈으로</StyledText>
              </TouchableOpacity>
            </View>
            
          </View>
          <View style={styles.item3}>
          </View>
         
        </View>
      </View>
    );
  }
}

const styles =StyleSheet.create({
  container : {
    height : "100%",
    flex : 1,
    backgroundColor : config.colors.makeItemColor,
  },
  body : {
    flex : 1,
    marginLeft : 10,
    marginRight : 10,
    
  },
  item1 : {
    flex : 2,
    width : "100%",
  },
  item2 : {
    flex : 5,
    width : "100%",
    justifyContent:"center",
    alignItems: "center",
  },
  item3 : {
    flex : 3,
    width : "100%",
  },
  doneMessage : {
    flex:1,
    justifyContent:"flex-end",
    alignItems:"center"
  },
  bottom : {
    flex:1,
    alignItems:"center",
    flexDirection:"row"
  },
  bottomItem : {
    height:30, width:config.deviceInfo.width * 0.7 / 2 - 10,
    borderWidth : 1,
    borderRadius: 5,
    borderColor : "gray",
    margin : 5,
    alignItems:"center",
    justifyContent:"center"

  }
    
});