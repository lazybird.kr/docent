import React, {Component}  from "react";
import {View, Text, Image, FlatList, StyleSheet, TouchableOpacity, TextInput} from "react-native";
import config from 'src/config';
import {BackIcon, HeaderTitle, NextIcon} from 'src/components/header';
import DocumentPicker from 'react-native-document-picker';
import RNFS from 'react-native-fs';
import {StyledText} from 'src/components/styledComponents';
// import RNFetchBlob from 'rn-fetch-blob';
import Confirm from "../../components/Popup/Confirm";

export default class AddFile extends Component {

  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    return {
      headerLeft: () => <BackIcon navigation={navigation}/>,
      headerTitle : () => <HeaderTitle title="첨부파일" />,
      headerRight : () => <NextIcon title="완료" navigation={navigation} clickNext={params? params.clickNext : ()=>{}} />,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
      },
    }
  };

  state = {
    _subject : "",
    _subjectFile : {},
    _locationList : [],
    soundList : [],
    _updateFlag : false,
    alertMsg : "",
    alertOpen : false,

  }

  next = () => {
    //음성 파일을 전부 채웠는지 확인하기
    const {_locationList, _subjectFile} = this.state
    const uriFlags = _locationList.every(location => {
      return location.audio.uri
    })
    console.log("_locationList : ", _locationList)
    console.log("uriFlags : ", uriFlags)
    if(uriFlags && _subjectFile.uri){
      this.props.navigation.goBack();
      this.props.navigation.state.params.onGoBack(this.state._subjectFile, this.state._locationList);
    } else {
      this.setState({
        alertMsg : "모든 오디오 파일을 등록해 주세요",
        alertOpen : true,
      })
    }
    
  }

  componentDidMount(){
    console.log("AddFile componentDidMount");
    this.props.navigation.setParams({
      clickNext: this.next,
    });
    
    this.subs = [
      this.props.navigation.addListener("didFocus", () => {
        console.log("locationList2 : ", this.props.navigation.state.params.locationList2)
        this.setState(
          {
            _subject : this.props.navigation.state.params.subject,
            _updateFlag : this.props.navigation.state.params.updateFlag,
            _locationList : this.props.navigation.state.params.locationList2,
          });
      }),
      this.props.navigation.addListener("willFocus", () => {
        console.log("AddFile willFocus");
      }),
      this.props.navigation.addListener("willBlur", () => {
        console.log("AddFile willBlur");
      }),
      this.props.navigation.addListener("didBlur", () => {
        console.log("AddFile didBlur");
      }),
    ];
    
  }

  componentWillUnmount() {
    console.log("AddFile componentWillUnmount()");
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  _selectFile = async (index) => {
    // Pick a single file
    console.log('_selectFile')
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.audio],
      });
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size
      );
      //const base64Data = await RNFS.readFile(res.uri, "base64")
      let locList = [...this.state._locationList];
      locList[index] = { ...locList[index],
        //"audio" : {...res, base64 : base64Data}
        "audio" : {...res}
      }

      // let stat = await RNFetchBlob.fs.stat(res.uri)
      // const mainBundle = Platform.OS === 'ios'
      //     ? encodeURIComponent(Sound.MAIN_BUNDLE)
      //     : Sound.MAIN_BUNDLE;

      // const sound = new Sound(stat.path, null, (err) => {
      //   let duration = 0;
      //   if(err) {
      //     console.log('sound error:', err)
      //   } else {
      //     duration = sound.getDuration();
      //   }
      //   locList[index].audio.duration = duration.toString();

      //   console.log('locList.audio:', locList[index].audio)

      //   this.setState({_locationList : locList});
      // })
      this.setState({_locationList : locList});

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }
  _selectSubjectFile = async () => {
    // Pick a single file
    console.log('_selectSubjectFile')
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.audio],
      });
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size
      );

      // let stat = await RNFetchBlob.fs.stat(res.uri)
      // const mainBundle = Platform.OS === 'ios'
      //     ? encodeURIComponent(Sound.MAIN_BUNDLE)
      //     : Sound.MAIN_BUNDLE;

      // const sound = new Sound(stat.path, null, (err) => {
      //   let duration = 0;
      //   if(err) {
      //     console.log('sound error:', err)
      //   } else {
      //     duration = sound.getDuration();
      //   }
      //   res.duration = duration.toString();
      //   this.setState({_subjectFile : {...res}});
      // })
      this.setState({_subjectFile : {...res}});

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }
  _initFile = (index) => {
    let locList = [...this.state._locationList];
    delete locList[index]["audio"];
    this.setState({_locationList : locList})
  }
  _initSubjectFile = () => {
    this.setState({_subjectFile : {}})
  }

  _keyExtractor(item, index) {
    return index.toString();
  }

  _renderLocation({item, index}) {
    const {_locationList} = this.state;
    let fileName = '';
    if(_locationList[index] && _locationList[index].audio && _locationList[index].audio.name && _locationList[index].audio.uri){
      const fileOrgName = _locationList[index].audio.name;
      if(fileOrgName.length > 20) {
        const postfix = fileOrgName.split('.')[1];
        fileName = fileOrgName.substr(0,20) + "..."+ postfix;
      } else {
        fileName = fileOrgName;
      } 
    }
    
    return (
      <View style={{flex:1}} key={index}>
        <View style={styles.fileItem} key={index}>
          <StyledText style={{marginLeft:10, textAlign : "center", flex:1, color: "#26675e", fontSize: 16}}>{index+1}</StyledText>    
          <TouchableOpacity style={{flex:7, 
              backgroundColor:fileName.length? "#f2fcfb" : "#ffffff",
              borderWidth:0.5, borderColor:"#707070",
              borderRadius: 5, height:40, justifyContent:"center", alignItems:"center",
              flexDirection:"row"}} onPress={()=>{this._selectFile(index)}}>
            <View style={{flex:6, paddingLeft:10}} >
            {fileName.length ?
              <StyledText style={{fontSize: 14, color:"#26675e"}}>{fileName}</StyledText>
              :
              <StyledText style={{fontSize: 14, color:"#707070"}}>{this.state._locationList[index].location.place_name}</StyledText>
            }
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{flex:1, alignItems:"center"}} onPress={()=>{this._initFile(index)}}>
            <Image source={config.images.delIcon} style={{height:14, width:14}} />
          </TouchableOpacity>
        </View>    
      </View>   
    )
  }
  
  render() {
    const { _subject, _subjectFile } = this.state;
    let fileName = '';
    if(_subjectFile && _subjectFile.name){
      const fileOrgName = _subjectFile.name;
      if(fileOrgName.length > 20) {
        const postfix = fileOrgName.split('.')[1];
        fileName = fileOrgName.substr(0,20) + "..."+ (postfix ? postfix : "");
      } else {
        fileName = fileOrgName;
      } 
    }
    return (
      <View style={styles.container}>
        <Confirm visible={this.state.alertOpen}
            oneButton={true}
            onBackdropPress={()=>this.setState({alertOpen:false})}
            message={this.state.alertMsg}
            onCenterButtonPress={() => {
              this.setState({ alertOpen: false })
            }} />
        <View style={styles.body}>
          <View style={styles.fileItem}>
            <StyledText style={{marginLeft:10, textAlign : "center", flex:1, color: "#26675e", fontSize: 16}}>제목</StyledText>    
            <TouchableOpacity style={{flex:7, 
                backgroundColor:fileName.length? "#f2fcfb" : "#ffffff",
                borderWidth:0.5, borderColor:"#707070",
                borderRadius: 5, height:40, justifyContent:"center", alignItems:"center",
                flexDirection:"row"}} onPress={()=>{this._selectSubjectFile()}}>
              <View style={{flex:6, paddingLeft:10}} >
              {fileName.length ?
                <StyledText style={{fontSize: 14, color:"#26675e"}}>{fileName}</StyledText>
                :
                <StyledText style={{fontSize: 14, color:"#707070"}}>{_subject}</StyledText>
              }
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{flex:1, alignItems:"center"}} onPress={()=>{this._initSubjectFile()}}>
              <Image source={config.images.delIcon} style={{height:14, width:14}} />
            </TouchableOpacity>
          </View> 

          <FlatList
            data={this.state._locationList} 
            keyExtractor={(item, index) => this._keyExtractor(item, index)}
            renderItem={(item, index) => this._renderLocation(item, index,)}
            windowSize={3}
          />
        </View>
      </View>
    );
  }
}

const styles =StyleSheet.create({
  container : {
    height : "100%",
    flex : 1,
    justifyContent:"center",
    backgroundColor : config.colors.makeItemColor,
  },
  body : {
    flex : 1,
    marginLeft : 10,
    marginRight : 10
  },
  fileItem : {
    height : 50,
    width : "100%",
    marginTop : 10,
    justifyContent : "center",
    alignItems : "center",
    flexDirection : "row"
  },
  
});