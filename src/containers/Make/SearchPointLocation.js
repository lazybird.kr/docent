import React, {Component}  from "react";
import {View, FlatList,Keyboard, Image, Platform, Text, TouchableOpacity} from "react-native";
import config from 'src/config';
import {BackIcon, HeaderTitle, NextIcon} from 'src/components/header';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { SearchBar } from 'react-native-elements';
//import Ionicons from 'react-native-vector-icons/Ionicons'
import {StyledText, StyledTextInput} from 'src/components/styledComponents';

export default class SearchPointLocation extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: () => <BackIcon navigation={navigation}/>,
      headerTitle : () => <HeaderTitle title="검색" />,
      headerRight: () => <View style={{flex:1}}/>,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0, 
      }
    }
  };


    constructor() {
        super();
        this.state = {
            searchValue : '',
            searchData : [],
            keyboardHeight: 0,
        }
        this._isMounted = false;
    }

    searchFilterFunction = text => {
      console.log("text : ", text)
        if(!text){
          this.setState({searchValue : "", searchData : []});
          return;
        }
        if(this._isMounted)
          this.setState({ searchValue : text });
          const APP_KEY =config.kakaoRestAPI.appKey;
          const url =  config.kakaoRestAPI.searchURL +`?query=${text}`;

          fetch(url, {
            headers : {"Authorization" : "KakaoAK " + APP_KEY},
          })
          .then(res => res.json())
          .then(resp => {
            let newData = [];
            resp.documents.forEach(item => {
              newData.push(item)
            })
            if(this._isMounted) {
              this.setState({searchData : newData});
            }
            console.log(resp);
          })
      }
      /*<SearchBar  
                  ref={search => this.search = search}
                  platform={Platform.OS === "ios" ? "ios" : "android"}
                  cancelButtonTitle="취소"
                  inputStyle={{height : 40, fontFamily : config.altFontFamily, fontSize:14}} 
                  containerStyle={{backgroundColor:"#FBFBFB", 
                    borderBottomColor: 'transparent', borderTopColor: 'transparent'}} 
                  placeholder="검색"
                  onChangeText={this.searchFilterFunction}
                  onClearText={this.handleClearText}  
                  autoCorrect={false}
                  value={searchValue}
          />*/
      searchHeader = () => {
        const { searchValue } = this.state;
        return (
          
          <View style={{height:70, backgroundColor:"white", width:"100%", zIndex:1, padding:10}}>
            <View style={{flex:1, marginLeft:5, borderRadius:30, borderWidth:1, borderColor:"#d64409", flexDirection:"row", alignItems:"center"}}>
              <Image source={config.images.searchIcon} style={{marginLeft:10, height: 20, width :20, }}/>
              <StyledTextInput style={{ flex:9, fontSize: 14, marginLeft:10, color:"#000000", alignSelf:"center"}}
                  ref={(input) => this.textInput = input}
                  onSubmitEditing={()=>{}}
                  onChangeText={this.searchFilterFunction}
                  placeholder={"검색"} placeholderTextColor="#9e9e9e"
                  autoFocus={true}
                >
                {searchValue}
              </StyledTextInput>
              {searchValue ? <Icon style={{alignSelf:"center", width:24, marginRight:10}} name="close" color="grey" size={24}
                onPress={this.handleClearText}/>: <></>}
            </View>
          </View>
        );
      }

    
  
      
      handleClearText = () => {
        this.setState({
          searchValue : "", searchData :[],
        });
      }
  
     
      
      componentDidMount() {
        console.log("SearchInputModal :")
        this._isMounted = true;
        this.subs = [
          Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this)),
          Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this)),
        ];
      }

      componentWillUnmount() {
        this.subs.forEach((sub) => {
          sub.remove();
        });
        this._isMounted = false;
      }
    
      _keyboardDidShow(e) {
        if(this._isMounted)
          this.setState({keyboardHeight: e.endCoordinates.height});
      }
    
      _keyboardDidHide(e) {
        if(this._isMounted) 
          this.setState({keyboardHeight: 0});
      }
      
      renderSeparator = () => {
        return (
          <View
            
          />
        );
      }

      handleSelectPlace = (location) =>{
        const {handleSelectPlace} = this.props.navigation.state.params;
        handleSelectPlace(location);
        this.props.navigation.goBack();
        return;
      }

      render() {
        
          return(
          
              <View style={{ width:"100%", 
                            height : "100%"
                            
                            /*this.state.keyboardHeight? 
                                config.deviceInfo.height - this.state.keyboardHeight - (Platform.OS === 'ios' ? 20 : StatusBar.currentHeight)
          : config.deviceInfo.height * 0.7, */}}>
                  {/*<View style={{
                      
                      alignItems: 'center',
                      height: 50,
                      borderBottomWidth : 1,
                      borderColor : "grey",
                      justifyContent : "center",
                      flexDirection : "row",
                      paddingLeft : 10,
                      paddingRight : 10,
                      backgroundColor : "#FFFFFF"
                    }}>
                      <View
                          style={{flex : 1}}
                      />
                      <View style={{flex : 8, justifyContent:"center", alignItems : "center"}}>
                        <StyledText style={{fontSize: 14}}>위치</StyledText>
                      </View>
                      <Icon
                        name="close"
                        size={20}
                        color='#525966'
                        style={{ flex : 1, marginLeft: 10 }}
                        onPress={()=>{this.props.toggleSearch()}}
                      />
                  </View>*/}
                    <FlatList 
                      style={{backgroundColor:"#FFFFFF"}}
                      data={this.state.searchData}
                      renderItem={({ item, index }) => (
                        <View style={{height:50, margin:5, marginLeft:30}} key={index}>
                          <TouchableOpacity onPress={()=>{this.handleSelectPlace(item)}}>
                            <StyledText style={{fontSize:14, color:"black"}}>{item.place_name}</StyledText>
                            <StyledText style={{fontSize:12, color:"grey"}}>{item.address_name}</StyledText>
                          </TouchableOpacity>
                          
                        </View>
                        )
                      }
                      keyExtractor={(item, index) => "key" + index}
                      ItemSeparatorComponent={this.renderSeparator}
                      ListHeaderComponent={this.searchHeader}
                      keyboardShouldPersistTaps="always"
                      stickyHeaderIndices={[0]}
                      //contentContainerStyle={{ paddingBottom: 50}}
                    />
                </View>
          )
      }

     
}