import React, {Component}  from "react";
import {View, StatusBar, Image, FlatList, StyleSheet, TouchableOpacity, TextInput,} from "react-native";
import config from 'src/config';
import {HeaderTitle} from 'src/components/header';
import {StyledText} from 'src/components/styledComponents';
import { Alert } from "react-native";
import axios from 'axios';
import Loading from 'src/components/Loading';
import Confirm from "src/components/Popup/Confirm";

export default class Make0Step extends Component {

  state = {
    tempWritingList : [
      /*{title : "창덕궁에 가면", date : "2020.05.01. 15:21"},
      {title : "가을에 만난 수원화성", date : "2020.05.07. 10:01"},
      {title : "지금 여기 익선동", date : "2020.05.09. 11:11"},*/
    ],
    guideList : [],
    tempWritingListFlag : true,
    isFocused: false,
    loading : false,
    confirmMsg: '',
    deleteConfirm: null,
  }
  constructor(props) {
    super(props);
  }
  
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    const cnt = params && params.state && 
      params.state.tempWritingList && params.state.tempWritingList.length ||  0;
    const title="여행만들기 ";
    return {
      headerLeft: () => null,
      headerTitle : () => <HeaderTitle style={{fontSize: 16, color:"#000000"}} title={title} />,
      // headerTitle : () => {
      //     const title="여행만들기 ";
      //     return (
      //       <View style={{
      //         alignItems: "center",
      //         justifyContent:"center",
      //         flexDirection : "row",
      //         flex : 1,}}>
      //         <StyledText style={{fontSize : 16,color : 'black'}}>
      //           {title}
      //         </StyledText>
      //         {/*<StyledText style={{fontSize : 16,color : 'red'}}>
      //           {cnt}
      //       </StyledText>*/}
      //       </View>
      //     );
      // },
      headerStyle : {
        backgroundColor: config.colors.headerColor,
        borderBottomWidth: 0,
        elevation: 0,
        shadowOpacity: 0,
      }
    }
  };
  
  componentDidMount(){
    console.log("componentDidMount");
    
    this.subs = [
      this.props.navigation.addListener("didFocus", () => {
        console.log("Make0Step willFocus");
        this.props.navigation.setParams({
          state: this.state,
        });
        this.setState({isFocused : true})
      }),
      this.props.navigation.addListener("willFocus", () => {
        console.log("Make0Step willFocus");
      }),
      this.props.navigation.addListener("willBlur", () => {
        console.log("Make0Step willBlur");
        this.setState({isFocused : false})
      }),
      this.props.navigation.addListener("didBlur", () => {
        console.log("Make0Step didBlur");
      }),
    ];
  }
  

  next = () => {
    this.props.navigation.navigate("make1step", 
    {itemId:""})
  }

  // deleteItem = async(item) =>{
  deleteItem = async() =>{
    let item = this.state.deleteConfirm
    console.log("item : ", item);
    console.log("config.member.member_id : ", config.member.member_id)
    try {
      const send_data = {
        "category": "guide",
        "service": "DeleteGuide",
        "device" : {
          "device_id" : config.deviceInfo.device_id,
          "session_id" : config.sessionInfo.session_id,
        },
        "guide_id" : item.id,
        "member_id" : config.member.member_id
        
      }
      const resp = await axios.post(config.url.apiSvr,
        {"data" : send_data});
      console.log("DeleteGuide : ", resp.data);
      this.getMyPublishedList();
      this.setState({
        deleteConfirm: null,
      })
    } catch(e){
      console.log("DeleteGuide Error : ", e);
    }

  }
  confirmDelete = (item)=>{
    // Alert.alert(
    //   "",
    //   "정말 삭제 하시겠습니까?",
    //   [
    //     {text:"아니오", onPress: ()=>{
    //     }},
    //     {text:"예", onPress: ()=>{
    //       this.deleteItem(item)        
    //     }}
    //   ],
    //   );
    this.setState({
      deleteConfirm: item,
    })
  }

  updateItem = (item) =>{
    console.log("item : ", item)
    this.props.navigation.navigate("make1step", {itemId:item.id})
  }

  _renderPost({item, props, index}) {
    const {tempWritingListFlag} = this.state;
    let createAt = new Date(item.date)
    let formatDate = createAt.getFullYear() + '.' + (createAt.getMonth()+1) + '.' + createAt.getDate();
    formatDate += ' ' + createAt.getHours() + ':' + createAt.getMinutes();

    return (
      <View key={index.toString()} style={styles.tempWritingList}>
        <TouchableOpacity style={{flex:9, justifyContent:"center"}} onPress={()=>this.updateItem(item)}> 
          <StyledText style={styles.cardText}>{item.title}</StyledText>
          <StyledText style={{color:"#707070", fontSize:11, marginTop: 5}}>{formatDate}</StyledText>
        </TouchableOpacity>
        <TouchableOpacity style={{flex:1,justifyContent:"center", alignItems:"center"}} onPress={()=>this.confirmDelete(item)}>
          <Image source={config.images.delIcon} style={{width:16,height:16}}/>

        </TouchableOpacity>
        {/*tempWritingListFlag && <TouchableOpacity style={{flex:1, justifyContent:"center"}} onPress={()=>{alert("나중에")}}> 
          <Image source={config.images.delIcon} style={{height: 14, width : 14}}/>
          </TouchableOpacity>*/}
      </View>
    )
  }

  componentDidUpdate(prevProps, prevState){
    if(prevState.isFocused !== this.state.isFocused && this.state.isFocused){
      this.getMyPublishedList();
      // if(!config.access_info && this.state.isFocused){
      //   Alert.alert(
      //     "",
      //     "로그인 후 이용해 주세요.",
      //     [
      //       {text:"예", onPress: ()=>{
      //         this.props.navigation.navigate("aboutMyself");
      //         return;
      //       }}
      //     ],
      //     {cancelable : false}
      //     );
      // }
    }
  }

  getMyPublishedList = async () => {
    this.setState({loading : true})
    try {
      const send_data = {
        "category": "guide",
        "service": "GetMyGuide",
        "device" : {
          "device_id" : config.deviceInfo.device_id,
          "session_id" : config.sessionInfo.session_id,
        },
        "member": {
          "member_id" : config.member.member_id
        }
      }
      const resp = await axios.post(config.url.apiSvr,
        {"data" : send_data});
        console.log("GetMyGuide : ", resp.data);
      const {guide_list} = resp.data.data;
      let guideList = [];
      guide_list && guide_list.length >0 &&  guide_list.forEach(guide => {
        guideList.push({
          title : guide.title, 
          id : guide.id, date:guide.updated_at || guide.created_at}) // 생성시 update_at도 채워지기 때문에 update 날짜로 채움
      })
      this.setState({guideList})
    } catch(e){
      console.log("GetMyGuide Error : ", e);
    }
    this.setState({loading : false});
  }

  _keyExtractor(item, index) {
    return index.toString();
  }

  clickHeader = () => {
    this.setState({tempWritingListFlag:!this.state.tempWritingListFlag})
  }

  clickToLogin = () => {
    this.props.navigation.navigate("aboutMyself")
  }

  render() {
    const {tempWritingListFlag, tempWritingList, guideList, isFocused, loading} = this.state;
    const {clickHeader} = this;
    const newText = "  새로운 여행 만들기"
    
    return (
      <View style={styles.container}>
        {loading && <Loading color={"grey"}/>}
        {isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
          <View style={styles.body}>
            <Confirm visible={!config.access_info && this.state.isFocused}
              oneButton={true}
              onBackdropPress={this.pressReportButton}
              message={'로그인 후 이용해 주세요.'}
              onCenterButtonPress={this.clickToLogin} />
            <Confirm visible={this.state.deleteConfirm != null}
              onBackdropPress={this.pressReportButton}
              message={'정말 삭제 하시겠습니까?'}
              leftButtonLabel={'취소하기'}
              rightButtonLabel={'삭제하기'}
              onLeftButtonPress={() => {
                this.setState({
                  deleteConfirm: null,
                })
              }}
              onRightButtonPress={() => {
                this.deleteItem()
              }} />
            <View style={styles.headerOfItems}>
              <TouchableOpacity style={{flex:1, height:"100%", justifyContent:"center", alignItems:"center",borderBottomColor:"#d64409", borderBottomWidth:3}}
                onPress={clickHeader}
              >
                <StyledText style={{color:tempWritingListFlag? "#d64409": "#707070"}}>
                  {"발행한 여행"}
                </StyledText>
              </TouchableOpacity>
              <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center", borderBottomColor:"#d64409", borderBottomWidth:0}}
                onPress={()=>{}}
              >
                <StyledText style={{color:tempWritingListFlag?"#d64409":"#707070"}}>
                  {""}
                </StyledText>
              </TouchableOpacity>
              
            </View>
              <FlatList
                //data={tempWritingListFlag ? tempWritingList: guideList} 
                data={guideList} 
                keyExtractor={(item, index) => this._keyExtractor(item, index)}
                renderItem={(item, index) => this._renderPost(item, this.props, index)}
                windowSize={3}
              />
              <TouchableOpacity style={styles.newButton} onPress={this.next}>
                <Image source={config.images.newIcon} style={{height: 18, width : 18}}/>
                <StyledText style={{color : "white", textAlign : "center", fontSize : 14,}}>{newText}</StyledText>
              </TouchableOpacity>
          </View>
        
       
      </View>
    );
  }
}

const styles=StyleSheet.create({
  container : {
    height : "100%",
    flex : 1,
    justifyContent:"center",
    backgroundColor : config.colors.makeItemColor,
    paddingBottom : config.marginBottom,
  },
  body : {
    flex : 9,
    margin : 10,
  },
  newButton : {
    height : 50,
    width : "100%",
    backgroundColor: config.colors.newButtonColor,
    borderRadius: 30,
    justifyContent : "center",
    alignItems : "center",
    flexDirection:"row"
  },
  headerOfItems:{
    height : 40,
    width : "100%",
    justifyContent : "center",
    alignItems : "center",
    flexDirection:"row"
  },
  tempWritingList : {
    // height : 60,
    height : 71,
    width : "100%",
    backgroundColor: "white",
    borderColor : "#707070",
    borderRadius: 5,
    borderWidth : 0.5,
    justifyContent : "center",
    marginTop : 10,
    paddingLeft : 10,   
    flexDirection:"row" 
  },
  textInput : {
    fontSize : 16,
    flex:1
  },
  cardText: {
    color:"#292929",
    fontSize: 14,
  },
});
