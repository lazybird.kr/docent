import React, {Component}  from "react";
import {View, StatusBar, Image, StyleSheet, TouchableOpacity, ScrollView} from "react-native";
import Modal from 'react-native-modal';
import config from 'src/config';
import {BackIcon, HeaderTitle, NextIcon} from 'src/components/header';
import {isNumeric} from 'src/modules/utils';
import axios from 'axios';
import {saveFile} from 'src/modules/utils';
import {StyledText, StyledTextInput} from 'src/components/styledComponents';
import Loading from 'src/components/Loading';
import Confirm from "../../components/Popup/Confirm";
import AsyncStorage from "@react-native-community/async-storage";

export default class Make2Step extends Component {

  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    return {
      headerLeft: () => <BackIcon navigation={navigation}/>,
      headerTitle : () => <HeaderTitle title="여행 만들기 (2/2)" />,
      headerRight : () => <NextIcon title="올리기" navigation={navigation} clickNext={params? params.clickNext : ()=>{}} />,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
        borderBottomWidth: 0,
        elevation: 0,
        shadowOpacity: 0,
      },
    }
  };

  state = {
    guideid: '',
    categoryList : [
      {key:"자연", checked: false}, {key:"문화",     checked: false}, {key:"관광", checked: false}, {key:"동네", checked : false},
      {key:"공원", checked: false}, {key:"문화재",   checked: false}, {key:"액티비티", checked: false}, {key:"지역상점", checked : false},
      {key:"맛집", checked: false}, {key:"카페",     checked: false}, {key:"공방", checked: false}, {key:"랜선투어", checked : false},
      {key:"야경", checked: false}, {key:"비오는날", checked: false}, {key:"데이트", checked: false}, {key:"키즈", checked : false}
    ],
    reference : "",
    price : "무료",
    // season : "사계절",
    season : ["사계절"],
    // time : "하루",
    time : ["하루"],
    level : "낮음",
    guide : "음성변환",
    subject : "",
    description  : "",
    locationList : [],
    subjectFile : null,
    cardWidth : config.deviceInfo.width / 4 - 15,
    loading : false,
    isFocused : false,
    tag: "",
    openConfirm: false,
    alertOpen:false,
    alertMsg:""
  }

  next = () => {
    if(this.state.orgGuide && this.state.orgGuide.id){
      this.updateGuide();
    } else {
      this.createGuide();
    } 
  }
  checkEnoughAudioFile = () => {
    const {subjectFile, locationList} = this.state;
    if(!subjectFile) {
      console.log('audio file of subject is not exist')
      return false
    } 
    
    let check = true;
    for(let i = 0; i < locationList.length; i++) {
      if(!locationList[i].audio) {
        console.log('There is not enough audio file')
        check = false;
        break;
      }
    }

    console.log('enough:', check)
    return check;
  }

  createGuide = async () => {
    const { subject, description, locationList, picture, price, reference, season, time, level,
            categoryList, guide, subjectFile} = this.state;
    let place_list = [];
    
    if(guide !== "음성변환" && this.checkEnoughAudioFile() == false) return
    locationList.forEach((element) => {
      let image_file_names = [];
      console.log("element : ", element);
      element.picture.map((pic, i) =>{
        image_file_names.push(pic.fileName || new Date().getTime().toString() + '_' + i + ".jpg");
      })
      let place = {
        name : element.location.place_name,
        description : element.description || "",
        tip : element.tip || "",
        voice_file: {
          name: guide === "음성변환" ? "" : new Date().getTime().toString() + ".mp3", //element.audio.name
          duration: guide === "음성변환" ? "" : element.audio.duration,
        },
        address : element.location.address_name,
        location : {
          latitude : element.location.y,
          longitude : element.location.x
        },
        transport: "walking",
        image_file_names: image_file_names,
      }
      place_list.push(place);
    });
    let categories = [];
    categoryList.forEach(element => {
      if(element.checked){
          categories.push(element.key);
      }
    })
    // categories.push(price, season, time, '난이도 ' + level, guide);
    categories.push(price, '난이도 ' + level, guide);
    categories = categories.concat(season);
    categories = categories.concat(time);
    console.log('category:', categories)
    let image_name_list = [];
    console.log("picture : ", picture);
    picture.map((pic, i) => {
      image_name_list.push(pic.fileName || new Date().getTime().toString() + "_" + i + ".jpg");
    })
    console.log('subjectFile:', subjectFile)
    const send_data = {
      "category": "guide",
      "service": "CreateGuide",
      "device" : {
        "device_id" : config.deviceInfo.device_id,
        "session_id" : config.sessionInfo.session_id,
      },
      "guide": {
        "written_by" : {
          "member_id" : config.member.member_id,
          "name" : config.member.nickname,
        },
        "title": subject,
        "description": description,
        "price": price.toString() || "",
        "reference": reference || "",
        //"tag" : tag  ||[],
        "category": categories,
        "image_name_list" : image_name_list,
        "voice": {
          name: guide === "음성변환" ? "" : new Date().getTime().toString() + ".mp3", //subjectFile.name
          duration: guide === "음성변환"? "" : subjectFile.duration
        },
        "guide_method": guide === "음성변환" ? "tts" : "",
        "place_num": place_list.length || 0,
        "place_list": place_list || []
      }
    }
    console.log("send_data : ", JSON.stringify(send_data));

    this.setState({loading:true})
    try {
      const resp = await axios.post(config.url.apiSvr, 
        { "data" : send_data },
        { timeout:10000 }
      );
      
      console.log("\\\\CreateGuide1 : ", resp.data.data.guide.id)
      let guide_image_list = resp.data.data.guide.image_list;
      let promiseList = [];
      console.log('guide_image_list:', guide_image_list)
      guide_image_list && guide_image_list.forEach((image,i) =>{
        const saveFileItem = saveFile(image.path, { 
          // uri : picture[i].uri,
          uri : picture[i].path,
          name : new Date().getTime().toString() + "_" + i + ".jpg", 
          type: 'image/jpeg'
        })
        promiseList.push(saveFileItem);
      })

      let location_list = resp.data.data.guide.place_list;
      location_list.forEach((place, i) => {
        place.image_files && place.image_files.forEach((image,j) =>{
          const saveFileItem = saveFile(image.path, { 
            // uri : locationList[i].picture[j].uri,
            uri : locationList[i].picture[j].path,
            name : new Date().getTime().toString() + "_" + i + ".jpg", 
            type: 'image/jpeg'
          })
          promiseList.push(saveFileItem);
        })
      })

      if(guide !== "음성변환"){
        const guideUri = resp.data.data.guide.voice.path
        place_list.forEach((place, i)  => {
          place.remoteUri = resp.data.data.guide.place_list[i].path;
          place.uri = locationList[i].audio.uri;
        });
        promiseList = [...promiseList,
          saveFile(guideUri, { 
            uri : subjectFile.uri,
            name : new Date().getTime().toString() + ".mp3", 
            type: 'audio/mp3'
          }),
          place_list.map(place => {
            return saveFile(place.remoteUri, {
              uri : place.uri,
              name : new Date().getTime().toString() + ".mp3",
              type: 'audio/mp3' });
          })
        ];
      }
      await Promise.all(promiseList);
      // this.setState({loading:false},()=>{
      //   this.props.navigation.navigate("make3step");
      // })
      this.setState({
        loading: false,
        openConfirm: true,
        guideId: resp.data.data.guide.id,
      })
    } catch (e){
      console.log("e:",e)
      this.setState({loading:false})
    }
  }

  updateGuide = async () => {
    const { subject, description, locationList, picture, price, reference, season, time, level,
            categoryList, guide, subjectFile} = this.state;
    let place_list = [];
    
    if(guide !== "음성변환" && this.checkEnoughAudioFile() == false) return
    locationList.forEach((element) => {
      let image_files = [];
      console.log("element : ", element);
      element.picture.map((pic, i) =>{
        console.log("pic : ", pic)
        if(pic.path && pic.path.substring(0,4) === "http"){
          image_files.push(
            {path : pic.path, server_id:pic.server_id, name:pic.name, type:pic.type}
          );
        } else {
          image_files.push(
            { name : pic.fileName || new Date().getTime().toString() + '_' + i + ".jpg"}
          );
        }
        
      })
      let place = {
        id : element.id || "",
        name : element.location.place_name,
        description : element.description || "",
        tip : element.tip || "",
        voice_file: element.audio && element.audio.uri ?
        {
          name: guide === "음성변환" ? "" : new Date().getTime().toString() + ".mp3", //element.audio.name
          duration: guide === "음성변환" ? "" : element.audio.duration,
        } :{},
        address : element.location.address_name,
        location : {
          latitude : element.location.y,
          longitude : element.location.x
        },
        transport: "walking",
        image_files: image_files,
      }
      if (!element.audio || !element.audio.uri){
        delete(place.voice_file)
      }
      let placeIndex = this.state.orgGuide.place_list.findIndex(item => item.id === element.id);
      if(placeIndex>-1){
        if(element.description === this.state.orgGuide.place_list[placeIndex].description){
          delete(place.description)
        }
      }
      place_list.push(place);
    });
    console.log("place_list : ", place_list)
    
    let categories = [];
    categoryList.forEach(element => {
      if(element.checked){
          categories.push(element.key);
      }
    })
    // categories.push(price, season, time, '난이도 ' + level, guide);
    categories.push(price, '난이도 ' + level, guide);
    categories = categories.concat(season);
    categories = categories.concat(time);
    console.log('category:', categories)
    let image_list = [];
    console.log("picture : ", picture);
    picture.map((pic, i) => {
      if(pic.path && pic.path.substring(0,4) === "http"){
        image_list.push({path : pic.path, server_id:pic.server_id, name:pic.name, type:pic.type});
      } else {
        image_list.push(
          {name : pic.fileName || new Date().getTime().toString() + "_" + i + ".jpg"});
      }
    })
    console.log('subjectFile:', subjectFile)
    const send_data = {
      "category": "guide",
      "service": "UpdateGuide",
      "device" : {
        "device_id" : config.deviceInfo.device_id,
        "session_id" : config.sessionInfo.session_id,
      },
      "guide": {
        "id": this.state.guideid,
        "written_by" : {
          "member_id" : config.member.member_id,
          "name" : config.member.nickname,
        },
        "title": subject,
        "description": description,
        "price": price.toString() || "",
        "reference": reference || "",
        //"tag" : tag  ||[],
        "category": categories,
        "image_list" : image_list,
        "voice": 
          subjectFile.uri &&
          {
            name: new Date().getTime().toString() + ".mp3", //subjectFile.name
            duration: subjectFile.duration
          },
        "guide_method": guide === "음성변환" ? "tts" : "",
        "place_num": place_list.length || 0,
        "place_list": place_list || []
      }
    }
    // uri 가 없다는 건 변경된것이 아니다.
    if (!subjectFile.uri){
      delete(send_data.guide.voice)
    }
    if(this.state.orgGuide.description === description){
      delete(send_data.guide.description)
    }
    
    console.log("send_data : ", send_data);
    console.log("send_data : ", JSON.stringify(send_data));
    
    this.setState({loading:true})
    try {
      const resp = await axios.post(config.url.apiSvr, 
        { "data" : send_data },
        { timeout:10000 }
      );
      
      console.log("\\\\UpdGuide1 : ", resp.data.data.guide.id)
      console.log("\\\\UpdGuide1 : ", resp.data.data.guide)
      let guide_image_list = resp.data.data.guide.image_list;
      let promiseList = [];
      console.log('guide_image_list:', guide_image_list)
      guide_image_list && guide_image_list.forEach((image,i) =>{
        let _file = image.path.split(".jpg")[0]
        let index = parseInt(_file[_file.length-1])
        if(picture[index].path && picture[index].path.substring(0,4) !== "http"){
          const saveFileItem = saveFile(image.path, { 
            // uri : picture[i].uri,
            uri : picture[index].path,
            name : new Date().getTime().toString() + "_" + index + ".jpg", 
            type: 'image/jpeg'
          })
          promiseList.push(saveFileItem);
        }
      })
      
      let location_list = resp.data.data.guide.place_list;
      console.log('location_list:', location_list)
      location_list.forEach((place, i) => {
        place.image_files && place.image_files.forEach((image,j) =>{
          let _file = image.path.split(".jpg")[0]
          let index = parseInt(_file[_file.length-1])
          if(locationList[i].picture[index].path && locationList[i].picture[index].path.substring(0,4) !== "http"){
            const saveFileItem = saveFile(image.path, { 
              // uri : locationList[i].picture[j].uri,
              uri : locationList[i].picture[index].path,
              name : new Date().getTime().toString() + "_" + index + ".jpg", 
              type: 'image/jpeg'
            })
            promiseList.push(saveFileItem);
          }
          
        })
      })

      if(guide !== "음성변환"){
        const guideUri = resp.data.data.guide.voice ? resp.data.data.guide.voice.path : ""
        place_list.forEach((place, i)  => {
          place.remoteUri = resp.data.data.guide.place_list[i] ? resp.data.data.guide.place_list[i].path :"";
          place.uri = locationList[i].audio.uri;
        });
        promiseList = [...promiseList,
          guideUri && saveFile(guideUri, { 
            uri : subjectFile.uri,
            name : new Date().getTime().toString() + ".mp3", 
            type: 'audio/mp3'
          }),
          place_list.map(place => {
            return place.remoteUri && saveFile(place.remoteUri, {
              uri : place.uri,
              name : new Date().getTime().toString() + ".mp3",
              type: 'audio/mp3' });
          })
        ];
      }
      await Promise.all(promiseList);
      // this.setState({loading:false},()=>{
      //   this.props.navigation.navigate("make3step");
      // })
      this.setState({
        loading: false,
        openConfirm: true,
        guideId: resp.data.data.guide.id,
      })
    } catch (e){
      console.log("e:",e.message)
      this.setState({loading:false})
    }
  }

  componentDidMount(){
    console.log("Make2Step componentDidMount");

    this.props.navigation.setParams({
      clickNext: this.next,
    });
    const {category} = this.props.navigation.state.params.orgGuide
    let level = "낮음"
    let season = ["사계절"]
    let guide = "음성변환"
    let time = ["하루"]
    let categoryList = [...this.state.categoryList]
    if(category){
      level = category.find(i => {if(i === "난이도 낮음" || i === "난이도 중간" ||  i === "난이도 높음") return true})
      season = category.filter(i => {if(i === "봄" || i === "여름" ||  i === "가을" ||  i === "겨울" ||  i === "사계절") return true})
      guide = category.find(i => {if(i === "음성변환" || i === "오디오파일") return true})
      time = category.filter(i => {if(i === "하루" || i === "아침" ||  i === "낮" ||  i === "저녁" ||  i === "밤") return true})
      categoryList.forEach(item=>{
        if(category.findIndex(i => i === item.key)>-1)
          item.checked = true
      })
      if(level){
        level = level.split(" ")[1]
      }
    }
    console.log("vvvv : ", this.props.navigation.state.params.orgGuide)
    if(this.props.navigation.state.params && this.props.navigation.state.params.whereFrom === "Make1Step") {
      this.setState(
        {
          subject : this.props.navigation.state.params.subject,
          description : this.props.navigation.state.params.description,
          locationList : this.props.navigation.state.params.locationList,
          picture : this.props.navigation.state.params.picture,
          orgGuide : this.props.navigation.state.params.orgGuide,
          level, season, guide, time, categoryList,
          subjectFile : this.props.navigation.state.params.orgGuide.voice,
          guideid : this.props.navigation.state.params.orgGuide.id,
          reference : this.props.navigation.state.params.orgGuide.reference
        });
    }
    this.subs = [
      this.props.navigation.addListener("didFocus", () => {
        console.log("Make2Step didFocus : ", this.props.navigation.state.params.locationList);
        console.log("Make2Step didFocus locationList : ", this.state.locationList);
        this.setState({isFocused:true})
      }),
      this.props.navigation.addListener("willFocus", () => {
        console.log("Make2Step willFocus");
      }),
      this.props.navigation.addListener("willBlur", () => {
        console.log("Make2Step willBlur");
        this.setState({isFocused:false})
      }),
      this.props.navigation.addListener("didBlur", () => {
        console.log("Make2Step didBlur");
      }),
    ];
  }

  componentWillUnmount() {
    console.log("Make2Step componentWillUnmount()");
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }
  checkCategory = (index) => {
    let count = 0;
    let list = [...this.state.categoryList];
    list.map((item, i) => {
      if(item.checked && i !== index ){
        count++;
      }
    })
    if(count === 5){
      return;
    }
    list[index].checked =  !list[index].checked;
    this.setState({categoryList : list})
  }

  selectSeason = (item) => {
    const {season} = this.state;
    let newSeason = season;
    let index = season.indexOf(item);
    if(index >= 0) {
      newSeason = season.filter((ele) => ele !== item)
    } else {
      if(item === '사계절') {
        newSeason = [item]
      } else {
        if(season.indexOf('사계절') >= 0) {
          newSeason = [];
        }
        newSeason.push(item);
      }
    }
    this.setState({
      season: newSeason,
    })
    // this.setState({season : item})
  }
  selectTime = (item) => {
    const {time} = this.state;
    let newTime = time;
    let index = time.indexOf(item);
    if(index >= 0) {
      newTime = time.filter((ele) => ele !== item)
    } else {
      if(item === '하루') {
        newTime = [item]
      } else {
        if(time.indexOf('하루') >= 0) {
          newTime = [];
        }
        newTime.push(item);
      }
    }
    this.setState({
      time: newTime,
    })
    // this.setState({time : item})
  }
  selectLevel = (item) => {
    this.setState({level : item})
  }
  selectPrice = (item) => {
    this.setState({price : item})
  }
  selectGuide = (item) => {
    console.log("item : ", item)
    if(this.state.orgGuide.id && (this.state.guide !== item)){
      this.setState({
        alertOpen:true,
        alertMsg:"가이드 방식은 수정할 수 없습니다."
      })
      return;
    }

    this.setState({guide : item})
    if(item === "오디오파일"){
      this.changeGuideStyle(true)
    } else{
      this.changeGuideStyle(false)
    }
  }

  renderSeason = () => {
    const season = ["사계절", "","","", "봄", "여름", "가을", "겨울"];
    return (
      <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10, justifyContent:"space-between"}}>
        {season.map((item, index) =>{
          return (
            <View key={index} >
            {item?
              <TouchableOpacity style={{...styles.categoryItem, 
                // ...{backgroundColor : item === this.state.season ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                ...{backgroundColor : this.state.season.indexOf(item) >= 0 ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                onPress={()=>{this.selectSeason(item)}}>
                {/* <StyledText style={{color: item === this.state.season ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}> */}
                <StyledText style={{color: this.state.season.indexOf(item) >= 0 ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}>
                  {item}
                </StyledText>
              </TouchableOpacity>
              :
              <View style={{height : 24,
                backgroundColor: "white", 
                width:this.state.cardWidth }} 
                >
              </View>
            }
            </View>
          )
        })}
        </View>
    )
  }

  renderTime = () => {
    // const time = ["하루", "아침", "오전", "", "오후", "저녁", "밤",""];
    const time = ["하루", "", "", "", "아침", "낮", "저녁", "밤"];
    return (
      <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10, justifyContent:"space-between"}}>
        {time.map((item, index) =>{
          return (
            <View key={index} >
            {item?
              <TouchableOpacity style={{...styles.categoryItem, 
                // ...{backgroundColor : item === this.state.time ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                ...{backgroundColor : this.state.time.indexOf(item) >= 0 ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                onPress={()=>{this.selectTime(item)}}>
                {/* <StyledText style={{color: item === this.state.time ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}> */}
                <StyledText style={{color: this.state.time.indexOf(item) >= 0 ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}>
                  {item}
                </StyledText>
              </TouchableOpacity>
              :
              <View style={{height : 24,
                backgroundColor: "white", 
                width:this.state.cardWidth }} 
                >
              </View>
            }
            </View>
          )
        })}
        </View>
    )
  }

  renderLevel = () => {
    const level = ["낮음", "중간", "높음", ""];
    /*price : ["무료", "10도넛", "30도넛", "50도넛"],
    
    
    */
    return (
      <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10, justifyContent:"space-between"}}>
        {level.map((item, index) =>{
          return (
            <View key={index} >
            {item?
              <TouchableOpacity style={{...styles.categoryItem, 
                ...{backgroundColor : item === this.state.level ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                onPress={()=>{this.selectLevel(item)}}>
                <StyledText style={{color: item === this.state.level ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}>
                  {item}
                </StyledText>
              </TouchableOpacity>
              :
              <View style={{height : 24,
                backgroundColor: "white", 
                width:this.state.cardWidth }} 
                >
              </View>
            }
            </View>
          )
        })}
        </View>
    )
  }
  renderPrice = () => {
    
    const price = ["무료", "10도넛", "30도넛", "50도넛"];
    return (
      <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10, justifyContent:"space-between"}}>
        {price.map((item, index) =>{
          return (
            <View key={index} >
            {item?
              <TouchableOpacity style={{...styles.categoryItem, 
                ...{backgroundColor : item === this.state.price ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                onPress={()=>{this.selectPrice(item)}}>
                <StyledText style={{color: item === this.state.price ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}>
                  {item}
                </StyledText>
              </TouchableOpacity>
              :
              <View style={{height : 24,
                backgroundColor: "white", 
                width:this.state.cardWidth }} 
                >
              </View>
            }
            </View>
          )
        })}
        </View>
    )
  }

  renderGuide = () => {
    const guide = ["음성변환", "오디오파일", "", ""];
    return (
      <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10, justifyContent:"space-between"}}>
        {guide.map((item, index) =>{
          return (
            <View key={index} >
            {item?
              <TouchableOpacity style={{...styles.categoryItem, 
                ...{backgroundColor : item === this.state.guide ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                onPress={()=>{this.selectGuide(item)}}>
                <StyledText style={{color: item === this.state.guide ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}>
                  {item}
                </StyledText>
              </TouchableOpacity>
              :
              <View style={{height : 24,
                backgroundColor: "white", 
                width:this.state.cardWidth }} 
                >
              </View>
            }
            </View>
          )
        })}
        </View>
    )
  }


  renderCategory = () => {
    this.state.categoryList.map((item, index) => {
      //console.log("item : ", item)
      //console.log("item : ", index)
    });
    return (
        <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10, justifyContent:"space-between"}}>
        {this.state.categoryList.map((item, index) =>{
          return (
            <TouchableOpacity key={index} style={{...styles.categoryItem, ...{backgroundColor : item.checked ? "#266753" : "#ffffff", width:this.state.cardWidth }}} onPress={()=>{this.checkCategory(index)}}>
              <StyledText style={{color: item.checked ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}>{item.key}</StyledText>
            </TouchableOpacity>
          )
        })}
        </View>
    )
  }

  changeTag = (tag) => {
    this.setState({tag})
  }

  changeReferene = (reference) => {
    this.setState({reference})
  }
  changeGuideStyle = (bool) => {
    console.log("changeGuideStyle : ", bool)
    if(bool){
      console.log("Addfile : ", this.state.subject)
      this.props.navigation.navigate("addfile",{
        subject : this.state.subject,
        locationList2 : this.state.locationList,
        updateFlag : this.state.orgGuide.id ? true : false,
        onGoBack : this.selectLocation
      });
    }
  }
  selectLocation = (subjectFile, locationList) => {
    console.log("selectLocatoin  :", subjectFile, locationList)
    this.setState({locationList, subjectFile});
  }

  changePirce = (price) => {
    if(!isNumeric(price))
    {
      alert("숫자만 입력 가능합니다.");
      return;
    }
    this.setState({price})
  }

  goHome = () => {
    this.props.navigation.popToTop();
    this.props.navigation.navigate("home")
  }

  goListen = () => {
    const {guideId, subject} = this.state.guideId;

    this.props.navigation.popToTop();
    this.props.navigation.navigate("make0step")
    // console.log('goListen:', this.state.guideId)
    // AsyncStorage.setItem("itemTitle", subject);
    // AsyncStorage.setItem("itemId", guideId).then(
    //   this.props.navigation.navigate("listen")
    // )
  }

  render() {
    const {reference, loading, isFocused, tag} = this.state;
    return (
      <View style={styles.container}>
        {/* <Modal isVisible={this.state.openConfirm}
          onBackdropPress={this.pressReportButton}> */}
          <Confirm visible={this.state.openConfirm}
            onBackdropPress={this.pressReportButton}
            message={'당신의 여행이 등록되었습니다'}
            leftButtonLabel={'홈으로'}
            rightButtonLabel={'여행만들기로'}
            onLeftButtonPress={this.goHome}
            onRightButtonPress={this.goListen} />
          <Confirm visible={this.state.alertOpen}
            oneButton={true}
            onBackdropPress={this.pressReportButton}
            message={this.state.alertMsg}
            onCenterButtonPress={() => {
              this.setState({ alertOpen: false })
            }} />
        {/* </Modal> */}
        <ScrollView style={styles.scrollContainer}>
          {loading && <Loading color={"grey"}/>}
          {isFocused && <StatusBar barStyle="dark-content" backgroundColor={'white'}/>}
          <View style={styles.body}>
              <View style={styles.item1}>
                <View style={{flex:1, margin : 10}}>
                  <StyledText style={{flex:2, color: "#26675e", fontSize: 14,}}>출처 및 참고문헌</StyledText>  
                  <StyledTextInput style={{textAlignVertical: "top", fontSize: 12, flex:8, color:"grey"}}
                    autoCorrect={false} placeholder={`여행을 만들 때 참고한 내용을 적어주세요.\n링크도 좋아요 :)`}
                    placeholderTextColor="grey" multiline onChangeText={this.changeReferene}>{reference}
                  </StyledTextInput>
                </View>
              </View>
              {/*
              <View style={styles.item5}>
                <View style={{flex:1, margin : 10}}>
                  <StyledText style={{flex:2, color: "#26675e", fontSize: 14 }}>태그</StyledText>  
                  <StyledTextInput style={{textAlignVertical: "top", fontSize: 12, flex:4, color:"grey"}}
                    autoCorrect={false} placeholder={`여행의 태그를 자유롭게 달아주세요.)`}
                    placeholderTextColor="grey" numberOfLines={1} onChangeText={this.changeTag}>{tag}
                  </StyledTextInput>
                </View>
              </View>
              */}
              <View style={styles.item2}>
                <View style={{flex:1, alignItems:"flex-end", margin: 10, flexDirection:"row"}}>
                  <StyledText style={{color: "#26675e", fontSize: 14,}}>가이드방식</StyledText>  
                  <StyledText style={{marginLeft:10, color: "#707070", fontSize: 8,}}>음성변환 선택시 올리기를 누르면 자동으로 변환됩니다.</StyledText>  
                </View>
                <View style={{flex:1, marginLeft:10, marginRight:10, flexDirection:"row"}}>
                {this.renderGuide()}
                </View>
              </View>
              <View style={styles.item4}>
                <View style={{flex:1, alignItems:"flex-end", margin: 10, flexDirection:"row"}}>
                  <StyledText style={{color: "#26675e", fontSize: 14,}}>여행키워드</StyledText>  
                  <StyledText style={{marginLeft:10, color: "grey", fontSize: 8,}}>최대 5개 선택 가능합니다.</StyledText>  
                </View>
                <View style={{flex:4, marginLeft:10, marginRight : 10, }}>
                  {this.renderCategory()}
                </View>
              </View>
              <View style={styles.item2}>
                <View style={{flex:1, alignItems:"flex-end", margin: 10, flexDirection:"row"}}>
                  <StyledText style={{color: "#26675e", fontSize: 14,}}>계절</StyledText>  
                </View>
                <View style={{flex:1, marginLeft:10, marginRight:10}}>
                  {this.renderSeason()}
                </View>
              </View>
              <View style={styles.item2}>
                <View style={{flex:1, alignItems:"flex-end", margin: 10, flexDirection:"row"}}>
                  <StyledText style={{color: "#26675e", fontSize: 14,}}>시간</StyledText>  
                </View>
                <View style={{flex:1, marginLeft:10, marginRight:10}}>
                  {this.renderTime()}
                </View>
              </View>
              <View style={styles.item3}>
                <View style={{flex:1, alignItems:"flex-end", margin: 10, flexDirection:"row"}}>
                  <StyledText style={{color: "#26675e", fontSize: 14,}}>난이도</StyledText>  
                </View>
                <View style={{flex:1, marginLeft:10, marginRight:10}}>
                {this.renderLevel()}
                </View>
              </View>
              {/*<View style={styles.item3}>
                <View style={{flex:1, alignItems:"flex-end", margin: 10, flexDirection:"row"}}>
                  <StyledText style={{color: "#26675e", fontSize: 14,}}>가격</StyledText>  
                  <StyledText style={{marginLeft:10, color: "#707070", fontSize: 8,}}>무료로 등록할 경우 1도넛이 됩니다.</StyledText>  
                </View>
                <View style={{flex:1, marginLeft:10, marginRight:10}}>
                {this.renderPrice()}
                </View>
      </View>*/}
              <View style={{height:50}}>

              </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles =StyleSheet.create({
  container : {
    height : "100%",
    flex : 1,
    justifyContent:"center",
    paddingBottom : config.marginBottom,
    backgroundColor : config.colors.makeItemColor,
  },
  scrollContainer : {
    height : "100%",
    flex : 1,
    paddingBottom : config.marginBottom,
    backgroundColor : config.colors.makeItemColor,
  },
  body : {
    height : "100%",
    marginLeft : 10,
    marginRight : 10
  },
  item1 : {
    height : 150,
    width : "100%",
    backgroundColor: "white",
    borderWidth : 1,
    marginTop : 10,
    borderRadius: 5,
    borderColor : "#707070",
    //justifyContent : "center",
    //alignItems : "center"
  },
  item2 : {
    height : 100,
    width : "100%",
  },
  item3 : {
    height : 100,
    width : "100%",
  },
  item4 : {
    height : 160,
    width : "100%",
    marginTop : 10,
  },
  guideItem : {
    flex: 2,
    backgroundColor: "white",
    borderWidth : 1,
    borderRadius: 5,
    borderColor : "#707070",
    marginRight : 10,
    justifyContent : "center",
  },
  categoryItem : {
    height : 24,
    width : config.deviceInfo.width / 5,
    backgroundColor: "white",
    borderWidth : 1,
    borderRadius: 5,
    borderColor : "#707070",
    marginRight : 5,
    marginBottom : 5,
    justifyContent : "center"
  }
});