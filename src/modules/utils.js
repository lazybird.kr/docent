import { Dimensions, Platform } from 'react-native';
import axios from 'axios';

export function isIphoneX() {
  const dimen = Dimensions.get('window');
  return (
      Platform.OS === 'ios' &&
      !Platform.isPad &&
      !Platform.isTVOS &&
      ((dimen.height === 812 || dimen.width === 812) || (dimen.height === 896 || dimen.width === 896))
  );
}

export function getBottomSpace() {
  return isIphoneX() ? 34 : 0;
}

export const getFormatDate = (date, format) => { 
  if(!date) {
      return null;
  }
  let year = date.getFullYear();	//yyyy 
  let month = (1 + date.getMonth());	//M 
  month = month >= 10 ? month : '0' + month;	//month 두자리로 저장 
  let day = date.getDate();	//d 
  day = day >= 10 ? day : '0' + day;	//day 두자리로 저장 

  return year + format + month + format + day; 
}


export const differenceOfArrays = (array1, array2)  => {
  const temp = [];
  
  for(var i in array2) {
      if(!array1.includes(array2[i])) temp.push(array2[i]);
  }
  return temp.sort((a,b) => a-b);
}

export function strncmp(str1, str2, n) {
  str1 = str1.substring(0, n);
  str2 = str2.substring(0, n);
  return ( ( str1 === str2 ) ? 0 :
                              (( str1 > str2 ) ? 1 : -1 ));
}


export function formatBytes(bytes,decimals){
    if(bytes == 0) return '0 Bytes';
    var k = 1024,
        dm = decimals <= 0 ? 0 : decimals || 2,
        sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  export function isNumeric(num, opt){
    // 좌우 trim(공백제거)을 해준다.
    num = String(num).replace(/^\s+|\s+$/g, "");
   
    if(typeof opt == "undefined" || opt == "1"){
      // 모든 10진수 (부호 선택, 자릿수구분기호 선택, 소수점 선택)
      var regex = /^[+\-]?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+){1}(\.[0-9]+)?$/g;
    }else if(opt == "2"){
      // 부호 미사용, 자릿수구분기호 선택, 소수점 선택
      var regex = /^(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+){1}(\.[0-9]+)?$/g;
    }else if(opt == "3"){
      // 부호 미사용, 자릿수구분기호 미사용, 소수점 선택
      var regex = /^[0-9]+(\.[0-9]+)?$/g;
    }else{
      // only 숫자만(부호 미사용, 자릿수구분기호 미사용, 소수점 미사용)
      var regex = /^[0-9]$/g;
    }
   
    if( regex.test(num) ){
      num = num.replace(/,/g, "");
      return isNaN(num) ? false : true;
    }else{ return false;  }
  }

  export function formatInt(num) {
   
      if(num==0) return 0;
   
      var reg = /(^[+-]?\d+)(\d{3})/;
      var n = (num + '');
   
      while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');
   
      return n;
  }
  
  export function saveFile(fileServerPath, file){
    var formData = new FormData();
    formData.append('file', file);
    console.log('saveFile start : ', fileServerPath, file);
    return new Promise(async function(resolve, reject) {
      try {
        const response = await axios.post(
          fileServerPath,
          formData,
          );
        if (response.status === 200) {
          console.log('saveFile end!!')
          resolve(response.data);
        }
        else {
          console.log('saveFile error!!', response, fileServerPath, file)
          const errMsg = response.status ? "saveFile Failed: " + response.status.toString() : 
              "saveFile Failed";
          throw new Error(errMsg)
        }
      } catch(e) {
        console.log('saveFile error catch!! : ', e)
        reject(e);
      }
    })
  }
