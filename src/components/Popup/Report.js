import React, { Component } from 'react';
import {View, StyleSheet, TouchableOpacity, Image, Switch} from 'react-native';
import config from 'src/config';
import {StyledText} from 'src/components/styledComponents';
import Modal from 'react-native-modal';
import CheckBox from '@react-native-community/checkbox';
import { Platform } from 'react-native';

export default class Report extends Component {
  constructor(props) {
    super();
    this.state = { 
      checkBox : [],
    }
  }
  

  confirm = () => {
    const index = this.state.checkBox.indexOf(true);
    if(index !== -1){
      this.props.onRightButtonPress({index})
    } else {
        Alert.alert(
            "",
            "신고 이유를 선택해 주세요.",
            [
              {text:"OK", onPress: ()=> {}}
            ],
            {cancelable : false}
            );
    }
  }
  checkBox = (index) => {
    let _checkBox = []
    _checkBox[index] = true;
    this.setState({checkBox : _checkBox})

  }
  
  render () {
    const {
      visible,
      oneButton,
      onBackdropPress,
      onBackButtonPress,
      onLeftButtonPress,
      onCenterButtonPress,
      onRightButtonPress,
      leftButtonLabel,
      centerButtonLabel,
      rightButtonLabel,
    } = this.props;

    return (
      <Modal isVisible={visible}
      onBackdropPress={onBackdropPress}
      >
        <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
          <View style={styles.container}>  
              <View style={{flex:4, justifyContent:"center", padding:20, }}>
                <View style={{flex:1, justifyContent:"center"}}>
                  <StyledText style={{fontFamily:config.defaultBoldFontFamily, fontSize:20, color:"#292929"}}>
                    신고
                  </StyledText>
                </View>
                <View style={{flex:3, justifyContent:"center"}}>
                    <View style={{flex:1, alignItems:"center", flexDirection:"row"}}>
                      <StyledText style={{color:"#292929", fontSize:16,flex:1}}>
                        영리목적 / 홍보
                      </StyledText>
                      <CheckBox
                          style={styles.checkBox}
                          animationDuration={0} 
                          tintColors={{true:"#d64409"}}
                          onTintColor={"#d64409"} onCheckColor={"#ffffff"}
                          onFillColor={"#d64409"}
                          value={this.state.checkBox[0]}
                          onValueChange={()=>this.checkBox(0)}
                      />
                    </View>
                    <View style={{flex:1, alignItems:"center", flexDirection:"row"}}>
                      <StyledText style={{color:"#292929", fontSize:16,flex:1}}>
                        음란 / 선정성
                      </StyledText>
                      <CheckBox 
                          style={styles.checkBox}
                          animationDuration={0} 
                          tintColors={{true:"#d64409"}}
                          onTintColor={"#d64409"} onCheckColor={"#ffffff"}
                          onFillColor={"#d64409"}
                          value={this.state.checkBox[1]}
                          onValueChange={()=>this.checkBox(1)}
                      />
                    </View>
                    <View style={{flex:1, alignItems:"center", flexDirection:"row"}}>
                      <StyledText style={{color:"#292929", fontSize:16,flex:1}}>
                        부적절
                      </StyledText>
                      <CheckBox 
                          style={styles.checkBox}
                          animationDuration={0} 
                          tintColors={{true:"#d64409"}}
                          onTintColor={"#d64409"} onCheckColor={"#ffffff"}
                          onFillColor={"#d64409"}
                          value={this.state.checkBox[2]}
                          onValueChange={()=>this.checkBox(2)}
                                    />
                    </View>
                    <View style={{flex:1, alignItems:"center", flexDirection:"row"}}>
                      <Image source={config.images.warning} 
                            style={{width : 20, height:20, resizeMode:"contain", marginRight:10}}/>
                      <View>
                        <StyledText style={{color:"#d64409"}}>
                          {"누적 신고시 도슨투고에서 확인할 때까지"}
                        </StyledText>
                        <StyledText style={{color:"#d64409"}} multiline>
                          {"이 여행은 블라인드 처리됩니다."}
                        </StyledText>
                      </View>
                      
                    </View>
                </View>
              </View>
            <View style={{flex:1, borderTopColor:"#bfbfbf", borderTopWidth:1}}>
              {oneButton?
                <View style={{flex: 1}}>
                <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                  onPress={onCenterButtonPress}>
                  <StyledText style={styles.label2}>{centerButtonLabel || "확인하기"}</StyledText>
                </TouchableOpacity>
                </View>
              :
              <View style={{flex:1, flexDirection:"row"}}>
                <View style={{flex:1, borderRightWidth:1, borderRightColor:"#bfbfbf"}}>
                  <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                      onPress={onLeftButtonPress}>
                    <StyledText style={styles.label1}>{leftButtonLabel || "취소하기"}</StyledText>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1}}>
                  <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                      onPress={this.confirm}>
                    <StyledText style={styles.label2}>{rightButtonLabel || "확인하기"}</StyledText>
                  </TouchableOpacity>
                </View>
                </View>
              }
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    height:config.deviceInfo.width - 60,
    width:config.deviceInfo.width - 60, 
    borderRadius:20, 
    backgroundColor:"white"
  },
  confirmLogo: {
    width: 80,
    height: 80,
  },
  label1: {
    fontSize:16, 
    fontFamily:config.defaultBoldFontFamily, 
    color:"#bfbfbf"
  },
  label2: {
    fontSize:16, 
    fontFamily:config.defaultBoldFontFamily, 
    color:"#d64409"
  },
  message: {
    fontSize:16,
    color: config.colors.defaultText, 
    margin: 30,
    textAlign: 'center',
  },
  checkBox: {
    transform: Platform.OS === "android" ? [{scaleX : 1.0}, {scaleY: 1.0}] :[{scaleX : 0.8}, {scaleY: 0.8}] 
   }
})