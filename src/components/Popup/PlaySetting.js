import React, { Component } from 'react';
import {View, StyleSheet, TouchableOpacity, Image, Switch} from 'react-native';
import config from 'src/config';
import {StyledText} from 'src/components/styledComponents';
import Modal from 'react-native-modal';
import showToast from 'src/components/Toast';
import { Platform } from 'react-native';

export default class PlaySettins extends Component {
  constructor(props) {
    super();
    this.state = { 
      autoPlay : false,
      beep : false,
      onlyWifi : false
    }
  }
  toggleAutoPlay = () =>{
    this.setState({autoPlay : !this.state.autoPlay})
  }
  toggleBeep = () =>{
    if(this.state.autoPlay){
      this.setState({beep : !this.state.beep})
    } else {
      showToast('자동 재생 설정이 켜 있을 경우에만 유효한 설정입니다.')
    }
  }
  toggleOnlyWifi = () =>{
    this.setState({onlyWifi : !this.state.onlyWifi})
  }

  confirm = () => {
    const {autoPlay, beep, onlyWifi} = this.state;
    this.props.onRightButtonPress({autoPlay, beep, onlyWifi:!onlyWifi})
  }

  componentDidMount(){
    console.log("config.useAutoPlay :", config.useAutoPlay)
    this.setState({
      autoPlay : config.useAutoPlay,
      beep : config.useBeep,
      onlyWifi : !config.useMobileData
    })
  }
  componentDidUpdate(prevProps, prevState){
    if(prevState.autoPlay !== this.state.autoPlay){
      if(!this.state.autoPlay){
        this.setState({beep: false})
      }
    }
  }
  

  render () {
    const {
      visible,
      oneButton,
      onBackdropPress,
      onBackButtonPress,
      onLeftButtonPress,
      onCenterButtonPress,
      onRightButtonPress,
      leftButtonLabel,
      centerButtonLabel,
      rightButtonLabel,
    } = this.props;

    const {autoPlay, beep, onlyWifi} = this.state; 

    return (
      <Modal isVisible={visible}
      onBackdropPress={onBackdropPress}
      >
        <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
          <View style={styles.container}>  
              <View style={{flex:4, justifyContent:"center", padding:20, }}>
                <View style={{flex:1, justifyContent:"center"}}>
                  <StyledText style={{fontFamily:config.defaultBoldFontFamily, fontSize:20, color:"#292929"}}>
                    재생 설정
                  </StyledText>
                </View>
                <View style={{flex:3, justifyContent:"center"}}>
                    <View style={{flex:1, alignItems:"center", flexDirection:"row"}}>
                      <StyledText style={{color:"#292929", fontSize:16,flex:1}}>
                        자동 재생
                      </StyledText>
                      <Switch
                        style={styles.switch}
                        trackColor={{false : "#707070", true : "#26675e"}}
                        thumbColor={"#ffffff"}
                        thumbSize={5}
                        ios_backgroundColor="#ffffff"
                        onValueChange={this.toggleAutoPlay}
                        value={this.state.autoPlay}
                      />
                    </View>
                    {/* <View style={{flex:1, alignItems:"center", flexDirection:"row"}}>
                      <StyledText style={{color:"#292929", fontSize:16,flex:1}}>
                        자동 재생 중 비프음 재생
                      </StyledText>
                      <Switch
                        style={styles.switch}
                        trackColor={{false : "#707070", true : "#26675e"}}
                        thumbColor={"#ffffff"}
                        thumbSize={5}
                        ios_backgroundColor="#ffffff"
                        onValueChange={this.toggleBeep}
                        value={this.state.beep}
                      />
                    </View> */}
                    <View style={{flex:1, alignItems:"center", flexDirection:"row"}}>
                      <StyledText style={{color:"#292929", fontSize:16,flex:1}}>
                        Wi-Fi 환경에서만 재생
                      </StyledText>
                        <Switch
                          style={styles.switch}
                          trackColor={{false : "#707070", true : "#26675e"}}
                          thumbColor={"#ffffff"}
                          thumbSize={5}
                          ios_backgroundColor="#ffffff"
                          onValueChange={this.toggleOnlyWifi}
                          value={this.state.onlyWifi}
                        /> 
                    </View>
                    <View style={{flex:1, alignItems:"center", flexDirection:"row"}}>
                      <Image source={config.images.warning} 
                            style={{width : 20, height:20, resizeMode:"contain", marginRight:10}}/>
                      <StyledText style={{color:"#d64409"}}>
                        해당 내용은 설정에 동일하게 반영됩니다.
                      </StyledText>
                    </View>
                </View>
              </View>
            <View style={{flex:1, borderTopColor:"#bfbfbf", borderTopWidth:1}}>
              {oneButton?
                <View style={{flex: 1}}>
                <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                  onPress={onCenterButtonPress}>
                  <StyledText style={styles.label2}>{centerButtonLabel || "확인하기"}</StyledText>
                </TouchableOpacity>
                </View>
              :
              <View style={{flex:1, flexDirection:"row"}}>
                <View style={{flex:1, borderRightWidth:1, borderRightColor:"#bfbfbf"}}>
                  <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                      onPress={onLeftButtonPress}>
                    <StyledText style={styles.label1}>{leftButtonLabel || "취소하기"}</StyledText>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1}}>
                  <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                      onPress={this.confirm}>
                    <StyledText style={styles.label2}>{rightButtonLabel || "확인하기"}</StyledText>
                  </TouchableOpacity>
                </View>
                </View>
              }
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    height:config.deviceInfo.width - 60,
    width:config.deviceInfo.width - 60, 
    borderRadius:20, 
    backgroundColor:"white"
  },
  confirmLogo: {
    width: 80,
    height: 80,
  },
  label1: {
    fontSize:16, 
    fontFamily:config.defaultBoldFontFamily, 
    color:"#bfbfbf"
  },
  label2: {
    fontSize:16, 
    fontFamily:config.defaultBoldFontFamily, 
    color:"#d64409"
  },
  message: {
    fontSize:16,
    color: config.colors.defaultText, 
    margin: 30,
    textAlign: 'center',
  },
  switch: {
    transform: Platform.OS === "android" ? [{scaleX : 1.3}, {scaleY: 1.3}] : [{scaleX : 1.0}, {scaleY: 1.0}]
   }
})