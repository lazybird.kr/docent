import React, { Component } from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import config from 'src/config';
import {StyledText} from 'src/components/styledComponents';
import Modal from 'react-native-modal';
import { ScrollView } from 'react-native-gesture-handler';

export default class Confirm extends Component {
  constructor(props) {
    super();
    this.state = { 
      isOpen: false,
    }
  }
  

  render () {
    const {
      visible,
      oneButton,
      onBackdropPress,
      onBackButtonPress,
      onLeftButtonPress,
      onCenterButtonPress,
      onRightButtonPress,
      message,
      leftButtonLabel,
      centerButtonLabel,
      rightButtonLabel,
      noImage,
      image,
      imageStyle
  }
      = this.props;

    return (
      <Modal isVisible={visible}
      onBackdropPress={onBackdropPress}
      >
        <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
          <View style={styles.container}>
            {
              noImage?
              <View style={{flex:4, justifyContent:"center"}}>
                <ScrollView>
                  <StyledText style={styles.message, {textAlign: "left", margin: 20}}>{message || ""}</StyledText>
                </ScrollView>
              </View>
              :
              <View style={{flex:4, justifyContent:"center", alignItems:"center"}}>
                <Image source={image? image : config.images.popIcon} style={[styles.confirmLogo, imageStyle]}></Image>
                {
                  message &&
                  <StyledText style={styles.message}>{message || ""}</StyledText>
                }
              </View> 
            }
            
            <View style={{flex:1, borderTopColor:"#bfbfbf", borderTopWidth:1}}>
              {oneButton?
                <View style={{flex: 1}}>
                <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                  onPress={onCenterButtonPress}>
                  <StyledText style={styles.label2}>{centerButtonLabel || "확인하기"}</StyledText>
                </TouchableOpacity>
                </View>
              :
              <View style={{flex:1, flexDirection:"row"}}>
                <View style={{flex:1, borderRightWidth:1, borderRightColor:"#bfbfbf"}}>
                  <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                      onPress={onLeftButtonPress}>
                    <StyledText style={styles.label1}>{leftButtonLabel || "취소하기"}</StyledText>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1}}>
                  <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                      onPress={onRightButtonPress}>
                    <StyledText style={styles.label2}>{rightButtonLabel || "확인하기"}</StyledText>
                  </TouchableOpacity>
                </View>
                </View>
              }
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    height:config.deviceInfo.width - 60,
    width:config.deviceInfo.width - 60, 
    borderRadius:20, 
    backgroundColor:"white"
  },
  confirmLogo: {
    width: 80,
    height: 80,
  },
  label1: {
    fontSize:16, 
    fontFamily:config.defaultBoldFontFamily, 
    color:"#bfbfbf"
  },
  label2: {
    fontSize:16, 
    fontFamily:config.defaultBoldFontFamily, 
    color:"#d64409"
  },
  message: {
    fontSize:16,
    color: config.colors.defaultText, 
    margin: 30,
    textAlign: 'center',
  }
})