import React, {Component, useRef} from 'react';
import {Text,TextInput} from 'react-native';
import config from 'src/config';

export class StyledText extends Component {
        render() {
                const {props} = this;
                return(
                        <Text {...props} style={[{fontFamily: config.defaultFontFamily, includeFontPadding: false, textAlignVertical : "center"},
                        props.style]}>{props.children}</Text>
                )
        }
}

export class StyledTextInput extends Component {
        getInnerRef = () => this.ref
        render() {
                const {props} = this;
                return (
                        <TextInput {...props} 
                        ref={(r) => this.ref = r}
                        style={[{fontFamily: config.defaultFontFamily, includeFontPadding: false, textAlignVertical : "center"},
                                props.style]}>{props.children}</TextInput>
                )
        }
}
