import React from 'react';
import {
    StyleSheet,
    View,
    ActivityIndicator,
} from 'react-native';
import Modal from 'react-native-modal';

export default function Loading(props) {
    let indicatorSize = "large";
    let indicatorColor = "#0000ff";
    // console.log('props', props);
    if (props) {
        const {size, color} = props;
        if (size) {
            indicatorSize = size;
        }
        if (color) {
            indicatorColor = color;
        }
    }
    /*return (
        <View style={{...StyleSheet.absoluteFillObject,position:"absolute", height:"100%", width:"100%", zIndex:100}}>
            <View style={[styles.container, styles.horizontal]}>
                <ActivityIndicator
                    size={indicatorSize}
                    color={indicatorColor} />
            </View>
        </View>
        
    );*/
    return (
        <Modal isVisible={true}  /*backdropColor='transparent'*/>
            <ActivityIndicator
                    size={indicatorSize}
                    color={indicatorColor} />
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    
    },
    horizontal: {
    }
});
