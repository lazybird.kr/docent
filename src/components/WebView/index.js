import React, { useState, useEffect } from 'react';  
import {
    View, StatusBar,ScrollView, TouchableOpacity,
    StyleSheet, FlatList, 
} from 'react-native';
import { WebView } from 'react-native-webview';
import {HeaderTitle, BackIcon, HeaderImage} from 'src/components/header';
import config from 'src/config';
import {getStatusBarHeight} from "react-native-status-bar-height";
import Loading from 'src/components/Loading';
import {StyledText} from 'src/components/styledComponents';

export default function WebViewScreen(props) {

    const {navigation} = props;
    const {uri, title} = navigation.state.params;
    const [isLoading, setLoading] = useState(false);
    useEffect(() => {
      setLoading(true)
      return () => {
      }
    }, []);
    const handleWebView = () => {

    }
    const handleWebViewLoad = () => {
      setLoading(false)
    }

    
    if(title === "오픈소스 라이선스"){
       return <Licenses />
    }

    const INJECTED_JAVASCRIPT = `(function() {
      const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta);
    })();`;

    return (
        <View style={styles.container}>
            {isLoading && <Loading color={'#01a3ff'} />}
            <StatusBar barStyle="dark-content" backgroundColor={"#ffffff"}/>
            <WebView source={{uri}} 
              onMessage={handleWebView} 
              injectedJavaScript={INJECTED_JAVASCRIPT}
              onLoad={handleWebViewLoad}/>
        </View>

    )
}

WebViewScreen.navigationOptions =  ({ navigation}) => {
    return {
      headerLeft: () => <BackIcon navigation={navigation} color={"black"}/>,
      headerTitle :() => <HeaderTitle style={{alignSelf:"center", fontSize:16}} title={navigation.getParam('title')}/>,
      // headerTitle :() => <HeaderImage img={config.images.busStation}
      //     style={{alignSelf:"center",width:50, height:50, resizeMode:"contain"}}  />,
      headerRight : () => <></>,
      headerStyle : {
        backgroundColor: "#ffffff",
        borderBottomWidth: 0.5,
        borderColor: config.colors.lineColor,
        elevation: 0,       //remove shadow on Android
        shadowOpacity: 0,   //remove shadow on iOS
        height: config.size._184px + 19 + (Platform.OS === 'ios' ? getStatusBarHeight() : 0),
      }
    }
  }

const styles=StyleSheet.create({
    container: {
      flex:1,
      paddingLeft:10,
      paddingRight:10,
      backgroundColor:"white"
    },
    
  });



//license용
const renderSeparator = () => {
  return (
    <View
      style={{
        height: 1,
        width: '100%',
        backgroundColor: '#CED0CE',
        //marginLeft: '14%',
        alignSelf : "center"
      }}
    />
  );
}

const renderItem = (item)  => {
  
  return (
      <View style={{height : 70, justifyContent:"center", paddingLeft:10}}>
        <TouchableOpacity onPress={() => {}}>
          <View style={{flexDirection:"row"}}>
            <View style={{flexDirection:"column", flex:9}}>
              <StyledText style={{fontSize: 16}}>{item.name}</StyledText>
              <View style={{flexDirection:"row"}}>
                <StyledText style={{fontSize: 14, color : "grey"}}>{item.version}</StyledText>
                <StyledText style={{fontSize: 14, color : "grey"}}>{`(${item.licenseSpecs.licenses})`}</StyledText>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
      
  )
}


//function Licenses(props){
//  const licenses = require('src/../licenses.json');
//  const numberRegex = /\d+(\.\d+)*/;
//  const atRegex = /(?:@)/gi;
//
//  let finalLicense = [];
//
//  for (let [libraryName, librarySpecs] of Object.entries(licenses)){
//    const version = libraryName.match(numberRegex);
//    const nameWithoutVersion = libraryName.replace(atRegex, '').replace(version ? version[0] : '', '');
//    finalLicense.push({ name: nameWithoutVersion, version: version ? version[0] : '', licenseSpecs: librarySpecs });
//  }

//  return (
//    <View style={{...styles.container, backgroundColor:"white"}}>
//      <FlatList
//        data={finalLicense}
//        renderItem={({ item }) => {
//          return renderItem(item);
//        }}
//        ItemSeparatorComponent={renderSeparator}
//        keyExtractor={(item, index) => index.toString()}
//      />
//    </View>
//  );
//  
//}
