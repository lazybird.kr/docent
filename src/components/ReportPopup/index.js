import React, {Component}  from "react";
import {View, StyleSheet,Keyboard, ScrollView, Platform, Text, TouchableOpacity} from "react-native";
import config from 'src/config';
import {StyledText} from 'src/components/styledComponents';
import CheckBox from '@react-native-community/checkbox';
import { Alert } from "react-native";

export default class ReportPopup extends Component {
    constructor() {
        super();
        this.state = { 
            isReported : false,  
            checkBox : [],
        }
    }

    
    componentDidMount() {
    
    }

    componentWillUnmount() {
    
    }

    report = () => {
        const index = this.state.checkBox.indexOf(true);
        console.log("index :", index );
        if(index !== -1){
            setTimeout(()=>this.setState({isReported:true}), 500);
        } else {
            Alert.alert(
                "",
                "신고 이유를 선택해 주세요.",
                [
                  {text:"OK", onPress: ()=> {}}
                ],
                {cancelable : false}
                );
        }
        
    }
    checkBox = (index) => {
        let _checkBox = []
        _checkBox[index] = true;
        this.setState({checkBox : _checkBox})

    }
    
      
    render() {

        const  {isReported} = this.state;

        return(
        <View style={{flex:1, justifyContent:"center", alignItems:"center"}}>
            {
                !isReported ? 
                    <View style={styles.container}>
                        <View style={{flex:5,justifyContent:"center"}}>
                            <View style={{marginHorizontal:20}}>
                                <View style={{marginBottom:20, flexDirection:"row", alignItems:"center"}}>
                                    <CheckBox animationDuration={0} onTintColor={"#ffffff"} onCheckColor={"#ffffff"}onFillColor={"#d64409"}boxType={"square"}
                                        value={this.state.checkBox[0]}
                                        onValueChange={()=>this.checkBox(0)}
                                    />
                                    <StyledText style={{marginLeft:10, color:"#000000", fontSize:14}} >{"영리목적/홍보"}</StyledText>
                                </View>
                                <View style={{marginBottom:20, flexDirection:"row", alignItems:"center"}}>
                                    <CheckBox animationDuration={0} onTintColor={"#ffffff"} onCheckColor={"#ffffff"}onFillColor={"#d64409"}boxType={"square"}
                                    value={this.state.checkBox[1]}
                                    onValueChange={()=>this.checkBox(1)}
                                    />   
                                    <StyledText style={{marginLeft:10, color:"#000000", fontSize:14}}>{"음란/선정성"}</StyledText>
                                </View>
                                <View style={{marginBottom:20, flexDirection:"row", alignItems:"center"}}>
                                    <CheckBox animationDuration={0} onTintColor={"#ffffff"} onCheckColor={"#ffffff"}onFillColor={"#d64409"}boxType={"square"}
                                    value={this.state.checkBox[2]}
                                    onValueChange={()=>this.checkBox(2)}
                                    />
                                    <StyledText style={{marginLeft:10, color:"#000000", fontSize:14}}>{"부적절"}</StyledText>
                                </View>
                             </View>   
                            <View style={{marginLeft:20, marginRight:20}} >
                                <StyledText style={{fontSize:14,color:"#d64409"}}>{"누적 신고 시 도슨투고에서 확인할 때까지\n이 여행은 블라인드 처리됩니다."}</StyledText>
                            </View>
                        </View>
                        <View style={{flex:1, flexDirection:"row", borderTopColor:"#9e9e9e", borderTopWidth:1}}>
                            <View style={{flex:1, borderRightWidth:1, borderRightColor:"#9e9e9e"}}>
                                <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                                    onPress={this.props.close}
                                >
                                    <StyledText style={{fontSize:16, fontFamily:config.defaultBoldFontFamily, color:"#9e9e9e"}}>{"닫기"}</StyledText>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex:1}}>
                                <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                                    onPress={this.report}
                                >
                                    <StyledText style={{fontSize:16, fontFamily:config.defaultBoldFontFamily, color:"#d64409"}}>{"신고"}</StyledText>
                                </TouchableOpacity>
                            </View>
                            
                        </View>
                    </View> :
                    <View style={styles.container}>
                        <View style={{flex:3, justifyContent:"center", alignItems:"center"}}>
                            <StyledText style={{fontSize:20, color:"#d64409", fontFamily:config.defaultBoldFontFamily}}>{"*정상적으로 신고 처리되었습니다*"}</StyledText>
                        </View>
                        <View style={{flex:1, flexDirection:"row", borderTopColor:"#9e9e9e", borderTopWidth:1}}>
                            <View style={{flex:1}}>
                                <TouchableOpacity style={{flex:1, justifyContent:"center", alignItems:"center"}}
                                    onPress={this.props.close}
                                >
                                    <StyledText style={{fontSize:20, fontFamily:config.defaultBoldFontFamily, color:"#9e9e9e"}}>{"닫기"}</StyledText>
                                </TouchableOpacity>
                            </View>
                        </View>
                     </View>   
            }
            
        </View>
        )
    }

     
}

const styles =StyleSheet.create({
    container : {
        height:config.deviceInfo.width - 40,
        width:config.deviceInfo.width - 60, borderRadius:20, backgroundColor:"white"
    }
})