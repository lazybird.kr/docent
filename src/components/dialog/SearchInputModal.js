import React, {Component}  from "react";
import {View, FlatList,Keyboard, StatusBar, Platform, Text, TouchableOpacity} from "react-native";
import config from 'src/config';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { SearchBar } from 'react-native-elements';
import {StyledText} from 'src/components/styledComponents';

export default class SearchInputModal extends Component {
    constructor() {
        super();
        this.state = {
            searchValue : '',
            searchData : [],
            keyboardHeight: 0,
        }
        this._isMounted = false;
    }

    searchFilterFunction = text => {
      console.log("text : ", text)
        if(!text){
          this.setState({searchValue : "", searchData : []});
          return;
        }
        if(this._isMounted)
          this.setState({ searchValue : text });
          const APP_KEY =config.kakaoRestAPI.appKey;
        const url =  config.kakaoRestAPI.searchURL +`?query=${text}`;

          fetch(url, {
            headers : {"Authorization" : "KakaoAK " +APP_KEY},
          })
          .then(res => res.json())
          .then(resp => {
            let newData = [];
            resp.documents.forEach(item => {
              newData.push(item)
            })
            if(this._isMounted) {
              this.setState({searchData : newData});
            }
            console.log(resp);
          })
      }

      searchHeader = () => {
        const { searchValue } = this.state;
        return (
          <SearchBar  
                  ref={search => this.search = search}
                  platform={Platform.OS === "ios" ? "ios" : "android"}
                  cancelButtonTitle="취소"
                  inputStyle={{height : 40, fontFamily : config.altFontFamily, fontSize:14}} 
                  containerStyle={{backgroundColor:"#FBFBFB", 
                    borderBottomColor: 'transparent', borderTopColor: 'transparent'}} 
                  placeholder="검색"
                  onChangeText={this.searchFilterFunction}
                  onClearText={this.handleClearText}  
                  autoCorrect={false}
                  value={searchValue}
                />
        );
      }

    
  
      
      handleClearText = () => {
        this.setState({
          searchValue : "", searchData :[],
        });
      }
  
     
      
      componentDidMount() {
        console.log("SearchInputModal :")
        this._isMounted = true;
        this.subs = [
          Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this)),
          Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this)),
        ];
      }

      componentWillUnmount() {
        this.subs.forEach((sub) => {
          sub.remove();
        });
        this._isMounted = false;
      }
    
      _keyboardDidShow(e) {
        if(this._isMounted)
          this.setState({keyboardHeight: e.endCoordinates.height});
      }
    
      _keyboardDidHide(e) {
        if(this._isMounted) 
          this.setState({keyboardHeight: 0});
      }
      
      renderSeparator = () => {
        return (
          <View
            style={{
              height: 1,
              width: '86%',
              backgroundColor: '#CED0CE',
              //marginLeft: '14%',
            }}
          />
        );
      }

      render() {
          return(
          
              <View style={{ width:"100%", 
                            height : "95%"
                            
                            /*this.state.keyboardHeight? 
                                config.deviceInfo.height - this.state.keyboardHeight - (Platform.OS === 'ios' ? 20 : StatusBar.currentHeight)
          : config.deviceInfo.height * 0.7, */}}>
                  <View style={{
                      
                      alignItems: 'center',
                      height: 50,
                      borderBottomWidth : 1,
                      borderColor : "grey",
                      justifyContent : "center",
                      flexDirection : "row",
                      paddingLeft : 10,
                      paddingRight : 10,
                      backgroundColor : "#FFFFFF"
                    }}>
                      <View
                          style={{flex : 1}}
                      />
                      <View style={{flex : 8, justifyContent:"center", alignItems : "center"}}>
                        <StyledText style={{fontSize: 14}}>위치</StyledText>
                      </View>
                      <Icon
                        name="close"
                        size={20}
                        color='#525966'
                        style={{ flex : 1, marginLeft: 10 }}
                        onPress={()=>{this.props.toggleSearch()}}
                      />
                    </View>
                    <FlatList 
                      style={{backgroundColor:"#FFFFFF"}}
                      data={this.state.searchData}
                      renderItem={({ item, index }) => (
                        <View style={{height:40, margin:5, marginLeft:10}} key={index}>
                          <TouchableOpacity onPress={()=>{this.props.handleSelectPlace(item)}}>
                            <StyledText style={{fontSize:14, color:"black"}}>{item.place_name}</StyledText>
                            <StyledText style={{fontSize:12, color:"grey"}}>{item.address_name}</StyledText>
                          </TouchableOpacity>
                          
                        </View>
                        )
                      }
                      keyExtractor={(item, index) => "key" + index}
                      ItemSeparatorComponent={this.renderSeparator}
                      ListHeaderComponent={this.searchHeader}
                      keyboardShouldPersistTaps="always"
                      stickyHeaderIndices={[0]}
                      //contentContainerStyle={{ paddingBottom: 50}}
                    />
                </View>
          )
      }

     
}