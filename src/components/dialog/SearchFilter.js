import React, {Component}  from "react";
import {View, StyleSheet,Keyboard, ScrollView, Platform, Text, Image, TouchableOpacity} from "react-native";
import config from 'src/config';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {StyledText} from 'src/components/styledComponents';
import AsyncStorage from "@react-native-community/async-storage";
import { abs } from "react-native-reanimated";

export default class SearchFilter extends Component {
    constructor() {
        super();
        this.state = {
            categoryList : [
                {key:"자연", checked: false}, {key:"문화",     checked: false}, {key:"관광", checked: false}, {key:"동네", checked : false},
                {key:"공원", checked: false}, {key:"문화재",   checked: false}, {key:"액티비티", checked: false}, {key:"지역상점", checked : false},
                {key:"맛집", checked: false}, {key:"카페",     checked: false}, {key:"공방", checked: false}, {key:"랜선투어", checked : false},
                {key:"야경", checked: false}, {key:"비오는날", checked: false}, {key:"데이트", checked: false}, {key:"키즈", checked : false}
              ],
              //price : "무료",
              // season : "", //"사계절",
              season: [],
              // time : "", //"하루",
              time : [], //"하루",
              level : "", //"낮음",
              guide : "", //"음성변환",
              cardWidth : config.deviceInfo.width / 4 - (10*3.5),
        }
        this._isMounted = false;
    }

    
      componentDidMount() {
        const {categoryList} = this.state;
        console.log("SearchInputModal :")
        this._isMounted = true;
        this.subs = [
          Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this)),
          Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this)),
        ];
        AsyncStorage.getItem("localCategory").then((data)=>{
          console.log("data : ", JSON.parse(data))
          const localCategory = JSON.parse(data);
          let _categoryList = [...categoryList];
          let timeIndex = -1;
          let seasonIndex = -1;
          let guideIndex = -1;
          let levelIndex = -1;
          // const time = ["하루", "아침", "오전", "오후", "저녁", "밤"];
          const time = ["하루", "아침", "낮", "저녁", "밤"];
          const season = ["사계절", "봄", "여름", "가을", "겨울"];
          const guide = ["음성변환", "오디오파일"];
          const level = ["낮음", "중간", "높음"];
          let seasonList = [];
          let timeList = [];
          localCategory.forEach(item=>{
            let i = _categoryList.findIndex(categoryItem => item === categoryItem.key);
            if(i>=0){
              _categoryList[i].checked = true;
            }
            i = time.indexOf(item);
            // timeIndex = i;
            if(i >= 0) timeList.push(item);
            i = season.indexOf(item);
            if(i >= 0) seasonList.push(item);
            // seasonIndex = i;
            i = guide.indexOf(item);
            guideIndex = i;
            i = level.indexOf(item);
            levelIndex = i;
          })
          console.log('timeList:', timeList)
          this.setState({categoryList:_categoryList,
            // time : timeIndex === -1? time[timeIndex] : "",
            // season : seasonIndex === -1? season[seasonIndex] : "",
            time: timeList,
            season : seasonList,
            guide : guideIndex === -1? guide[guideIndex] : "",
            level : levelIndex === -1? level[levelIndex] : ""
          })
        })
      }

      componentWillUnmount() {
        this.subs.forEach((sub) => {
          sub.remove();
        });
        this._isMounted = false;
      }
    
      _keyboardDidShow(e) {
        if(this._isMounted)
          this.setState({keyboardHeight: e.endCoordinates.height});
      }
    
      _keyboardDidHide(e) {
        if(this._isMounted) 
          this.setState({keyboardHeight: 0});
      }

      selectGuide = (item) => {
        const {guide} = this.state;
        console.log("item : ", item)
        if(guide === item){
          this.setState({guide: ""});
          return;
        }
        this.setState({guide : item})
      }

      renderGuide = () => {
        const guide = ["음성변환", "오디오파일", "", ""];
        return (
          <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10, justifyContent:"space-between"}}>
            {guide.map((item, index) =>{
              return (
                <View key={index} >
                {item?
                  <TouchableOpacity style={{...styles.categoryItem, 
                    ...{backgroundColor : item === this.state.guide ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                    onPress={()=>{this.selectGuide(item)}}>
                    <StyledText style={{color: item === this.state.guide ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}>
                      {item}
                    </StyledText>
                  </TouchableOpacity>
                  :
                  <View style={{height : 24,
                    width:this.state.cardWidth }} 
                    >
                  </View>
                }
                </View>
              )
            })}
            </View>
        )
      }

      checkCategory = (index) => {
        let count = 0;
        let list = [...this.state.categoryList];
        list.map((item, i) => {
          if(item.checked && i !== index ){
            count++;
          }
        })
        if(count === 3){
          return;
        }
        list[index].checked =  !list[index].checked;
        this.setState({categoryList : list})
      }

      selectSeason = (item) => {
        const {season} = this.state;
        let newSeason = season;
        let index = season.indexOf(item);
        if(index >= 0) {
          newSeason = season.filter((ele) => ele !== item)
        } else {
          if(item === '사계절') {
            newSeason = [item]
          } else {
            if(season.indexOf('사계절') >= 0) {
              newSeason = [];
            }
            newSeason.push(item);
          }
        }
        this.setState({
          season: newSeason,
        })
        // if(season === item){
        //   this.setState({season : ""})
        //   return;
        // }
        // this.setState({season : item})
      }
      selectTime = (item) => {
        const {time} = this.state;
        let newTime = time;
        let index = time.indexOf(item);
        if(index >= 0) {
          newTime = time.filter((ele) => ele !== item)
        } else {
          if(item === '하루') {
            newTime = [item]
          } else {
            if(time.indexOf('하루') >= 0) {
              newTime = [];
            }
            newTime.push(item);
          }
        }
        this.setState({
          time: newTime,
        })
        // if(time === item){
        //   this.setState({time : ""})
        //   return;
        // }
        // this.setState({time : item})
      }
      selectLevel = (item) => {
        const {level} = this.state;
        if(level === item){
          this.setState({level : ""})
          return;
        }
        this.setState({level : item})
      }
      selectPrice = (item) => {
        const {price} = this.state;
        if(price === item){
          this.setState({price : ""})
          return;
        }
        this.setState({price : item})
      }

      renderSeason = () => {
        const season = ["사계절", "","","", "봄", "여름", "가을", "겨울"];
        return (
          <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10, justifyContent:"space-between"}}>
            {season.map((item, index) =>{
              return (
                <View key={index} >
                {item?
                  <TouchableOpacity style={{...styles.categoryItem, 
                    // ...{backgroundColor : item === this.state.season ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                    ...{backgroundColor : this.state.season.indexOf(item) >= 0 ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                    onPress={()=>{this.selectSeason(item)}}>
                    {/* <StyledText style={{color: item === this.state.season ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}> */}
                    <StyledText style={{color: this.state.season.indexOf(item) >= 0 ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}>
                      {item}
                    </StyledText>
                  </TouchableOpacity>
                  :
                  <View style={{height : 24,
                    backgroundColor: "white", 
                    width:this.state.cardWidth }} 
                    >
                  </View>
                }
                </View>
              )
            })}
            </View>
        )
      }

      renderCategory = () => {
        return (
            <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10, justifyContent:"space-between"}}>
            {this.state.categoryList.map((item, index) =>{
              return (
                <TouchableOpacity key={index} style={{...styles.categoryItem, ...{backgroundColor : item.checked ? "#266753" : "#ffffff", width:this.state.cardWidth }}} onPress={()=>{this.checkCategory(index)}}>
                  <StyledText style={{color: item.checked ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}>{item.key}</StyledText>
                </TouchableOpacity>
              )
            })}
            </View>
        )
      }
      renderTime = () => {
        // const time = ["하루", "아침", "오전", "", "오후", "저녁", "밤",""];
        const time = ["하루", "", "", "", "아침", "낮", "저녁", "밤"];
        console.log('renderTime:', this.state.time)
        return (
          <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10, justifyContent:"space-between"}}>
            {time.map((item, index) =>{
              return (
                <View key={index} >
                {item?
                  <TouchableOpacity style={{...styles.categoryItem, 
                    // ...{backgroundColor : item === this.state.time ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                    ...{backgroundColor : this.state.time.indexOf(item) >= 0 ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                    onPress={()=>{this.selectTime(item)}}>
                    <StyledText style={{color: this.state.time.indexOf(item) >= 0 ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}>
                      {item}
                    </StyledText>
                  </TouchableOpacity>
                  :
                  <View style={{height : 24,
                    backgroundColor: "white", 
                    width:this.state.cardWidth }} 
                    >
                  </View>
                }
                </View>
              )
            })}
            </View>
        )
      }
    
      renderLevel = () => {
        const level = ["낮음", "중간", "높음", ""];
        return (
          <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10, justifyContent:"space-between"}}>
            {level.map((item, index) =>{
              return (
                <View key={index} >
                {item?
                  <TouchableOpacity style={{...styles.categoryItem, 
                    ...{backgroundColor : item === this.state.level ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                    onPress={()=>{this.selectLevel(item)}}>
                    <StyledText style={{color: item === this.state.level ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}>
                      {item}
                    </StyledText>
                  </TouchableOpacity>
                  :
                  <View style={{height : 24,
                    backgroundColor: "white", 
                    width:this.state.cardWidth }} 
                    >
                  </View>
                }
                </View>
              )
            })}
            </View>
        )
      }
      renderPrice = () => {
        
        const price = ["무료", "10도넛", "30도넛", "50도넛"];
        return (
          <View style={{flex:1, flexDirection:"row", flexWrap:"wrap", marginBottom:10, justifyContent:"space-between"}}>
            {price.map((item, index) =>{
              return (
                <View key={index} >
                {item?
                  <TouchableOpacity style={{...styles.categoryItem, 
                    ...{backgroundColor : item === this.state.price ? "#266753" : "#ffffff", width:this.state.cardWidth }}} 
                    onPress={()=>{this.selectPrice(item)}}>
                    <StyledText style={{color: item === this.state.price ? "#ffffff" : "#707070", textAlign:"center", fontSize: 12,}}>
                      {item}
                    </StyledText>
                  </TouchableOpacity>
                  :
                  <View style={{height : 24,
                    backgroundColor: "white", 
                    width:this.state.cardWidth }} 
                    >
                  </View>
                }
                </View>
              )
            })}
            </View>
        )
      }

      confirm = ()=>{
          let filters = [];
          const {guide, season, time, level, /*price*/} = this.state
          this.state.categoryList.map(item => {
              if(item.checked){
                filters.push(item.key);
              }
          })
          guide && filters.push(guide);
          // season && filters.push(season);
          filters = filters.concat(season);
          // time && filters.push(time);
          filters = filters.concat(time);
          level && filters.push('난이도 ' + level);
          //price && filters.push(price);
          this.props.confirm([
            ...filters
          ])
          

      }
      
      onDeselect = () => {
        let initCategoryList = this.state.categoryList.map((item) => {
          return {
            key: item.key,
            checked: false,
          }
        });

        this.setState({
          categoryList: initCategoryList,
          season: [],
          time: [],
          level: "",
          guide: "",
        })
      }
      
      render() {
          return(
            <View style={styles.container}>
              <View style={styles.top}>
                <TouchableOpacity style={styles.deselect} onPress={this.onDeselect}>
                  <Image source={config.images.deselect} style={{width: 16, height: 14, marginRight: 3}}></Image>
                  <StyledText style={{color: "#26675e", fontSize: 14,}}>선택해제</StyledText>  
                </TouchableOpacity>
              </View>
              <View style={{flex: 8}}>
                <ScrollView style={{ flex: 1,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 10,
                            // margin:10,
                            /*this.state.keyboardHeight? 
                                config.deviceInfo.height - this.state.keyboardHeight - (Platform.OS === 'ios' ? 20 : StatusBar.currentHeight)
          : config.deviceInfo.height * 0.7, */}}>
                    <View style={styles.item2, {alignItems: "flex-start"}}>
                        <View style={{flex:1, alignItems:"flex-end", margin: 5, flexDirection:"row"}}>
                            <StyledText style={{color: "#26675e", fontSize: 14,}}>가이드방식</StyledText>  
                        </View>
                        <View style={{flex:1, marginLeft:5, marginRight:5, flexDirection:"row"}}>
                            {this.renderGuide()}
                        </View>
                    </View>
                    <View style={styles.item4}>
                        <View style={{flex:1, alignItems:"flex-end", margin: 5, flexDirection:"row"}}>
                            <StyledText style={{color: "#26675e", fontSize: 14,}}>여행키워드</StyledText>  
                        </View>
                        <View style={{flex:4, marginLeft:5, marginRight : 5, }}>
                            {this.renderCategory()}
                        </View>
                    </View>
                    <View style={styles.item2}>
                        <View style={{flex:1, alignItems:"flex-end", margin: 5, flexDirection:"row"}}>
                            <StyledText style={{color: "#26675e", fontSize: 14,}}>계절</StyledText>  
                        </View>
                        <View style={{flex:2, marginLeft:5, marginRight:5}}>
                            {this.renderSeason()}
                        </View>
                    </View>
                    <View style={styles.item2}>
                        <View style={{flex:1, alignItems:"flex-end", margin: 5, flexDirection:"row"}}>
                            <StyledText style={{color: "#26675e", fontSize: 14,}}>시간</StyledText>  
                        </View>
                        <View style={{flex:2, marginLeft:5, marginRight:5}}>
                            {this.renderTime()}
                        </View>
                    </View>
                    <View style={styles.item2}>
                        <View style={{flex:1, alignItems:"flex-end", margin: 5, flexDirection:"row"}}>
                            <StyledText style={{color: "#26675e", fontSize: 14,}}>난이도</StyledText>  
                        </View>
                        <View style={{flex:1, marginLeft:5, marginRight:5}}>
                        {this.renderLevel()}
                        </View>
                    </View>
                    {/*<View style={styles.item3}>
                        <View style={{flex:1, alignItems:"flex-end", margin: 5, flexDirection:"row"}}>
                            <StyledText style={{color: "#26675e", fontSize: 14,}}>가격</StyledText>  
                        </View>
                        <View style={{flex:1, marginLeft:10, marginRight:10}}>
                        {this.renderPrice()}
                        </View>
                    </View>*/}
                    {/* <View style={styles.bottom}>
                        <TouchableOpacity style={{width:"50%", backgroundColor:"#ffffff", borderColor:"#d64409", borderWidth:1, borderRadius:5, justifyContent:"center", alignItems:"center"}}
                            onPress={()=>this.props.cancel()}
                        >
                            <StyledText style={{color:"#d64409", fontSize:12}}>취소하기</StyledText>
                        </TouchableOpacity>
                        <TouchableOpacity style={{width:"50%", backgroundColor:"#d64409", borderRadius:5, justifyContent:"center", alignItems:"center"}}
                            onPress={()=>{this.confirm()}}
                        >
                            <StyledText style={{color:"white", fontSize:12}}>적용하기</StyledText>
                        </TouchableOpacity>
                    </View> */}
                    {/* <View style={{height:30}}>

                    </View> */}
                  </ScrollView>
                </View>
                <View style={styles.bottom}>
                  <TouchableOpacity style={styles.cancel}
                      onPress={()=>this.props.cancel()}
                  >
                    <StyledText style={styles.label1}>취소하기</StyledText>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.confirm}
                      onPress={()=>{this.confirm()}}
                  >
                    <StyledText style={styles.label2}>확인하기</StyledText>
                  </TouchableOpacity>
                </View>
            </View>
          )
      }

     
}

const styles =StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 5,
  },
  top: {
    height: 48,
  },
  deselect: {
    flex: 1, 
    flexDirection: "row", 
    alignItems:"center", 
    justifyContent: "flex-end", 
    marginTop: 30,
    marginRight: 20,
  },
    item2 : {
      height : 90,
      width : "100%",
    },
    categoryItem : {
        height : 24,
        width : config.deviceInfo.width / 5,
        backgroundColor: "white",
        borderWidth : 1,
        borderRadius: 5,
        borderColor : "#707070",
        marginRight : 5,
        marginBottom : 5,
        justifyContent : "center"
    },
    item4 : {
        height : 160,
        width : "100%",
    },
    bottom:{
      flex: 1,
      // position: "absolute",
      // bottom: 0,
      // marginTop:10,
      //   // height: 30,
      // height: 65,
      width:"100%",
      flexDirection:"row",
      justifyContent:"space-around",

      borderTopColor: "#bfbfbf",
      borderTopWidth: 1,
    },
    cancel: {
      flex: 1,
      justifyContent:"center", 
      alignItems:"center",
      borderRightColor: '#bfbfbf',
      borderRightWidth: 1,
    },
    confirm: {
      flex: 1,
      justifyContent:"center", 
      alignItems:"center",
    },
    label1: {
      fontSize:16, 
      fontFamily:config.defaultBoldFontFamily, 
      color:"#bfbfbf"
    },
    label2: {
      fontSize:16, 
      fontFamily:config.defaultBoldFontFamily, 
      color:"#d64409"
    },
})