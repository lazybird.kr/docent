import React, {Component}  from "react";
import {View, FlatList,Keyboard, StatusBar, Platform, Text, TouchableOpacity} from "react-native";
import config from 'src/config';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { WebView } from 'react-native-webview';
import NaverMapView, {Circle, Marker, Path, Polyline, Polygon} from "react-native-nmap";
import {StyledText} from 'src/components/styledComponents';

export default class Map extends Component {
    constructor() {
        super();
        this._isMounted = false;
        this.state ={
          place : null,
        }
    }
     
      
      componentDidMount() {
        console.log("Maps :", this.props.place)
        this._isMounted = true;
        this.setState({place : this.props.place})
      }

      componentWillUnmount() {
        this._isMounted = false;
      }
    
      render() {
        const {place} = this.state;
        const tmpHtml = place ? `
          <!DOCTYPE html>
          <html>
          <body>
            <div id="map" style="width:1000px;height:1000px;"></div>
            <script type="text/javascript" src="https://dapi.kakao.com/v2/maps/sdk.js?appkey=65c756d7bf6cae5b922a846d2d7d5503"></script>
            <script>
              var container = document.getElementById('map');
              var options = {
                center: new kakao.maps.LatLng(${place.y}, ${place.x}),
                level: 3
              };

              var map = new kakao.maps.Map(container, options);
            </script>
          </body>
          </html>
          ` : `<h1>test </h1>`;
          
          return(
          
              <View style={{flex:1}}>
                
                  <View style={{
                      alignItems: 'center',
                      height:300,
                      borderBottomWidth : 1,
                      borderColor : "grey",
                      justifyContent : "center",
                      flexDirection : "row",
                      paddingLeft : 10,
                      paddingRight : 10,
                      backgroundColor : "green"
                    }}>
                      <View
                          style={{flex : 1}}
                      />
                      <View style={{flex : 8, justifyContent:"center", alignItems : "center"}}>
                        <StyledText style={{fontSize: 14}}>지도</StyledText>
                      </View>
                      <Icon
                        name="close"
                        size={20}
                        color='#525966'
                        style={{ flex : 1, marginLeft: 10 }}
                        onPress={()=>{this.props.toggleMap()}}
                      />
                  </View>
                    
                    
                      {place &&
                      <View style={{height:400,  paddingLeft : 10,
                        paddingRight : 10,backgroundColor:"blue"}} >
                        <NaverMapView style={{width: '100%', height: '100%'}}
                         //showsMyLocationButton={true}
                         center={{latitude: parseFloat(place.y),
                          longitude: parseFloat(place.x), zoom: this.state.zoom}}
                         onTouch={e => console.log('onTouch', JSON.stringify(e.nativeEvent))}
                         onCameraChange={e => console.log('onCameraChange', JSON.stringify(e))}
                         onMapClick={e => console.log('onMapClick', JSON.stringify(e))}>
                          
                   
                         <Marker coordinate={{latitude: parseFloat(place.y),
                          longitude: parseFloat(place.x)}} onClick={() => console.warn('onClick! p0')}/>
                        
        
                        </NaverMapView>
                        </View>
                        }
                    
                </View>
          )
      }

     
}