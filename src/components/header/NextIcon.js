import React, {Component}  from "react";
import { NavigationActions, StackActions } from 'react-navigation';
import { TouchableOpacity, Text } from "react-native";
import {StyledText} from 'src/components/styledComponents';

export default class NextIcon extends Component {
  render() {
    const {title, navigation, clickNext} = this.props;
    return (
      <TouchableOpacity style={{marginRight : 10}} onPress={clickNext} >
        <StyledText style={{color:"#d64409", fontSize : 14}}>{title}</StyledText>
      </TouchableOpacity>
    );
  }
}
