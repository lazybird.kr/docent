import React, {Component}  from "react";
import {Image, View, StyleSheet, Text} from 'react-native';
import {StyledText} from 'src/components/styledComponents';
import config from 'src/config';

export default class Headertitle extends Component {
  render() {
    const {image, title, style} = this.props;
    return (
      <View style={styles.container}>
        {/*<Image source={image} style={{width : 36, height : 36}} />*/}
        <StyledText style={[styles.titleText, style]}>{title}</StyledText>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container : {
    alignItems: "center",
    justifyContent:"center",
    flexDirection : "row",
    //backgroundColor : "green",
    flex : 1,
  },
  titleText : {
    fontFamily: config.defaultBoldFontFamily,
    fontSize : 16,
    color : 'black'
  }
});