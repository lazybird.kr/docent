import React, {Component}  from "react";
import {View, TouchableOpacity, Text, Switch, StyleSheet, Image} from "react-native";
import config from 'src/config';
// import ImagePicker from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import {StyledText, StyledTextInput} from 'src/components/styledComponents';
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class MakeSubject extends Component {

  addPicture = () => {
    // const options = {
    //   quality: 1.0,
    //   maxWidth: 500,
    //   maxHeight: 500,
    //   storageOptions: {
    //     skipBackup: true,
    //   },
    // };
    // const options = {
    //   width: 500,
    //   height: 500,
    //   multiple: true,
    // };

    ImagePicker.openPicker({
      compressImageMaxWidth: 500,
      compressImageMaxHeight: 500,
      multiple: true
    }).then(images => {
      console.log(images);
      this.props.selectPicture(images);

    });

    // ImagePicker.launchImageLibrary(options, (response) => {
    
    //   if (response.didCancel) {
    //     console.log('User cancelled image picker');
    //   } else if (response.error) {
    //     console.log('ImagePicker Error: ', response.error);
    //   } else if (response.customButton) {
    //     console.log('User tapped custom button: ', response.customButton);
    //   } else {
        
    
    //     // You can also display the image using data:
    //     // const source = { uri: 'data:image/jpeg;base64,' + response.data };
    
    //     this.props.selectPicture(response);
    //   }
    // });
  }

  delPicture = (index) => {
    
    this.props.removePicture(index);
  }
  
  
  render() {
    const {toggleSubject, changeSubject, locationFlag, setLocationFlag,
        changeDesc, subject, description, picture } = this.props;
      /*
    let fileName = '';
    if(picture && picture.uri){
      const picArr = picture.uri.split('/');
      const fileOrgName = picArr[picArr.length-1];
      if(fileOrgName.length > 20) {
        const postfix = fileOrgName.split('.')[1];
        fileName = fileOrgName.substr(0,20) + "..."+ postfix;
      } else {
        fileName = fileOrgName;
      }
    }
    */
    
    return (
      <View style={styles.container}>
        <View style={styles.subject}>
          <StyledText style={{flex:1.5, color: "#26675e", fontSize: 14}}>제목</StyledText>  
          <StyledTextInput style={{paddingVertical : 0, fontSize: 14, flex:7.5, color:"grey"}}
            autoCorrect={false} placeholder="제목 입력" 
            placeholderTextColor="grey" onChangeText={changeSubject}>{subject}
            </StyledTextInput>
          <TouchableOpacity style={{flex:1, alignItems:"center"}} onPress={toggleSubject}>
            {/*<Image source={config.images.subjectDrawer} style={{height: 25, width :25 * 88/64, resizeMode:"contain" }}/>*/}
            <Icon name={"chevron-up"} size={24} color={"#707070"} />
          </TouchableOpacity>
        </View>
        <View style={styles.description}>
          
            {/*<StyledText style={{flex:2, color: "#ffb468", fontSize: 14,}}>설명</StyledText>  */}
            <View style={{height:20,justifyContent:"center"}}>
              <StyledText style={{ color: "#26675e", fontSize: 14,}}>설명</StyledText>  
            </View>
 
            <StyledTextInput style={{textAlignVertical: "top", padding: 5, fontSize: 14, flex:8, color:"grey"}}
              autoCorrect={false} placeholder="당신의 여행을 입력해 주세요." 
              placeholderTextColor="grey" multiline onChangeText={changeDesc}>{description}
            </StyledTextInput>
          
        </View>
        <View style={styles.defineLocation}>
          <View style={{height:40, width:"80%",justifyContent:"center"}}>
            <StyledText style={{color: "#26675e", fontSize: 14,}}>위치기반</StyledText>
            <StyledText style={{color:"#bfbfbf", fontSize:10}}>{"* '위치기반'을 선택하시면, 포인트를 입력할 때 장소 검색을 합니다."}</StyledText>
          </View>
          <View style={{height:40, width:"20%",justifyContent:"center", alignItems:"flex-end"}}>
            <Switch
              trackColor={{false : "#bfbfbf", true : "#26675e"}}
              thumbColor={locationFlag? "#ffffff" : "#ffffff"}
              ios_backgroundColor="#ffffff"
              onValueChange={setLocationFlag}
              value={locationFlag}
            />
          </View>
        </View>
        <View style={styles.addPicture}>
          <View style={{height:40, justifyContent:"center", alignItems:"center", flexDirection:"row"}}>
            <StyledText style={{flex:1, color: "#26675e", fontSize: 14,}}>사진첨부</StyledText>  
            <View style={{flex:3, height:40, flexDirection:"row", justifyContent:"center", alignItems:"center"}}>
              <TouchableOpacity style={{width:40, height : 40, backgroundColor:"#26675e",
                borderRadius: 5, justifyContent:"center", alignItems:"center",
                }} onPress={()=>{this.addPicture()}}>
                  <Entypo name={"plus"} size={32} color={"#ffffff"}/>
              </TouchableOpacity>
              {picture && picture.map((pic,i) =>{
                return (
                <TouchableOpacity key={"pic" + i.toString()} style={{marginLeft:10, width:40, height:40}}
                onPress={()=>this.delPicture(i)}
              >
                {/* <Image source={{uri : pic.uri}} style={{borderRadius:5, width:40, height:40, resizeMode:"stretch"}}></Image> */}
                <Image source={{uri : pic.path}} style={{borderRadius:5, width:40, height:40, resizeMode:"stretch"}}></Image>
                <Icon style={{position:"absolute", zIndex:1, right:1,top:1}} name={"close"} size={20} color={"#ffffff"} />
              </TouchableOpacity>

                )
              })
              
                
              }
              <View style={{flex:1, alignItems:"center"}} >
              </View>
            </View>
          </View>
        </View>
        
            
      </View>
    );
  }
}

const styles =StyleSheet.create({
  container : {
    flex: 1,
    paddingLeft : 10,
    paddingRight : 10,
    backgroundColor:"#f2fcfb",
    borderRadius:5,
  },
  subject : {
    flex:1.5,
    flexDirection:"row",
    justifyContent : "center",
    alignItems : "center",
    borderBottomColor : "grey",
    borderBottomWidth : 1,
    marginTop : 10,
  },
  description : {
    flex:6,
    paddingTop :10,
    paddingBottom :10,
    borderBottomColor : "grey",
    borderBottomWidth : 1,
  },
  addPicture : {
    flex:1,
    paddingTop :10,
    paddingBottom :10,
  },
  defineLocation : {
    flex:1,
    paddingTop :10,
    paddingBottom :10,
    borderBottomColor : "grey",
    borderBottomWidth : 1,
    flexDirection:"row",
  }
});
