import MakeSubject from './MakeSubject';
import MakeLocation from './MakeLocation';

export {
 MakeSubject,
 MakeLocation,
}