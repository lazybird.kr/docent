import React, {Component}  from "react";
import {View, Text, TouchableOpacity, TextInput, StyleSheet, Image} from "react-native";
import config from 'src/config';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Modal from 'react-native-modal';
import {SearchInputModal} from 'src/components/dialog'
import {StyledText, StyledTextInput} from 'src/components/styledComponents';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
// import ImagePicker from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';

import Entypo from 'react-native-vector-icons/Entypo';
export default class MakeLocation extends Component {
  
  state = {
    isModalVisible : false
  }

  toggleSearch = () => {
    this.setState({isModalVisible : !this.state.isModalVisible});
  }

  handleSelectPlace = (location) => {
    console.log("selected Location : ", location);
    this.props.changeLocation(this.props.index, location);
    //this.toggleSearch();
  }

  searchLocation = () => {
    //this.setState({isModalVisible : true});
    this.props.navigation.navigate("searchPointLocation" , {
      handleSelectPlace : this.handleSelectPlace
    })
  }

  addPicture = () => {
    ImagePicker.openPicker({
      compressImageMaxWidth: 500,
      compressImageMaxHeight: 500,
      multiple: true
    }).then(images => {
      console.log(images);
      this.props.changeLocPicture(this.props.index, images);

    });

    // const options = {
    //   quality: 1.0,
    //   maxWidth: 500,
    //   maxHeight: 500,
    //   storageOptions: {
    //     skipBackup: true,
    //   },
    // };

    // ImagePicker.launchImageLibrary(options, (response) => {
    
    //   if (response.didCancel) {
    //     console.log('User cancelled image picker');
    //   } else if (response.error) {
    //     console.log('ImagePicker Error: ', response.error);
    //   } else if (response.customButton) {
    //     console.log('User tapped custom button: ', response.customButton);
    //   } else {
        
    
    //     // You can also display the image using data:
    //     // const source = { uri: 'data:image/jpeg;base64,' + response.data };
    
    //     this.props.changeLocPicture(this.props.index, response);
    //   }
    // });
  }

  delPicture = (index) => {
    this.props.removeLocPicture(this.props.index, index);
  }
  
  render() {
    const {handleClick, index, locationFlag,
        changeLocation, changeLocDesc, changeLocTip, locationInfo, removeLocation,
    } = this.props;

    const {isModalVisible }= this.state;
    console.log("isModalVisible : ", isModalVisible);

    /*
    let fileName = '';
    if(locationInfo.picture && locationInfo.picture.uri){
      const picArr = locationInfo.picture.uri.split('/');
      const fileOrgName = picArr[picArr.length-1];
      if(fileOrgName.length > 20) {
        const postfix = fileOrgName.split('.')[1];
        fileName = fileOrgName.substr(0,20) + "..."+ postfix;
      } else {
        fileName = fileOrgName;
      }
    }
    */
    return (
      <View style={styles.container}>
        <Modal isVisible={isModalVisible}
              onBackdropPress={this.toggleSearch}
              avoidKeyboard={true}
              style={styles.modalStyle} >
          <SearchInputModal
            toggleSearch={this.toggleSearch}
            handleSelectPlace={this.handleSelectPlace} />
        </Modal>
       {/* <KeyboardAwareScrollView contentContainerStyle={{flexGrow: 1}}
            resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>*/}
        <View style={styles.subject}>
          <StyledText style={{flex:1.5, color: "#26675e", fontSize: 14}}>{index+1}</StyledText>  
          {/* <StyledText style={{flex:1.5, color: "#26675e", fontSize: 14}}>{index+1}</StyledText>   */}
          {!locationFlag ?
            <StyledTextInput style={{paddingVertical : 0, fontSize: 14, flex:6.5, color:"grey"}}
              autoCorrect={false} placeholder="포인트 입력" numberOfLines={1}
              placeholderTextColor="grey" onChangeText={(text) => changeLocation(index, text)}>
                {locationInfo.location && locationInfo.location.place_name}
            </StyledTextInput> :
            <TouchableOpacity style={{flex:6.5,}} onPress={this.searchLocation}>
              <StyledText numberOfLines={1} style={{paddingVertical : 0, fontSize: 14,  color:"grey"}}>
                {locationInfo && locationInfo.location && locationInfo.location.place_name ? locationInfo.location.place_name : "포인트입력"}
              </StyledText>
            </TouchableOpacity>
          }
          {locationFlag && 
            <TouchableOpacity style={{flex:1, alignItems:"center"}} onPress={this.searchLocation}>
              <Image source={config.images.searchIcon} style={{height: 20, width :20, }}/>
            </TouchableOpacity>
          }
          
          <TouchableOpacity style={{flex:1, alignItems:"center"}} onPress={handleClick}>
            {/*<Image source={config.images.subjectDrawer} style={{height: 25, width :25 * 88/64, resizeMode:"contain" }}/>*/}
            <Icon name={"chevron-up"} size={24} color={"#707070"} />
          </TouchableOpacity>
        </View>
        <View style={styles.description}>
            <StyledText style={{height:20, color: "#26675e", fontSize: 14,}}>설명</StyledText>  
            <StyledTextInput style={{textAlignVertical: "top",padding: 5, fontSize: 14, flex:8, color:"grey"}}
              autoCorrect={false} placeholder="당신의 여행을 입력해 주세요." 
              placeholderTextColor="grey" multiline onChangeText={(text) => changeLocDesc(index, text)}>{locationInfo.description}
            </StyledTextInput>
          
        </View>
        <View style={styles.tip}>
            <StyledText style={{height:20,color: "#26675e", fontSize: 14,}}>추가정보</StyledText>  
            <StyledTextInput style={{flex:1, textAlignVertical: "top", padding: 5,fontSize: 14, color:"grey",}}
              placeholder="이 장소의 여행팁을 입력해 주세요"
              placeholderTextColor="grey" multiline onChangeText={(text) => changeLocTip(index, text)}>{locationInfo.tip}</StyledTextInput>
        </View>
        <View style={styles.addPicture}>
          <View style={{flex: 1, justifyContent:"center", alignItems:"center", flexDirection:"row"}}>
            <StyledText style={{flex:1, color: "#26675e", fontSize: 14,}}>사진첨부</StyledText>  
            <View style={{flex:3, height:40, flexDirection:"row", justifyContent:"center", alignItems:"center"}}>
              <TouchableOpacity style={{width:40, height : 40, backgroundColor:"#26675e",
                borderRadius: 5, justifyContent:"center", alignItems:"center",
                }} onPress={this.addPicture}>
                  <Entypo name={"plus"} size={32} color={"#ffffff"}/>
              </TouchableOpacity>
              {locationInfo.picture && locationInfo.picture.map((pic,i)=>{
                return (
                <TouchableOpacity key={"pic" + i.toString()} style={{marginLeft:10, width:40, height:40}}
                  onPress={()=>this.delPicture(i)}
                >
                  {/* <Image source={{uri : pic.uri}} style={{borderRadius:5, width:40, height:40, resizeMode:"stretch"}}></Image> */}
                  <Image source={{uri : pic.path}} style={{borderRadius:5, width:40, height:40, resizeMode:"stretch"}}></Image>
                  <Icon style={{position:"absolute", zIndex:1, right:1,top:1}} name={"close"} size={20} color={"#ffffff"} />
                </TouchableOpacity>
                )
              })
                
              }
              <View style={{flex:1, alignItems:"center"}} > 
              </View>
            </View>
          </View>
        </View>
        {/* <View style={styles.delete}>
            <StyledText style={{color: "#707070", fontSize: 10,}} onPress={()=>{removeLocation(index)}}>삭제하기</StyledText>
        </View> */}
        {/*</KeyboardAwareScrollView>*/}
      </View>
    );
  }
}

const styles =StyleSheet.create({
  container : {
    flex: 1,
    paddingLeft : 10,
    paddingRight : 10,
    backgroundColor:"#f2fcfb",
    borderRadius:5,
  },
  subject : {
    flex:1.5,
    flexDirection:"row",
    justifyContent : "center",
    alignItems : "center",
    borderBottomColor : "grey",
    borderBottomWidth : 1,
    marginTop : 10,
    //paddingTop :5, 
    //paddingBottom :5, 
    
  },
  description : {
    flex:5,
    paddingTop :10, 
    paddingBottom :10, 
    borderBottomColor : "grey",
    borderBottomWidth : 1,
  },
  tip : {
    flex:3,
    paddingTop :10,
    borderBottomColor : "grey",
    borderBottomWidth : 1,
  },
  delete : {
    flex:1,
    justifyContent:"flex-end",
    paddingTop :10, 
    paddingBottom :10, 
  },
  modalStyle: { 
    flex: 1,
    justifyContent: "flex-end",
    margin: 0
  },
  addPicture : {
    flex:1,
    paddingTop :10, 
    paddingBottom :10,
    
    //backgroundColor : "blue",
  }

});
