import React, {Component} from 'react';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';

import { createBottomTabNavigator } from 'react-navigation-tabs';
import config from 'src/config';
import {View, Image, StatusBar} from 'react-native';
import {StyledText} from 'src/components/styledComponents';
import {MakeStack} from 'src/containers/Make';
import {ListenStack} from 'src/containers/Listen';
import {HomeStack} from 'src/containers/Home';
import {AboutMyselfStack} from 'src/containers/MyInfo';
import {StartScreen} from 'src/containers/Start';
import DeviceInfo from 'react-native-device-info';
import {isNetworkAvailable} from 'src/components/NetworkUtil';
import showToast from 'src/components/Toast';
import AsyncStorage from "@react-native-community/async-storage";
import axios from 'axios';
import SplashScreen from 'react-native-splash-screen'
import LottieView from 'lottie-react-native';
import NavigationService from './NavigationService';
import dynamicLinks from '@react-native-firebase/dynamic-links';

const tabBarOnPress = ({ navigation, route }) => {
  navigation.popToTop();
  navigation.navigate(navigation.state.routeName);
};

const Tabs = createBottomTabNavigator({
  communityTab:{
    screen : HomeStack,
    navigationOptions: {
      tabBarLabel: ()=>{},
      tabBarIcon: ({focused, tintColor}) => (
        <View>
          { !focused ? 
          <View style={{justifyContent:"center", alignItems:"center"}}>
            <Image style={{height:25, width:25, resizeMode:"contain"}} source={config.images.homeIcon} />
            {/*<StyledText style={{color:"#c1c1c1", fontSize:8}}>{"홈"}</StyledText>*/}
          </View>
          :
          <View style={{justifyContent:"center", alignItems:"center"}}>
            <Image style={{height:25, width:25, resizeMode:"contain"}} source={config.images.selectedHomeIcon} />
            {/*<StyledText style={{color:"#d64409", fontSize:8}}>{"홈"}</StyledText>*/}
          </View>
          }
        </View>
      ),
      barStyle: {backgroundColor: '#FFC0CB'},
      tabBarOnPress: tabBarOnPress
    },
    
  },
    listenTab: {
      screen : ListenStack,
      navigationOptions: {
        tabBarLabel: ()=>{},
        tabBarIcon: ({focused, tintColor}) => (
          <View>
            { !focused ?
            <View style={{justifyContent:"center", alignItems:"center"}}>
              <Image style={{height:28, width:28, resizeMode:"contain"}} source={config.images.listenIcon} />
              {/*<StyledText style={{color:"#c1c1c1", fontSize:8}}>{"듣기"}</StyledText>*/}
            </View>
            :
            <View style={{justifyContent:"center", alignItems:"center"}}>
              <Image style={{height:28, width:28, resizeMode:"contain"}} source={config.images.selectedListenIcon} />
              {/*<StyledText style={{color:"#d64409", fontSize:8}}>{"듣기"}</StyledText>*/}
            </View>
            } 
          </View>
        ),
        barStyle: {backgroundColor: '#FFC0CB'},
        tabBarOnPress: tabBarOnPress
      },
    },
    makeTab: {
      screen : MakeStack,
      navigationOptions: {
        tabBarLabel: ()=>{},
        tabBarIcon: ({focused, tintColor}) => (
          <View>
            {!focused?
            <View style={{justifyContent:"center", alignItems:"center"}}>
              <Image style={{height:25, width:25, resizeMode:"contain"}} source={config.images.createIcon} />
              {/*<StyledText style={{color:"#c1c1c1", fontSize:8}}>{"만들기"}</StyledText>*/}
            </View>
            :
            <View style={{justifyContent:"center", alignItems:"center"}}>
              <Image style={{height:25, width:25, resizeMode:"contain"}} source={config.images.selectedCreateIcon} />
              {/*<StyledText style={{color:"#d64409", fontSize:8}}>{"만들기"}</StyledText>*/}
            </View>
            }
          </View>
        ),
        barStyle: {backgroundColor: '#FFC0CB'},
        tabBarOnPress: tabBarOnPress
      },
    },
    abountmyselfTab:{
      screen : AboutMyselfStack,
      navigationOptions: {
        //tabBarLabel: '나',
        tabBarLabel: ()=>{},
        tabBarIcon: ({focused, tintColor}) => (
          <View>
            {!focused ?
            <View style={{justifyContent:"center", alignItems:"center"}}>
              <Image style={{height:25, width:25, resizeMode:"contain"}} source={config.images.meIcon} />
              {/*<StyledText style={{color:"#c1c1c1", fontSize:8}}>{"나"}</StyledText>*/}
            </View>
            :
            <View style={{justifyContent:"center", alignItems:"center"}}>
              <Image style={{height:25, width:25, resizeMode:"contain"}} source={config.images.selectedMeIcon} />
              {/*<StyledText style={{color:"#d64409", fontSize:8}}>{"나"}</StyledText>*/}
            </View>
            }
          </View>
        ),
        
        barStyle: {backgroundColor: '#FFC0CB'},
        tabBarOnPress: tabBarOnPress
      },
    },
  },{
    tabBarOptions : {
      activeTintColor: '#d64409',
      inactiveTintColor: '#c1c1c1',
      style: {
        //height:100,
        borderTopColor: "#e2e2e2",
        borderTopWidth:1,
        shadowOpacity: 0.1,
        elevation: 3,
        //zIndex:1,
      }
    }
})

const MainStack = createSwitchNavigator({
  //main: StartScreen,
  start: Tabs,
});





const AppContainer = createAppContainer(MainStack);

export default class Main extends Component {
  state = {
    loading : false,
  }

  setInitData = async () => {
    const uniqueId = DeviceInfo.getUniqueId();
    const access_info = await AsyncStorage.getItem("access_info");
    let access_token = "";
    if(access_info){
      const d = JSON.parse(access_info);
      access_token = d.access_token;
      config.access_info = d;
      console.log("access_info.access_token : ", access_token);
    }
    console.log("config.notificationToken : ", config.notificationToken)
    const brandName = DeviceInfo.getBrand();
    const systemVersion = DeviceInfo.getSystemVersion();
    const deviceModel = DeviceInfo.getModel();
    
    try {
      console.log("AAAA1")
      const isConnected = await isNetworkAvailable();
      if(!isConnected) {
        throw new Error("네트워크 상태를 확인해 주세요.")
      }
      console.log("AAAA2")
      const resp = await axios.post(config.url.apiSvr, {
        "data": {
          "category": "public",
          "service" :"GetSession",
          "access_token" : access_token || "",
          "device": {
            "device_id": uniqueId,
            "notification_token": config.notificationToken || "",
            "platform": Platform.OS === "android" ? "android" : "ios",
            "info": brandName + ", " + deviceModel + ", " + systemVersion
          },
        }
      }, {timeout:4000})
      console.log("resp : ", resp)
      const {device, member} = resp.data.data;
      console.log("session id:", device.session_id)
      config.sessionInfo.session_id = device.session_id;
      config.deviceInfo.device_id = uniqueId;
      if (member) {
        config.member = member;
      } else {
        await AsyncStorage.removeItem("access_info")
        config.member = {
          member_id: '',
          nickname: '',
          introduce: '',
        };
        config.access_info = null;
      }
      setTimeout(()=>this.setState({loading:true}),3000);
    } catch(e){
      console.log("MainComponent  getSession error : ", e)
      showToast(e.message, "LONG");
    }
  }

  handleDynamicLink = async link => {
    // 다이나믹 링크 별로 분기 탐
    if (link && link.url) {
      console.log("link --> :", link);
      const s = link.url.split(config.url.webPage)
      console.log("s --> :", s);
      if (s && s.length === 2) {
        const params = s[1].split("/");
        console.log('params', params);
        if (params && params.length > 1) {
          if (params[1] === 'post') {
            if (params.length === 3) {
              console.log('id', params[2]);
              AsyncStorage.setItem("itemTitle","ttt");
              AsyncStorage.setItem("itemId",params[2]).then(
                NavigationService.navigate("listen")
              )
            }
          }
        } else {

        }
        
      } else {
      }
    }
  };



  componentDidMount() {
    dynamicLinks().onLink(this.handleDynamicLink);
    console.log("AAAAA");
    this.setInitData();
    SplashScreen.hide();
    
  //   setTimeout(() => {
  //     SplashScreen.hide();
  // }, 1000);
  }

  componentDidUpdate(prevProps, prevState ){
    // if(prevState.didGetToken !== this.state.didGetToken && this.state.didGetToken){
    //   this.setInitData();
    // }
    if(prevState.loading !== this.state.loading && this.state.loading){
      // this.recoveringNotification();

      /* backgound or terminated dynamic link */
      dynamicLinks()
      .getInitialLink()
      .then(link => {
        this.handleDynamicLink(link);
      });
    }
  }
  
  render() {
    console.log("loading : ", this.state.loading)
    if(!this.state.loading){
      return (
        <View style={{flex:1, backgroundColor: '#ffffff'}}>
          <StatusBar barStyle="dark-content" backgroundColor={'white'}/>
          <LottieView source={require("src/assets/docentogo.json")} autoPlay loop={false} />
          
        </View>)
    }
    return (
        <AppContainer ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }} />
    );
  }
}
