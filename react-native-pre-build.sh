# Android:
# [start emulator or plug in a device]
# [update build version number in AndroidManifest.xml and build.gradle]
npx react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res_temp
cd android/
./gradlew clean
cd ../

#iOS:
npx react-native bundle --dev false --entry-file index.js --bundle-output ios/main.jsbundle --assets-dest ./ios --platform ios
# [toggle jsBundle line in AppDelegate.madb]


